class RemoveCouchIds < ActiveRecord::Migration
  def up
    remove_column :workouts, :couch_id
    remove_column :segments, :couch_id
  end

  def down
  end
end
