class SWS.Views.AbsSwsModal extends Backbone.View
  autoHide: null
  elPrefix: ''
  importTemplate: JST['importControls']
  modalType: ''
  performance: null
  system: null
  user: null
  wrapperName: ''

  constructor: (options) ->
    @cid = _.uniqueId('view')
    @_configure(options || {})
    @wrapperName = options.wrapperName ? '#modal'
    @autoHide = options.autoHide ? true
    @_ensureElement()
    @user = SWS.Singletons.SystemUser.get()
    @performance = SWS.Singletons.Performance.get()
    @system = SWS.Singletons.System.get()
    @initialize.apply(this, arguments)
    @delegateEvents()

  initialize: (options = {}) ->
    @modalType = options.modalType ? 'master'

  ajaxErrorHandler: (request, error, error_type) =>
    # The request will be unauthorized only happen if the session expired since the page was loaded, so handle it simply
    if request and request.status == 401
      SWS.index.reload("Login Needed")
    else if error_type is "Bad Request" and typeof(request.responseJSON) is 'object' and request.responseJSON.errors
      errors = request.responseJSON.errors
      for key, msg of errors
        if key is 'message'
          $('#modal_message').html(@errors_to_txt("<strong>#{errors.message}</strong>"))
          $('#modal_message').addClass('text-danger')
        if $("#error_#{key}").html()
          $("#error_#{key}").html(@errors_to_txt(msg))
          $("##{key}").parent().addClass('has-error')

  hide: () =>
    SWS.index.modalHide()

  handleAddToPerformance: (event) =>
    SWS.log "AbsSwsModal:handleAddToPerformance"
    event.preventDefault()
    @hide()
    # Load the stage if we're not already performing!
    @performance.addStage(@model, !@performance.isPerforming())

  handleAddToWorkout: (event) =>
    SWS.log "AbsSwsModal:handleAddToWorkout"
    event.preventDefault()
    # User should be logged in, in order to get here.  If they are not, then fail
    if !@user.isLoggedIn()
      return false
    $('#current_controls').html(@importTemplate(formName: @formName, model: @model, workouts: @user.workouts))
    @delegateEvents()

  handleCancel: (event) =>
    SWS.log "absSwsModal: handleCancel"
    event.preventDefault()
    @stopListening()
    @hide()
    false

  handleFormSubmit: (event) =>
    event.preventDefault()

    # Empty possible errors from previous invocations
    $("##{@formName} .text-error").each(() ->
      $(this).empty()
    )
    ajaxObj =
      type: "POST"
      url: @formUrl
      data: $("##{@formName}").serialize()
      success: (data, textStatus, request) =>
        @hide()
        # a model was passed into this view, just update that - ex: user model for NewSessionModal
        if @model
          @model.set(data)
        else
          $('#main-container').html(data)
          if request.getResponseHeader('REDIRECT')
            Backbone.history.navigate(request.getResponseHeader('REDIRECT'), {trigger: true})
      error: @ajaxErrorHandler

    # if this is a slave-modal, force the type to json
    if @modalType is 'slave'
      ajaxObj['dataType'] = 'json'
    jQuery.ajax(ajaxObj)

  errors_to_txt: (errors) ->
    html = []
    if typeof errors is "string"
      @symbol_to_txt(errors)
    else
      for error in errors
        html.push @symbol_to_txt(error)
        html.join('<br>')

  standardRender: () =>
    # Empty the modal first - in case anything has been left behind
    $(@wrapperName).empty()

    $(@el).html(@template(model: @model, formName: @formName))
    $(@wrapperName).html(@el)
    $(@wrapperName).modal({keyboard: false})
    SWS.index.modalShow("AbsSwsModal::standardRender")
    this

  symbol_to_txt: (symbol) ->
    str = symbol.replace(/_/g, ' ')
    str.charAt(0).toUpperCase() + str.slice(1)

  # ToDo: implement something like this:
#    events:
#      "focus input": "clearField"

#  clearField: (event) ->
#    if $(event.target).hasClass('error')
#      $(event.target).removeClass('error')
#      $(event.target).val('')
#    SWS.log(event)