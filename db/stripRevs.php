<?php

// Open stdin
//$fp = fopen('', 'r');

// Read into string
$json = stream_get_contents(STDIN);
$input = json_decode($json, true);

/* Input should look like:
 * Array
 * (
 *    [total_rows] => 357
 *    [offset] => 0
 *    [rows] => Array
 *        (
 *            [0] => Array
 *                (
 *                    [id] => 104197905
 *                    [key] => 104197905
 *                    [value] => Array
 *                        (
 *                            [rev] => 5-984f6c6d569a585f0add8938417fbf20
 *                        )
 *
 *                    [doc] => Array
 *                        (
 *                            [_id] => 104197905
 *                            [_rev] => 5-984f6c6d569a585f0add8938417fbf20
 *                            [tracklink_enid] => 104197905
 *                            [trackid] => 41908
 *                            [duration] => 313
 *                            [artist] => clavado en un bar
 *                            [title] => mana
 *                            [type] => track
 *                        )
 *
 *                )
 */

$output = array();
$output['docs'] = array();

if (is_array($input)) {
    // we only care about what's in ['doc']
    foreach ($input['rows'] as $doc) {
        unset($doc['doc']['_rev']); // prevent doc conflict errors
        if (is_array($doc) && $doc['doc']) 
            $output['docs'][] = $doc['doc'];
    }
}

echo json_encode($output);

?>
