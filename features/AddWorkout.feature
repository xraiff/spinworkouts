Feature: User can manually add workout

  Background: System knows about a set of users
    Given the system knows about the following users:
      | uid | name      |  email |
      | 12345 | xraiff  |  steve@example.com |

Scenario: See add workout link
  Given I am logged in with provider "identity"
  And I am on the homepage
  Then I should see "New Workout"

#xScenario
#  When I follow "New Workout"
#  Then I should be on the Create New Workout page
#  When I fill in "Title" with "Workout #3"
#  When I fill in "Description" with "Long intense workout"
#  And I press "Save Changes"
#  Then I should be on the Spinworkouts home page
#  And I should see "Workout #3"