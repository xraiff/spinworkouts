When /^the client requests GET (.*)$/ do |path|
  header 'Accept', 'application/json'
  header 'Content-Type', 'application/json'     
  visit(path)
end
                                                                                       
Given /^I am running pjax$/ do 
  header 'X-PJAX', 'true'  
  header 'X-PJAX-Container', '#dummy'  
end

Then /^the response should be JSON:$/ do |string| 
  JSON.parse(page.source).should == JSON.parse(string)
end                                           
  
Then /^the data-workouts should be JSON:$/ do |json_string|
  pagedata = page.execute_script("$('#container').data('workouts'))")
  JSON.parse(pagedata).should == JSON.parse(json_string)  
end   

Then /^I can run Hello World$/ do |json_string|
  page.execute_script("alert('Hello World');")
end

Then /^at "([\.\#][^"])*", I should see JSON data:(.*)$/ do |locator,json_string|
  JSON.parse(page.find(locator)).should == JSON.parse(json_string)  
end