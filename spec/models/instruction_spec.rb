require 'spec_helper'

describe Instruction do
  before :each do
    @instruction = FactoryGirl.build(:instruction)
  end

  it "should save the instruction" do
    @instruction.text = "fred"
    @instruction.text.should eq("fred")
  end

  describe "#find_by_user" do
    before(:each) do
      FactoryGirl.reload
      @user = FactoryGirl.create(:user)
      @i1 = FactoryGirl.create(:instruction, :user => @user)
      @i2 = FactoryGirl.create(:instruction, :user => @user)
    end

    it "should find segments by factory user" do
      i = Instruction.find_all_by_user_id(@user.id)
      i.length.should == 2
    end
  end
end
