class SegmentInstruction < ActiveRecord::Base
  attr_accessible :instruction_id, :offset, :segment_id    
  belongs_to :segment
  belongs_to :instruction       
    
  # Offset is actually a time index (secs) in the segment, 
  # so the default should probably be one second past the end of the last one for this association
  #before_save do
  #  
  #end
end
