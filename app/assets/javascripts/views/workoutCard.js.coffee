class SWS.Views.WorkoutCard extends Backbone.View
  showDeleteLinks: false
  user: null

  events:
    "click .card_link" : "handleCardLink"
    "click .card_delete_link" : "handleDeleteLink"

  # The workout card will cycle through the various stages of the workout, highlighting the respective parts
  # of the SVG

  constructor: (options) ->
    @cid = _.uniqueId('view');
    @_configure(options || {});

    # Set element to pre-existing element in constructor, so that
    # "delegate" can function appropriately
    @el = "#workout_card_#{@model.get('id')}"
    @_ensureElement();
    @initialize.apply(this, arguments);
    @delegateEvents();

  initialize: (options = {}) ->
    @showDeleteLinks = options.showDeleteLinks ? true
    @user = SWS.Singletons.SystemUser.get()
    @render()

  handleCardLink: (event) =>
    SWS.log "WorkoutCard::handleCardLink"
    event.preventDefault()
    new SWS.Views.WorkoutPlayerModal({model: @model})

  handleDeleteLink: (event) =>
    SWS.log "WorkoutCard::handleDeleteLink"
    event.preventDefault()
    new SWS.Views.MessageModal({
      message: "Are you sure you want to delete this workout?"
      ok: () =>
        @model.destroy()
        $(@el).remove()
    })

  render: () =>
    if @showDeleteLinks and (@model.get('user_id') == @user.id)
      $(@el).find("a.card_delete_link").show()