class SWS.Views.MessageModal extends SWS.Views.AbsSwsModal
  template: JST['messageModal']

  events:
    "click #complete" : "handleComplete"
    "click #cancel" : "handleCancel"

  initialize: (options = {}) ->
    @render()

  handleCancel: (event) =>
    event.preventDefault()
    @hide()
    if @options.cancel
      @options.cancel()
    false

  handleComplete: (event) =>
    SWS.log "handleComplete"
    event.preventDefault()
    @hide()
    if @options.ok
      @options.ok()
    false

  render: () =>
    # Empty the modal first - in case anything has been left behind
    $('#modal').empty()

    $(@el).html(@template(model: @model, message: @options.message))
    $('#modal').html(@el)
    $('#modal').modal({keyboard: false})
    SWS.index.modalShow("MessageModal::render")
    this