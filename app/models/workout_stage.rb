class WorkoutStage < ActiveRecord::Base
  attr_accessible :order, :stage_id, :workout_id  
  belongs_to :workout
  belongs_to :stage       
end
