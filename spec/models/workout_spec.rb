require 'spec_helper'

describe Workout do
  @longtext='now is the time for all good men to come to the aid of our country. now is the time for all good men to come to the aid of our country. now is the time for all good men to come to the aid of our country. now is the time for all good men to come to the aid of our country. now is the time for all good men to come to the aid of our country.  now is the time for all good men to come to the aid of our country. now is the time for all good men to come to the aid of our country. now is the time for all good men to come to the aid of our country. now is the time for all good men to come to the aid of our country.'

  describe "#create" do
    it "should create a new workout with the indicated title, description" do
      title = 'now is the time'
      description = 'for all good men to aid the country'
      new_workout = Workout.create({:title => title, :description => description})

      new_workout.id.should_not be_nil
      new_workout.title.should eq(title)
      new_workout.description.should eq(description)
    end
  end

  describe "#duration_sec" do
    it "should display the total duration when all the workout stages have a duration" do
      workout = FactoryGirl.build(:workout)
      workout.stages << FactoryGirl.build(:stage, duration_sec: 50)
      workout.stages << FactoryGirl.build(:stage, duration_sec: 60)
      workout.stages << FactoryGirl.build(:stage, duration_sec: 30)
      workout.save
      workout.duration_sec.should == 50 + 60 + 30
    end
  end

  describe "#set_duration_sec" do
    before(:each) do
      @w = FactoryGirl.create(:workout, :with_stages)
    end

    it "should return the sum of all stage durations" do
      w = Workout.find(@w.id)
      w.set_duration_sec
      w.duration.should == w.stages.map(&:duration_sec).inject(0, :+)
    end

    it "should return nil if any stage has nil duration" do
      w = Workout.find(@w.id)
      w.stages[0].duration_sec = nil
      w.set_duration_sec
      w.duration.should be_nil
    end
  end

  describe "#stages" do
    it "should respond to stages" do
      w = Workout.new
      w.should respond_to(:stages)
    end

    it "should be able to delete stages" do
      w = FactoryGirl.create(:workout, :with_stages)
      w.stages.count.should > 0
      expect { w.stages.destroy_all }.to_not raise_error
    end

    it "should be able to add stages" do
      w = FactoryGirl.create(:workout)
      s = FactoryGirl.create(:stage)
      expect { w.stages << s }.to change{w.stages.count}.by(1)
    end

    it "should update workout_stages when stages are added" do
      w = FactoryGirl.create(:workout)
      s = FactoryGirl.create(:stage)
      expect { w.stages << s }.to change{w.workout_stages.count}.by(1)
    end
  end

  describe "#title" do
    it { should_not accept_values_for(:title, nil, "a", @longtext) }
    it { should accept_values_for(:title, "This is a title", "This is also a title") }
  end

  describe "#workout_stages" do
    before(:each) do
      FactoryGirl.reload
      @number_of_stages = 5
      #@w = FactoryGirl.create(:workout_with_stages)
      @w = FactoryGirl.create(:workout, :with_stages, :number_of_stages => @number_of_stages)
    end

    it "should respond to workout_stages" do
      @w.should respond_to(:workout_stages)
    end

    it "should have stages" do
      @w.workout_stages.count.should == @number_of_stages
    end

    it "should persist workout_stages and preserve order" do
      w2 = Workout.find(@w.id)
      w2.workout_stages[0].order.should == 1
      w2.workout_stages[0].stage_id.should_not be_nil
      w2.workout_stages[0].workout_id.should_not be_nil
      w2.workout_stages[1].order.should == 2
      w2.workout_stages[1].stage_id.should_not be_nil
      w2.workout_stages[1].workout_id.should_not be_nil
    end

    it "should auto-assign order field when unspecified" do
      ws = FactoryGirl.build(:workout_stage)
      @w.workout_stages.push(ws)
      @w.workout_stages.length.should == @number_of_stages + 1
      @w.workout_stages[@number_of_stages].id.should == ws.id
      @w.workout_stages[@number_of_stages].order.should == @number_of_stages + 1
    end

    it "should delete workout_stages when it is deleted" do
      ws1 = @w.workout_stages[0]
      ws2 = @w.workout_stages[1]
      @w.destroy
      expect { WorkoutStage.find(ws1.id) }.to raise_error(ActiveRecord::RecordNotFound)
      expect { WorkoutStage.find(ws2.id) }.to raise_error(ActiveRecord::RecordNotFound)
    end

    it "should not delete stages when it is deleted" do
      st1 = @w.workout_stages[0].stage
      st2 = @w.workout_stages[1].stage
      @w.destroy
      expect { Stage.find(st1.id) }.to_not raise_error(ActiveRecord::RecordNotFound)
      expect { Stage.find(st2.id) }.to_not raise_error(ActiveRecord::RecordNotFound)
    end
  end
end
