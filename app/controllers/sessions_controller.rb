class SessionsController < ApplicationController
  #ToDo: Omniauth identity registration error handling
  #ToDo: Omniauth custom registration form: https://github.com/intridea/omniauth-identity                                                                                        
  def new
    respond_to do |format|
      format.html { render :layout => get_layout() } # responds with JUST new.html.erb
    end
  end

  def create               
    @user = User.from_omniauth(request.env["omniauth.auth"])
    session[:user_id] = @user.id
    respond_to do |format|   
      format.json { render :json => @user }
      format.html {
        if pjax_request?
          flash[:send_user] = true
          #if session[:return_to]
          #  Rails.logger.info "session/create, returning_to #{session[:return_to]}"
          #  return_to = session[:return_to]
          #  session[:return_to] = nil
          #  redirect_to return_to
          #  return
          #end
          render :layout => get_layout() # create.html.erb
        else
          redirect_to root_url, notice: "Signed in!" 
        end
      }
    end
  end

  def destroy 
    session[:user_id] = nil
    respond_to do |format|
      format.json { render :json => {success: true} }
      format.html {
        if pjax_request?
          render :layout => get_layout({:class => 'nav-collapse'})
        else
          redirect_to root_url, notice: "Signed out!"
        end
      }
    end
  end

  def failure
    #redirect_to login_path, alert: "Authentication failed, please try again."
    @identity = env['omniauth.identity']
    respond_to do |format|
      format.json { render :status => :bad_request, :json => { :errors => { :message => 'Invalid authentication'} } }
      format.html {
        if params['message'] == 'invalid_credentials'
          @identity_error = 'Invalid Credentials'
          render 'new', :layout => false
        else
          render '/identities/new', :layout => false
        end
      }
    end
  end  
end
