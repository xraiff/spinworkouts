describe("Workout view", function () {
  beforeEach(function() {          
    // SWS is initialized by sws.js.coffee
    stage1 = new SWS.Models.Stage({
      id: 1,
      title: "Workout Stage #1"
    });
    SWS.Collections.stages.add(stage1);
    stage2 = new SWS.Models.Stage({
      id: 2,
      title: "Workout Stage #2"
    });                               
    SWS.Collections.stages.add(stage2);
    stage3 = new SWS.Models.Stage({
      id: 3,
      title: "Workout Stage #3"
    });
    SWS.Collections.stages.add(stage3);
    this.workout = new SWS.Models.Workout({
      title: "My Workout",
      description: "a workout",
      stage_ids: [1,2,3]
    });   
    this.workoutView = new SWS.Views.Workout({model: this.workout})
    this.workoutView.render();
  });
 
  describe("when displayed", function() {                     
    it("the workout title is rendered", function() { 
      expect($(this.workoutView.el)).toContainHtml("My Workout");
    });    
    
    it("the stage images appear", function() {
      expect($(this.workoutView.el)).toContain("img");
    })
            
    it("all the stage images appear", function() {
        //expect($(this.workoutView.el).find('.stage-images').children().length).toEqual(3);
        expect($(this.workoutView.el).find('.stage-image').length).toEqual(3);        
    });
    
  });
   
});