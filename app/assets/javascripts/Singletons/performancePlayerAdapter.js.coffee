class SWS.Singletons.PerformancePlayerAdapter

  instance = null

  class PerformancePlayerAdapterPrivate extends SWS.Models.JwPlayerAdapter
    playerElement: "performanceplayer"

  @get: (options) ->
    instance ?= new PerformancePlayerAdapterPrivate(options)