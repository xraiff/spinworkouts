require 'net/http'
require 'uri'
require 'youtube_service'

class SearchesController < ApplicationController

  def index
    handle_search
  end

  def create
    handle_search
  end

  def handle_search
    begin
      if session[:debug_yt]
        raw_search = File.read(Rails.root.join('app', 'data', 'yt.json'))
        yt_results = JSON.parse(raw_search)
        Rails.logger.info "DEBUG_YT"
      else
        Rails.logger.info "handle_params, searchfor = #{params[:searchfor]}"
        yt_results = YouTubeService::query(params[:searchfor])
      end

      @search = []
      yt_results.each do |item|
        # What if there are more?
        #stage = Stage.find_by(:youtube_id => item['youtube_id'])
        stage = Stage.where("youtube_id = '#{item['youtube_id']}'")
        if !stage.nil? and stage.count > 0
          @search << stage.first
        else
          @search << item
        end
      end

      respond_to do |format|
        format.html # index.html.erb
                    # format.xml  { render :xml => @workouts }
                    # format.xml  # index.xml.builder
        format.json { render :json => @search }
      end
    rescue Exception => e
      respond_to do |format|
        format.json { render :status => :bad_request, :json => { :errors => { :message => "Search failed: couldn't connect to YouTube"}}}
      end
    end
  end
end
