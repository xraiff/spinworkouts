class SWS.Views.SpinworkoutsIndex extends Backbone.View
  # This view is really the master visual container for the clientside application
  # It's responsibilities include:
  #     -central login facility
  #     -search

  modalCount: 0
  navDyamic: null
  msg_template: JST['message']
  user: null

  initialize: () ->
    SWS.log("index::initialize")
    @user = SWS.Singletons.SystemUser.get()
    @user.on('change', @render, this)
    @navDynamic = new SWS.Views.NavDynamic()

  loggedIn: ->
    SWS.log 'User has logged in'

  modalShow: (msg = '') =>
    if $('#modal').attr('visibility') is 'hidden'
      $('#modal').modal('show')

    @modalCount = @modalCount + 1
    full_msg = "modalShow, modalShow = #{@modalCount}, from #{msg}"
    SWS.log full_msg

  modalHide: (msg = null) =>
    if @modalCount > 0
      @modalCount = @modalCount - 1
    full_msg = "modalHide, modalCount = #{@modalCount}, from #{msg}"
    SWS.log full_msg
    #    if @modalCount is 0
    #      $('#modal').empty()
    $('#modal').modal('hide')
    $('.modal-backdrop').each(() ->
      $(this).remove()
    )

  navHome: =>
    SWS.log "take me home"
    Backbone.history.navigate("/", {trigger: true})

  reload: (message) ->
    # Close the modals, set an alert message, then reload the page.
    window.location.href = "{window.location.href}#main-container"
    $('#modal').modal('hide')
    $('#message').html("<div class='alert alert-error'>#{message}, reloading page...</div>")
    setTimeout(() =>
      window.location.href = '/'
    , 5000)

  setTimedMessage: (msg, sec = 2) ->
    $("#message").html(@msg_template(msg: msg))
    $("#message").show()
    @timedMessageHide(sec)

  timedMessageHide: (sec = 2) ->
    setTimeout(() =>
      $('#message').hide()
      @render()
    , sec * 1000)