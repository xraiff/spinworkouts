module ApplicationHelper
  def title
    base_title = "Ruby on Rails Tutorial Sample App"
    if @title.nil?
      base_title
	  else
	    "#{base_title} | #{@title}"
	  end
  end

  def current_user
    begin
      @current_user ||= User.find(session[:user_id]) if session[:user_id]
    rescue Exception => e
      @current_user = nil
    end
  end

  def duration_formatted(model)
    t = Time.new(2013)
    return "00:00:00" if model.duration_sec.nil?
    t += model.duration_sec
    if t.hour > 0
      t.strftime("%H:%M:%S")
    elsif t.min > 0
      t.strftime("%M:%S")
    elsif t.sec > 0
      t.strftime("%S")
    else
      "00:00:00"
    end
  end

  def graph_div(model, options)
    content_tag :div, options[:id] || "", {:class => options[:class]} do
      raw(model.to_svg({'height' => options[:height], 'width' => options[:width]}))
    end
  end

  def logo
    image_tag("logo.png", :alt => "Sample App", :class => "round")
  end

  def pjax?
    request.env['HTTP_X_PJAX'].present? || env['HTTP_X_PJAX'].present?
  end
end
