# The playerModal has 3 modes:
#
# (Workout)
#    -details => /workout/$wid
# /workout/$wid  => demo stages
#   -Perform stage
# /workout/$wid/perform
#    -Skip to this stage
#    -Delete this stage (from the performance, which isn't a workout yet)
#         This may require logging in
# /

# ? can the player modal be the same as the searchplayer modal?

class SWS.Views.PlayerModal extends SWS.Views.AbsSwsModal
  el: '#player_modal'
  ctrlsEl: '#player_modal_ctrls'
  ctrlsTemplate: JST['playerModalCtrls']
  isPerformanceStages: false
  message: null
  name: "playerModal"
  player: null

  events:
    "click #add_to_btn" : "handleAddToWorkout"
    "click #details_btn": "handleDetails"
    "click #perform_workout_btn" : "handlePerform"
    "click #player_modal_dismiss_btn": "handleCancel"

  constructor: (options = {}) ->
    options.wrapperName ||= '#player_modal'
    super(options)

  initialize: (options) ->
    @message = options.message ? ''
    @isPerformanceStages = options.isPerformanceStages ? false
    @render()
    @delegateEvents()

  loggit: (event) =>
    SWS.log "playerModal: loggit"

  handleAddTo: (event) =>
    event.preventDefault()
    SWS.log "playerModal: handleAddTo"

  handleDetails: (event) =>
    event.preventDefault()
    SWS.log "playerModal: handleDetails"
    @hide()
    $.pjax(
      container: '#inner-container'
      push: true
      url: "/workouts/#{@model.id}"
    )

  handlePerform: (event) =>
    event.preventDefault()
    SWS.log "playerModal: handlePerform"
    @hide()

    if @model.constructor.name == "Workout"
      @performance.loadWorkout(@model)
      $.pjax(
        container: '#inner-container'
        push: true
        url: "/workouts/#{@model.id}/perform"
      )
    else
      @performance.addStage(@model, true)

  render: () =>
    #ToDo: html purify the workout title
    $('#player_modal_title').html(@model.get('title'))
    buttons =
      perform: !@performance.isPerforming()
      skip_to: false
      add_to: false
      delete: false
      details: true
      close: true

    if @model.constructor.name == "Stage"
      buttons.details = false
      if isPerformanceStages
        buttons.delete = true

      else if @performance.isLoaded()
        buttons.add_to = true


    if _.size(buttons) > 3 or buttons.add_to
      ctrlClasses = 'col-md-6 col-md-offset-3'
    else
      ctrlClasses = 'col-md-6 col-md-offset-4'

    $(@ctrlsEl).html(@ctrlsTemplate(ctrlClasses: ctrlClasses, buttons: buttons))
    @graph = new SWS.Views.IntensityWidget({model: @model})
    $('#player_modal_graph').html(@graph.el)

    $('#player_modal').modal({keyboard: false})
    $('#player_modal').modal('show')
    this

