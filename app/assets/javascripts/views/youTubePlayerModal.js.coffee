class SWS.Views.YouTubePlayerModal extends SWS.Views.AbsSwsModal
  durationDefault: 'loading...'
  formName: 'importYT'
  formUrl: '/stages'
  name: "searchPlayerModal"
  player: null
  template: JST['youTubePlayerModal']

  events:
    "click #add_to_workout_btn": "handleAddToWorkout"
    "click #add_to_perform_btn": "handleAddToPerformance"
    "click #perform_btn": "handleAddToPerformance"
    "click #dismiss_btn": "handleCancel"
    "click #new_workout_btn": "handleNewWorkout"
    "click #save_btn": "handleSave"

  initialize: (options) ->
    SWS.log "YouTubePlayerModal :: initialize"
    @render()

  createStage: (event, callback = null) =>
    SWS.log "YouTubePlayerModal:handleSave"
    event.preventDefault()

    # Empty possible errors from previous invocations
    $("##{@formName} .text-error").each(() ->
      $(this).empty()
    )

    ajaxObj =
      type: "POST"
      url: @formUrl
      data: $("##{@formName}").serialize()
      dataType: 'json'
      success: (data, textStatus, request) =>
        if @model
          @model.set(data)
        if callback
          callback()
        else
          @hide()
          @system.setTimedMessage "Saved"
      error: @ajaxErrorHandler

    jQuery.ajax(ajaxObj)

  # Before launching the newWorkout modal, we save the stage first...
  # That way, we can pass in the stage as part of the workout create
  handleNewWorkout: (event) =>
    @createStage(event, () =>
      new SWS.Views.NewWorkoutModal({stage_ids: [@model.id]})
    )

  handleSave: (event, callback = null) =>
    @createStage(event, () =>
      data_obj =
        workout_id: $('#workoutSelect').val()

      ajaxObj =
        type: "POST"
        url: "/workouts/add_stage/#{@model.id}"
        data: data_obj
        dataType: 'json'
        success: (data, textStatus, request) =>
          @hide()
          @system.setTimedMessage "Saved"
        error: @ajaxErrorHandler

      jQuery.ajax(ajaxObj)
    )

  onPlayerStateChange: (event) =>
    duration = $('#sm_duration').html()
    if duration is @durationDefault
      newDuration = @player.getDuration()
      if newDuration > 0
        SWS.log "Setting duration to #{newDuration}"
        @model.set('duration_sec', newDuration)
        $('#sm_duration').html(newDuration.toHHMMSS())
        $('#current_controls div div button').removeClass('disabled')
        @stopListening(@player)
      else if newDuration == -1
        SWS.Notifier.log("Video duration: -1 on searchPlayer::getDuration")
        @system.setTimedMessage("This video cannot be imported from YouTube...")

  render: () =>
    SWS.log "YouTubeRender start"
    # Empty the modal first - in case anything has been left behind
    $('#modal').empty()

    duration = @durationDefault
    if @model and @model.get('duration_sec')
      duration = @model.get('duration_sec').toHHMMSS()

    $(@el).html(@template(model: @model, user: @user, performance: @performance, duration: duration))

    $('#modal').html(@el)
    $('#modal').modal({keyboard: false})
    @player = SWS.Singletons.ModalPlayerAdapter.get()
    @listenTo(@player, 'modalplayer:statechange', @onPlayerStateChange)
    @player.loadItem(@model, true)
    SWS.log "YouTubeRender end"
    this

