-- MySQL dump 10.13  Distrib 5.1.54, for apple-darwin10.5.0 (i386)
--
-- Host: localhost    Database: sws_development
-- ------------------------------------------------------
-- Server version	5.1.54

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `comments`
--

DROP TABLE IF EXISTS `comments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `commenter` varchar(255) DEFAULT NULL,
  `body` text,
  `workout_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `comments`
--

LOCK TABLES `comments` WRITE;
/*!40000 ALTER TABLE `comments` DISABLE KEYS */;
INSERT INTO `comments` VALUES (1,'Fred','Here is a comment',1,'2011-07-03 17:59:21','2011-07-03 17:59:21');
/*!40000 ALTER TABLE `comments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `schema_migrations`
--

DROP TABLE IF EXISTS `schema_migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `schema_migrations` (
  `version` varchar(255) NOT NULL,
  UNIQUE KEY `unique_schema_migrations` (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `schema_migrations`
--

LOCK TABLES `schema_migrations` WRITE;
/*!40000 ALTER TABLE `schema_migrations` DISABLE KEYS */;
INSERT INTO `schema_migrations` VALUES ('20110302141040'),('20110303023336'),('2011050100000'),('20110701043431'),('20110703180235');
/*!40000 ALTER TABLE `schema_migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `site_comments`
--

DROP TABLE IF EXISTS `site_comments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `site_comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `body` text,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `site_comments`
--

LOCK TABLES `site_comments` WRITE;
/*!40000 ALTER TABLE `site_comments` DISABLE KEYS */;
INSERT INTO `site_comments` VALUES (1,'fred','comment','2011-07-03 18:16:38','2011-07-03 18:16:38'),(9,'fred','howard','2011-07-04 14:58:35','2011-07-04 14:58:35'),(15,'martin','is short','2011-07-04 20:22:23','2011-07-04 20:22:23'),(16,'freddy','aksljfd','2011-07-04 20:23:12','2011-07-04 20:23:12');
/*!40000 ALTER TABLE `site_comments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tags`
--

DROP TABLE IF EXISTS `tags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tags` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `workout_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tags`
--

LOCK TABLES `tags` WRITE;
/*!40000 ALTER TABLE `tags` DISABLE KEYS */;
/*!40000 ALTER TABLE `tags` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) NOT NULL DEFAULT '',
  `encrypted_password` varchar(128) NOT NULL DEFAULT '',
  `reset_password_token` varchar(255) DEFAULT NULL,
  `reset_password_sent_at` datetime DEFAULT NULL,
  `remember_created_at` datetime DEFAULT NULL,
  `sign_in_count` int(11) DEFAULT '0',
  `current_sign_in_at` datetime DEFAULT NULL,
  `last_sign_in_at` datetime DEFAULT NULL,
  `current_sign_in_ip` varchar(255) DEFAULT NULL,
  `last_sign_in_ip` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_users_on_email` (`email`),
  UNIQUE KEY `index_users_on_reset_password_token` (`reset_password_token`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'steve.raiff@gmail.com','$2a$10$Z9VHNpkj3lPXgahfhKa/0u6CGm4vNYhsFFgF/EAJwGMyGsfHLuQzm','JtDOoU85YDERi8M6I2cE','2011-07-16 16:18:31',NULL,75,'2012-04-25 14:29:02','2012-04-02 13:49:55','127.0.0.1','127.0.0.1','2011-07-01 04:48:50','2012-04-25 14:29:02'),(2,'fred@fred.com','$2a$10$hlDRFYQSkHFZaUJDzU0xteEjvLXpH/F1TdNAPaZb6J9OGQLWZ7Xs.',NULL,NULL,NULL,1,'2011-07-01 13:42:22','2011-07-01 13:42:22','127.0.0.1','127.0.0.1','2011-07-01 13:42:22','2011-07-01 13:42:22'),(3,'lkjfaskljfd@laksjdflj.com','$2a$10$8ZFRc6Z0LMIHNEl0yLfSsul4UTO8dSBJ2XEreGKhVei/em.gr/L8e',NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'2011-07-06 14:13:40','2011-07-06 14:13:40'),(4,'laksjdfjlk@lkjasdfljk.com','$2a$10$j9jhiO2.Hg8Qwk3KUXxopePPdeUhfwZZratcKxwilmcdILkr4UuhG',NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'2011-07-06 14:15:38','2011-07-06 14:15:38'),(5,'klajsdflkj@lkajsdfl.com','$2a$10$cIsrBkTuvlDMbPgrxbPISOJwrIKEeCSo1p.ccFBvjQ1KA/aBSC5y.',NULL,NULL,NULL,1,'2011-07-06 14:57:00','2011-07-06 14:57:00','127.0.0.1','127.0.0.1','2011-07-06 14:57:00','2011-07-06 14:57:00'),(6,'alksdjflkds@alkjsdfjk.com','$2a$10$dnsv/cq/uTgmzb6HIwrb8.r7rNVWQxARHP2DU.6n..4tarWOVHZmC',NULL,NULL,NULL,1,'2011-07-06 17:06:07','2011-07-06 17:06:07','127.0.0.1','127.0.0.1','2011-07-06 17:06:07','2011-07-06 17:06:07'),(7,'lakjsdflkj@lakjsdflj.com','$2a$10$KoqFZVdni9oZI81Xhwv0Z.2e0M663La30g.9rfbTETSrcc48TFjlK',NULL,NULL,NULL,1,'2011-07-06 17:07:42','2011-07-06 17:07:42','127.0.0.1','127.0.0.1','2011-07-06 17:07:42','2011-07-06 17:07:42'),(8,'aklsjdflk@lakjsdlk.com','$2a$10$oDwpawHA8er9Wwv.gtTfsekV.v1FfUGkQ4XFTVmhagn.l30dFW18W',NULL,NULL,NULL,1,'2011-07-06 17:16:43','2011-07-06 17:16:43','127.0.0.1','127.0.0.1','2011-07-06 17:16:43','2011-07-06 17:16:43'),(9,'aklsjdfjlk@laksjdf.com','$2a$10$prp8ZlNkNnpy8X0/DjN3OuDVPVn5sj0yRN.IC6vrPzxwB9Jsc8BuO',NULL,NULL,NULL,1,'2011-07-07 01:35:11','2011-07-07 01:35:11','127.0.0.1','127.0.0.1','2011-07-07 01:35:11','2011-07-07 01:35:11'),(10,'aslkdfjksd@lkajsdfjl.com','$2a$10$YouG2UylvB.nmVhrPr476e3jKkwMEQPZIgKWWTV8UBufv8naG5LZa',NULL,NULL,NULL,1,'2011-07-07 01:36:13','2011-07-07 01:36:13','127.0.0.1','127.0.0.1','2011-07-07 01:36:13','2011-07-07 01:36:13'),(11,'steve.raiff@test.com','$2a$10$PAUrhZ6gYk3nwG7Erxb76OQyefgvXPN0ItOWSza/rfTF/Ov8tozO.',NULL,NULL,NULL,1,'2011-07-17 16:03:25','2011-07-17 16:03:25','127.0.0.1','127.0.0.1','2011-07-17 16:03:25','2011-07-17 16:03:25');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `workouts`
--

DROP TABLE IF EXISTS `workouts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `workouts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `duration` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `workouts`
--

LOCK TABLES `workouts` WRITE;
/*!40000 ALTER TABLE `workouts` DISABLE KEYS */;
INSERT INTO `workouts` VALUES (1,'Test workout','This is a test workout',45,'2011-07-03 17:59:08','2011-07-03 17:59:08'),(2,'test workout 2','another test',55,NULL,NULL);
/*!40000 ALTER TABLE `workouts` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2012-04-26  5:30:49
