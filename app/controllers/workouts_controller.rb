require 'net/http'
require 'uri'

class WorkoutsController < ApplicationController
  #before_filter :except => [:index, :show] do |controller|
  #  controller.authenticate_user!(request.fullpath)
  #end
  before_filter :authenticate_user!, :except => [:index, :show]
  # rescue_from ActiveRecord::RecordNotFound, :with => :record_not_found

  # GET /workouts
  def index
    @title = "Workouts"
    respond_to do |format|
      format.html {
        searchOptions = {:page => params[:page], :per_page => params[:per_page]}
        searchOptions[:conditions] = {:user_id => params[:user_id]} unless params[:user_id].nil?
        @workouts = Workout.paginate(searchOptions)
        render :layout => get_layout
      }
      format.json { send_data Workout.to_json, disposition: 'inline' }
    end
  end

  # GET /workouts/1 
  def show
    @workout = Workout.find(params[:id])
    respond_to do |format|
      format.html {
        # See : https://github.com/defunkt/jquery-pjax/issues/85
        #       https://github.com/defunkt/jquery-pjax/commit/3311a9aa
        # To force pjax to understand the redirection... This *could* be done
        # for all url's actually
        response.headers['X-PJAX-URL'] = request.url if pjax?
        render :layout => get_layout({:title => "Workout: #{@workout.title}"})
      }
      format.json { send_data @workout.to_json, disposition: 'inline' }
    end
  end

  # GET /workouts/new
  # New renders just the workout-specific fields
  def new
    begin
      @workout = Workout.find(params[:id]).clone
    rescue
      @workout = Workout.new({:user_id => current_user.id})
    end
    if params[:stage_ids]
      @stages = []
      params[:stage_ids].each do |stage_id|
        begin
          stage = Stage.find(stage_id)
          @stages << stage
        rescue
        end
      end
    end
    respond_to do |format|
      format.html { render :new, :layout => get_layout() } # new.html.erb
    end
  end

  # ToDo: rearrange workoutstages as necessary, but changes to stage data should be done separately
  # GET /workouts/1/edit
  def edit
    @workout = Workout.find(params[:id])
    respond_to do |format|
      format.html { render :layout => get_layout() } # edit.html.erb
    end
  end

  # POST /workouts
  def create
    if request.xhr?
      return create_xhr
    end

    @workout = Workout.new(params[:workout])
    begin
      @workout.user_id = current_user.id
      @workout.save
      if !@workout.errors.empty? and params[:stage_ids]
        params[:stage_ids].each do |stage_id|
          stage = Stage.find(stage_id)
          @workout.stages << stage unless stage.nil?
        end
      end
      # Supporting normal html requests for posterity...
      respond_to do |format|
        format.html {
          flash[:page_notice] = 'Congratulations, you now have a new workout to play with'
          redirect_to workout_path(@workout.id)
        }
      end
    rescue ActiveRecord::RecordInvalid
      Rails.logger.info "Workout:create error caught: #{@workout.errors.full_messages.join('\n')}"
      flash[:notice] = @workout.errors.full_messages.join('\n')
      render :action => :new
    end
  end

  # Ajax POST /workouts
  def create_xhr
    Rails.logger.info "workouts_controller : create_xhr"
    begin
      @workout = Workout.create(params[:workout])
      if params[:stages_json] and params[:stages_json].length > 0
        @workout.json_to_stages(params[:stages_json])
      else
        if @workout.errors.empty? and params[:stage_ids].length > 0
          stages = params[:stage_ids].split(',')
          stages.each do |stage_id|
            stage = Stage.find(stage_id)
            @workout.stages << stage unless stage.nil?
          end
        end
      end
      @workout.user_id = current_user.id
      @workout.save!
      render :json => @workout
    rescue => e
      render :status => :bad_request, :json => {:errors => e.message}
    end
  end

# PUT /workouts/1
  def update
    @workout = Workout.find(params[:id])
    if @workout.nil?
      #Redirect to create
      redirect_to create_workout_path(params)
    end

    # reject changes to underlying stages
    params[:workout].delete(:stages_attributes) if params[:workout]

    if @workout.user_id != current_user.id
      if request.xhr?
        render :status => :bad_request, :json => {:errors => "User doesn't have write access to this workout"}
      end
    else
      if @workout.update_attributes(params[:workout])
        redirect_to(@workout, :notice => 'Workout was successfully updated.')
      else
        if request.xhr?
          render :status => :bad_request, :json => {:errors => @workout.errors.to_hash}
        else
          render :action => "edit"
        end
      end
    end
  end

# DELETE /workouts/1
  def destroy
    @workout = Workout.find(params[:id])
    respond_to do |format|
      if @workout.destroy()
        Rails.logger.info "Here I am"
        format.html { redirect_to(workouts_url) }
        format.json { render :json => {:success => true}}
      else
        flash[:notice] = "Failed to delete workout."
        format.html { redirect_to(workouts_url) }
        format.json { render :json => {:success => false}}
      end
    end
  end

# GET /workouts/add_stage/:stage_id - renders a modal form asking which workout to add the stage to
# POST /workouts/add_stage/:stage_id?workout_id=id[&workout_title=title] - adds the given stage to the workout
  def add_stage
    if !request.xhr?
      render :file => "public/401", :status => :unauthorized, :layout => get_layout()
      return
    end

    @workouts = current_user.workouts
    @stage = Stage.find(params[:stage_id])
    http_status = :ok

    if request.post?
      begin
        # Inner exception from ActiveRecord::RecordNotFound (or any other exception) should yield to outer exception block
        begin
          @workout = Workout.find(params[:workout_id])
          if @workout.user_id != current_user.id
            render :status => :unauthorized, :json => {:errors => "not workout owner"}
            return
          end
          @workout.stages << @stage
        rescue ActiveRecord::RecordNotFound
          @workout = Workout.create({:user_id => current_user.id, :title => self.workoutTitle, :stages => [@stage]})
          if !@workout.errors.nil?
            render :status => :bad_request, :json => {:errors => @workout.errors.to_hash}
            return
          end
        end
      rescue Exception => e
        logger.info "add_stage: #{e.class} :: #{e.message}"
        render :status => :bad_request, :json => {:errors => e.message}
      end
    else
      render :status => :bad_request, :json => {:errors => 'denied'}
    end
    render :json => @workout
  end

  def workoutTitle
    if params[:workout_title].nil?
      "#{current_user.name}'s Workout"
    else
      params[:workout_title]
    end
  end

  private

  def record_not_found
    render :text => "404 Not Found", :content_type => "text/html", :status => 404
  end

end
