class User < ActiveRecord::Base 
  attr_accessible :email, :name, :provider, :uid, :system_user
  
  validates_presence_of :name
  validates_uniqueness_of :email
  # Twitter doesn't provide an email
  validates_format_of :email, :with => /^[-a-z0-9_+\.]+\@([-a-z0-9]+\.)+[a-z0-9]{2,4}$/i, :if => :email

  has_many :workouts
  has_many :stages
  has_many :segments
  has_many :instructions
  
  def self.from_omniauth(auth)
    find_by_provider_and_uid(auth["provider"], auth["uid"]) || create_with_omniauth(auth)
  end

  def self.create_with_omniauth(auth)
    create! do |user|
      user.provider = auth["provider"]
      user.uid = auth["uid"]
      user.name = auth["info"]["name"]
      user.email = auth["info"]["email"]
      user.admin = false
    end
  end

  def as_json(options = {})
    workouts = []
    if self.workouts
      self.workouts.each do |w|
        workouts << {'id' => w.id, 'title' => w.title, 'description' => w.description, 'duration_sec' => w.duration_sec}
      end
    end

    json = { "id" => self.id, "name" => self.name, "system_user" => self.system_user, "workouts" => workouts }
    logger.info "User: as_json: #{json}"
    json
  end
end