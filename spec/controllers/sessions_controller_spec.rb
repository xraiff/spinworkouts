require 'spec_helper'

describe SessionsController do        

  #integrate_views 
  #fixtures :session
  #session = double('session')
  #session.stub(:valid? => true)
                 
  #Html tests
  #it "should successfully log in with a good login" do
  #  post 'create'
  #  flash[:notice].should_not be_nil
  #  response.should redirect_to
  #end

  describe "GET #new" do
    it "should display new modal" do
      request.env['HTTP_X_PJAX'] = true
      get :new
      response.should render_template(:new)
    end
  end
                                                    
  describe "POST #create" do
    before(:each) do
      #@user = FactoryGirl.create(:user)
      # start the factory
      request.env['omniauth.auth'] = {"provider" => "twitter", "user_id" => '12345', 'info' => {'name' => 'fred', 'email' => 'fred@gmail.com'}}
    end

    it "should login normally" do
      post :create
      response.should be_success
    end

    it "should redirect as necessary back to the originator" do
      request.env['HTTP_X_PJAX'] = true
      redirect_to = '/workouts/new'
      session[:redirect_to] = redirect_to
      post :create
      response.should redirect_to(redirect_to)
    end
  end
  
end
