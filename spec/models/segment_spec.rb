require 'spec_helper'

describe Segment do       
  before :each do
    #@attr = {
    #  :description => 'Here is a testing workout'
    #  :duration => 
    #}                 
    @seg1_json = '{"id":"80028b64505b9e4dde8ef9239a383bae",
    "key":"80028b64505b9e4dde8ef9239a383bae",
    "value":{
      "rev":"1-4277443e869675710fbfb71e12d59071"},
      "doc":{
        "_id":"80028b64505b9e4dde8ef9239a383bae",
        "_rev":"1-4277443e869675710fbfb71e12d59071",
        "title":"Hill Climb, Cadence count 80 RPM",
        "duration":28,
        "is_duration_constant":true,
        "heartrate":[140,148],
        "instructions":[{
          "text":"We will be doing a cadence count - 18-20 for :15 seconds","offset":0},
          {"text":"Count your cadence in 3","offset":10},
          {"text":"Count your cadence in 2","offset":11},
          {"text":"Count your cadence in 1","offset":12},
          {"text":"Count your cadence","offset":13},
          {"text":"Stop counting","offset":28}],
        "resistance":[5],
        "type":"WorkoutSegment"}}'    
  end
                
  it "should import json" do   
    segObj = ActiveSupport::JSON.decode(@seg1_json) 
    seg = Segment.importObject(segObj['doc'])
    seg2 = Segment.find(seg.id)
    seg2.should_not be_nil
  end

  describe "#user_id" do
    it "Should deliver the userid" do
      @user = FactoryGirl.create(:user)
      seg = FactoryGirl.create(:segment, :user => @user)
      seg.user_id.should == @user.id
    end
  end

  describe "#find_by_user" do
    before(:each) do
      FactoryGirl.reload
      @user = FactoryGirl.create(:user)
      @seg1 = FactoryGirl.create(:segment, :with_instructions, :user => @user)
      @seg2 = FactoryGirl.create(:segment, :with_instructions, :user => @user)
    end

    it "should find segments by factory user" do
      segs = Segment.find_all_by_user_id(@user.id)
      segs.length.should == 2
    end
  end

  describe "#segment_instructions" do
    before(:each) do
      FactoryGirl.reload
      @seg1 = FactoryGirl.create(:segment, :with_instructions)
    end

    it "should persist segment_instructions and preserve order" do
      s2 = Segment.find(@seg1.id)
      s2.segment_instructions[0].offset.should == 10
      s2.segment_instructions[0].segment_id.should_not be_nil
      s2.segment_instructions[0].instruction_id.should_not be_nil
      s2.segment_instructions[1].offset.should == 20
      s2.segment_instructions[1].segment_id.should_not be_nil
      s2.segment_instructions[1].instruction_id.should_not be_nil
    end

    it "should find instructions that link back to the segment" do
      segfind = Segment.find(:first)
      segfind.should be_an_instance_of(Segment)
      segfind.heartrate.should be_an_instance_of(Array)
      segfind.resistance.should be_an_instance_of(Array)
      segfind.heartrate.count.should == 6
      segfind.resistance.count.should == 6
      # This is the number created by factory girl
      segfind.segment_instructions.count.should == 5
      segfind.instructions.count.should == 5
      segfind.instructions.size.should == 5
      segfind.instructions.length.should == 5
      segfind.instructions[0].segments[0].id.should == segfind.id
    end

    it "should delete segment_instructions when segment is deleted" do
      si = @seg1.segment_instructions[0]
      @seg1.destroy
      expect { SegmentInstruction.find(si.id) }.to raise_error(ActiveRecord::RecordNotFound)
    end

    it "should not delete instructions when segment is deleted" do
      inst = @seg1.segment_instructions[0].instruction
      @seg1.destroy
      expect { Instruction.find(inst.id) }.to_not raise_error(ActiveRecord::RecordNotFound)
    end
  end

end
