# -*- encoding: utf-8 -*-

Gem::Specification.new do |s|
  s.name = "couchrest_model"
  s.version = "1.1.1"

  s.required_rubygems_version = Gem::Requirement.new("> 1.3.1") if s.respond_to? :required_rubygems_version=
  s.authors = ["J. Chris Anderson", "Matt Aimonetti", "Marcos Tapajos", "Will Leinweber", "Sam Lown"]
  s.date = "2011-04-28"
  s.description = "CouchRest Model provides aditional features to the standard CouchRest Document class such as properties, view designs, associations, callbacks, typecasting and validations."
  s.email = "jchris@apache.org"
  s.extra_rdoc_files = ["LICENSE", "README.md", "THANKS.md"]
  s.files = ["LICENSE", "README.md", "THANKS.md"]
  s.homepage = "http://github.com/couchrest/couchrest_model"
  s.require_paths = ["lib"]
  s.rubygems_version = "1.8.10"
  s.summary = "Extends the CouchRest Document for advanced modelling."

  if s.respond_to? :specification_version then
    s.specification_version = 3

    if Gem::Version.new(Gem::VERSION) >= Gem::Version.new('1.2.0') then
      s.add_runtime_dependency(%q<couchrest>, ["= 1.1.1"])
      s.add_runtime_dependency(%q<mime-types>, ["~> 1.15"])
      s.add_runtime_dependency(%q<activemodel>, ["~> 3.0"])
      s.add_runtime_dependency(%q<tzinfo>, ["~> 0.3.22"])
      s.add_development_dependency(%q<rspec>, ["~> 2.6.0"])
      s.add_development_dependency(%q<json>, ["~> 1.5.1"])
      s.add_development_dependency(%q<rack-test>, [">= 0.5.7"])
    else
      s.add_dependency(%q<couchrest>, ["= 1.1.1"])
      s.add_dependency(%q<mime-types>, ["~> 1.15"])
      s.add_dependency(%q<activemodel>, ["~> 3.0"])
      s.add_dependency(%q<tzinfo>, ["~> 0.3.22"])
      s.add_dependency(%q<rspec>, ["~> 2.6.0"])
      s.add_dependency(%q<json>, ["~> 1.5.1"])
      s.add_dependency(%q<rack-test>, [">= 0.5.7"])
    end
  else
    s.add_dependency(%q<couchrest>, ["= 1.1.1"])
    s.add_dependency(%q<mime-types>, ["~> 1.15"])
    s.add_dependency(%q<activemodel>, ["~> 3.0"])
    s.add_dependency(%q<tzinfo>, ["~> 0.3.22"])
    s.add_dependency(%q<rspec>, ["~> 2.6.0"])
    s.add_dependency(%q<json>, ["~> 1.5.1"])
    s.add_dependency(%q<rack-test>, [">= 0.5.7"])
  end
end
