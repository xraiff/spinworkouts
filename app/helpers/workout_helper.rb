module WorkoutHelper
  def workout_select(workouts)
    options = ["Select"]
    workouts.each do |w|
      options << w.title
    end
    options
  end

  def metadata_right(workout)
    num = workout.stages.count
    if num > 5
      num = 5
    end
    t = (5 - num) * 134 + 15
    t.to_s() + "px"
  end
end