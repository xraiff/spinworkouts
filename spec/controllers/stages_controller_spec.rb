require 'spec_helper'

describe StagesController do
  render_views
  #fixtures :session

  context "logged out" do
    describe "GET #new" do
      it "should return unauthorized with xhr" do
        xhr :get, :new
        response.status.should == 401
      end
    end

    describe "GET #show.html" do
      it "should render a stage page" do
        @stage = FactoryGirl.create(:stage)
        get :show, :id => @stage.id
        assigns(:stage).should eq(@stage)
        response.should render_template(:show)
      end
    end

    describe "GET #show.json" do
      it "should return stage data" do
        @stage = FactoryGirl.create(:stage)
        pending
      end
    end

  end

  context "logged in" do
    before(:each) do
      @user = FactoryGirl.create(:user)
      session[:user_id] = @user.id
    end

    describe "GET #new" do
      before(:each) do
        request.env['HTTP_X_PJAX'] = true
      end

      it "should render new template" do
        get :new
        response.should render_template(:new)
      end

      it "should render the template in a modal" do
        pending
      end

      it "should allow the user to select the workout to add the stage to if one is not already specified (and more than 1 exists)" do
        pending
      end

      it "should allow the user to create a new workout with this stage on it" do
        pending
      end

      context "a stage info was passed in" do
        before(:each) do
          @stage = FactoryGirl.create(:stage)
        end

        it "should assign the stage variable when a stage id was passed in" do
          get :new,  { :id => @stage.id }
          assigns(:stage).should eq(@stage)
        end

        it "should assign the stage variable when stage information was in the parameters" do
          get :new,  { :stage => {:title => 'xyz 1234', :description => 'sodor was a cool place', :youtube_id => 'ABCD1234'}}
          s = assigns(:stage)
          s.title.should == 'xyz 1234'
          s.description.should == 'sodor was a cool place'
          s.youtube_id.should == 'ABCD1234'
        end
      end
    end

    describe "POST #create" do
      it "should add to current users array of stages" do
        expect {
          post :create, {:stage => {:title => 'new stage', :description => 'this is a new stage'}}
        }.to change { @user.stages.count }.by(1)
      end

      it "should set title accordingly" do
        post :create, {:stage => {:title => 'stage123', :description => 'this is a new stage'}}
        s = Stage.find_by_title('stage123')
        s.should_not be_nil
        s.title.should == 'stage123'
      end

      it "should render :new template if too short a stage title was passed in" do
        expect {
          post :create, {:stage => {:title => 'xyz', :description => 'this is a new stage'}}
          response.should render_template(:new)
        }.to change { @user.stages.count }.by(0)
      end

      it "should append segments to new stage if segments were passed in" do
        s1 = FactoryGirl.create(:segment)
        s2 = FactoryGirl.create(:segment)
        s3 = FactoryGirl.create(:segment)
        post :create, {:stage => {:title => 'stageXYZ', :description => 'this is a new stage'}, :segment_ids => [s1.id, s2.id, s3.id]}
        s = Stage.find_by_title('stageXYZ')
        s.should_not be_nil
        s.segments.count.should == 3
      end

      it "should create a stage with information from YouTube if a YouTube video id was passed in" do
        post :create, {:stage => {:title => 'stage123', :description => 'this is a new stage', :youtube_id => 'XYAFAJDF9'}}
        s = Stage.find_by_youtube_id('XYAFAJDF9')
        s.should_not be_nil
        s.title.should == 'stage123'
      end
    end

    describe "#update" do
      context "a workout id was passed in" do
        before(:each) do
          request.env['HTTP_X_PJAX'] = true
          @stage = FactoryGirl.create(:stage)
        end
      end
    end
  end
end