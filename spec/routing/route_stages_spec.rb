require 'spec_helper'

describe "routing to stages" do
  it "routes put /workouts/:id/stages/:id to stages/:id/update" do
    {:put => "/workouts/1/stages/2"}.should route_to(
        :controller => "stages",
        :action => "update",
        :id => "2",
        :workout_id => "1"
    )
  end

  it "routes put /workouts/:id/stages/:id (method = post) to stages/:id/update" do
    pending
    {:post => "/workouts/1/stages/2", :method => "post"}.should route_to(
        :controller => "stages",
        :action => "update",
        :id => "2",
        :workout_id => "1"
    )
  end
end