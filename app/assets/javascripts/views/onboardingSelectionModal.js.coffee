class SWS.Views.OnboardingSelectionModal extends SWS.Views.AbsSwsModal
  name: "OnboardingSelectionModal"
  # These are the templates corresponding to the different states of the modal
  templates:
    'init': JST['onboardingInit']
    'rpe': JST['onboardingRpe']
    'rpeInfo': JST['onboardingRpeInfo']
  state: "init"

  events:
    "click #start": "handleStart"
    "click #startRpe": "handleStartRpe"
    "click #showRpeInfo": "handleShowRpeInfo"
    "click #dismissRpeInfo": "handleDismissRpeInfo"
    "click #intensityChoice": "handleIntensityChoice"

  #ToDo: change this to use contructor so that second "delegateEvents" isn't necessary
  initialize: (options) ->
    super(options)
    @render()
    # only show the modal on initialize, subsequent states don't need to re-show
    SWS.index.modalShow('OnBoardingSelectionModal::initialize')

  # ToDo: Choose workout by Intensity Profile
  handleIntensityChoice: (event) =>
    event.preventDefault()
    $.pjax(
      container: '#inner-container'
      push: true
      url: '/workouts'
    )
    @hide()

  handleStart: (event) =>
    event.preventDefault()
    @state = 'rpe'
    @render()

  handleStartRpe: (event) =>
    event.preventDefault()
    SWS.Singletons.Performance.get().start()
    @hide()

  # ToDo: make modal larger to accomodate more information
  handleShowRpeInfo: (event) =>
    event.preventDefault()
    @state = 'rpeInfo'
    @render()

  handleDismissRpeInfo: (event) =>
    event.preventDefault()
    @state = 'rpe'
    @render()

  render: () =>
    $(@el).html(@templates[@state]())
    $('#modal').html(@el)
    $('#modal').modal({keyboard: false})
    @delegateEvents()
    this