SWS::Application.routes.draw do
  scope 'api' do
    resources :youtube  
  end

  match "sessions/new", to: "sessions#new"
  match "sessions/destroy", to: "sessions#destroy"  

  # OMNIAUTH Routes
  # root to: "sessions#new"              
  match "/auth/:provider/callback", to: "sessions#create"
  match "/auth/failure", to: "sessions#failure"

  resources :identities
  resources :workouts

  resources :users do
    resources :workouts
  end
  resources :searches

  match "/workouts/add_stage/:stage_id", to: "workouts#add_stage"

# Backbone will handle the "stage/:num" portion of the url
  match "/workouts/:id/*all", to: "workouts#show"

  match "/stages/created/:id", to: "stages#created"

  resources :stages
  match "/stages/:id/*all", to: "stages#show"

  match "/workouts/created/:id", to: "workouts#created"

  resources :workouts do
    resources :stages
  end

  # /workouts/new => BackboneView

  # An explicit path for testing purposes
  match '/home', :to => "pages#home"

  resources :log

  # An implicit matching path for all else
  match '*path', :to => "pages#home"

  #match '/contact', :to => 'pages#contact'
  #match '/about', :to => 'pages#about'
  #match '/help', :to => 'pages#help'
  #
  #
  #
  root :to => 'pages#home'
  

  # OBSOLETE: WORKOUT SNIPPETS
  #match '/workouts/:id/snippet' => 'workouts#snippet'
  #match '/workouts/gallery/:page' => 'workouts#gallery'

  # WORKOUT DATA
  #match '/workout/:id' => "workouts#show"
  #match '/youtube/:id' => "youtube#show"

  #match '/workout/workout-1234.xml', :to => redirect('/public/workout/workout-1234.xml')
  #match '/workout/workout-2345.xml', :to => redirect('/public/workout/workout-2345.xml')
  ###

end
