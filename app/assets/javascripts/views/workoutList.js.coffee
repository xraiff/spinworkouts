class SWS.Views.WorkoutList extends Backbone.View
  # ToDo: combine this with SearchResults into a generic scrollable container view

  # <Operationally>
  # The teasers start out as (javascript) carousels + svg graph
  # They are then passed to the teaser view, for activation and rotation.
  # When a preview is activated, then the workoutPreviewModal loaded with the respective
  # workout model.

  # <Data-wise>
  # During the initial carouseling of the workoutCard, the stage data is needed
  # in order to cycle through the stage names as well as the highlighting of the svg.

  initialize: (options = {}) ->
    if options.workouts
      showDeleteLinks = options.showDeleteLinks || false

      for workout in options.workouts.models
        new SWS.Views.WorkoutCard({model: workout, showDeleteLinks: showDeleteLinks})