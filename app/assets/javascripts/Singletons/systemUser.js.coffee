class SWS.Singletons.SystemUser

  instance = null

  class SystemUserPrivate extends Backbone.Model
    urlRoot: '/users/'
    loginCallback: null
    options: null
    workouts: null

    initialize: (data) ->
      SWS.log "system user initialize"
      if data.options
        @options = new SWS.Models.UserOptions(data.options)
      else
        @options = new SWS.Models.UserOptions

      if data.workouts
        @workouts = new SWS.Collections.Workouts(data.workouts)
      else
        @workouts = new SWS.Collections.Workouts

    isLoggedIn: () =>
      @get('id') > 0

    login: (options = {}) =>
      # ToDo: show some kind of loading icon
      new SWS.Views.NewSessionModal(options)
      false

    logout: =>
      jQuery.ajax(
        type: "POST"
        url: '/sessions/destroy'
        success: (html) =>
          @clear()
          false
        error: @ajaxErrorHandler
      )
      false

    loginRequired: (callback, message = 'Login Required') =>
      if !@get('id')
        @loginCallback = callback
        @once('change:id', () =>
            @loginCallback()
        )
        options =
          message: message
          modalType: 'slave'

        @login(options)
      else
        callback()

    set: (attrs, options) =>
      Backbone.Model.prototype.set.apply(this, arguments)

      if attrs and attrs.workouts and @workouts
        @workouts.reset(attrs.workouts)

  @get: (data) ->
    instance ?= new SystemUserPrivate(data)
