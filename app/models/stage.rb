class Stage < ActiveRecord::Base

  require 'json_friends'
  include JsonFriends

  attr_accessible :title, :description, :image_link, :youtube_id, :user_id, :duration_sec, :intensity_points_json, :num_points, :mean, :min, :max
  validates :title, :length => {:minimum => 5, :maximum => 255}
  validates :image_link, :format => URI::regexp(%w(http https)), :allow_nil => false
  validates :youtube_id, :length => {:minimum => 5, :maximum => 255}, :allow_nil => false

  has_many :workout_stages
  has_many :workouts, :through => :workout_stages

  has_many :stage_segments, :before_add => [:assign_segment_order], dependent: :delete_all
  has_many :segments, :through => :stage_segments

  belongs_to :user

  before_create :compute

  DATAPOINTS_ERR_MSG = "Values must in the form [{:t => 0, :i => 100}]"
  STAGE_DEFAULT_INTENSITY = 100

  # auto-scale the datapoints to the duration_sec
  # ToDo: change this to a trigger on datapoints?
  def compute()
    intensity_points = self.intensity_points
    if intensity_points.nil? or intensity_points.length < 1
      return
    end

    intensity_points.sort! { |p1, p2| p1[:t] <=> p2[:t] }
    time_delta = intensity_points[intensity_points.length-1][:t] - intensity_points[0][:t]
    if time_delta > 0
      time_scale = self.duration_sec / time_delta
      intensity_points.map! { |point| {t: (point[:t] * time_scale).to_int, i: point[:i]} }
    end
    intensities = intensity_points.collect { |p| p[:i] }
    self.num_points = intensity_points.count
    self.send(:intensity_points=, intensity_points, true)
    self.min, self.max = intensities.minmax
  end

  def intensity_points
    if self.intensity_points_json
      JSON.parse(self.intensity_points_json, {:symbolize_names => true})
    else
      []
    end
  end

  def intensity_points=(values, already_computed = false)
    if values.nil?
      self.intensity_points_json = ''
      return
    end
    raise KeyError.new(DATAPOINTS_ERR_MSG) unless values.is_a?(Array)

    values.each do |value|
      raise KeyError.new(DATAPOINTS_ERR_MSG) unless value.is_a?(Hash)
      raise KeyError.new(DATAPOINTS_ERR_MSG) unless value.has_key?(:t)
      raise KeyError.new(DATAPOINTS_ERR_MSG) unless value.has_key?(:i)
      raise RangeError.new(":t in hash should be an Integer >= 0") unless value[:t] >= 0 and value[:t].is_a?(Integer)
      raise RangeError.new(":i in hash should be an Integer >= 0") unless value[:i] >= 0 and value[:i].is_a?(Integer)
    end

    self.intensity_points_json = values.to_json
    # Prevents recursive case with compute()
    if !already_computed
      self.compute()
    end
  end

  def as_json(options = {})
    {:id => self.id,
     :classname => self.class.name,
     :title => self.title,
     :description => self.description,
     :image_link => self.image_link,
     :youtube_id => self.youtube_id,
     :duration_sec => self.duration_sec,
     :intensity_points => self.intensity_points
    }
  end

  def to_svg(options = {})
    options.select! { |k, v| ['width', 'height'].include?(k) }
    options.merge!({:xmlns => "http://www.w3.org/2000/svg", 'xmlns:xlink'.to_sym => "http://www.w3.org/1999/xlink"})
    rects = self.to_percentRects
    builder = Nokogiri::XML::Builder.new do |xml|
      xml.svg(options) {
        rects.each do |rect|
          Rails.logger.info "rect = #{rect.to_s}"
          xml.rect(:x => "#{rect[:x]}%", :width => "#{rect[:w]}%", :y => "#{rect[:y]}%", :height => "#{rect[:h]}%", :fill => "#8ff", :stroke => "black", 'stroke-width'.to_sym => "2")
        end
      }
    end
    builder.to_xml
  end

  def to_percentRects(options = {})
    rects = []
    lastPoint = nil
    points = self.intensity_points

    if !self.duration_sec.nil?
      if points.length > 1
        (0..points.length-2).each do |i|
          rects << self.pointsToRect(points[i], points[i+1], options)
        end

        # Add possibly missing last point
        if points[points.length-1][:t] < self.duration_sec
          rects << self.pointsToRect(points[points.length-1], {:t => self.duration_sec, :i => points[points.length-1][:i]}, options)
        end
      elsif points.length == 1
        rects << self.pointsToRect(points[0], {:t => self.duration_sec, :i => points[0][:i]}, options)
      else # no points
        rects << self.pointsToRect({:t => 0, :i => STAGE_DEFAULT_INTENSITY}, {:t => self.duration_sec, :i => STAGE_DEFAULT_INTENSITY})
      end
    end

    rects
  end

  def pointsToRect(p1, p2, options = {})
    scale = options[:scale] || 100.0
    offset = options[:offset] || 0.0
    index = options[:index] || 0
    x1 = (offset * 100.0) + ((Float(p1[:t]) / self.duration_sec) * scale).round
    x2 = (offset * 100.0) + ((Float(p2[:t]) / self.duration_sec) * scale).round
    y = p1[:i] / 10
    {:x => x1, :w => (x2 - x1), :y => (100-y), :h => y, :index => index}
  end

  # Validates whether the value of the specified attribute matches the format of an URL,
  # as defined by RFC 2396. See URI#parse for more information on URI decompositon and parsing.
  #
  # This method doesn't validate the existence of the domain, nor it validates the domain itself.
  #
  # Allowed values include http://foo.bar, http://www.foo.bar and even http://foo.
  # Please note that http://foo is a valid URL, as well http://localhost.
  # It's up to you to extend the validation with additional constraints.
  #
  #   class Site < ActiveRecord::Base
  #     validates_format_of :url, :on => :create
  #     validates_format_of :ftp, :schemes => [:ftp, :http, :https]
  #   end
  #
  # ==== Configurations
  #
  # * :schemes - An array of allowed schemes to match against (default is [:http, :https])
  # * :message - A custom error message (default is: "is invalid").
  # * :allow_nil - If set to true, skips this validation if the attribute is +nil+ (default is +false+).
  # * :allow_blank - If set to true, skips this validation if the attribute is blank (default is +false+).
  # * :on - Specifies when this validation is active (default is :save, other options :create, :update).
  # * :if - Specifies a method, proc or string to call to determine if the validation should
  #   occur (e.g. :if => :allow_validation, or :if => Proc.new { |user| user.signup_step > 2 }).  The
  #   method, proc or string should return or evaluate to a true or false value.
  # * :unless - Specifies a method, proc or string to call to determine if the validation should
  #   not occur (e.g. :unless => :skip_validation, or :unless => Proc.new { |user| user.signup_step <= 2 }).  The
  #   method, proc or string should return or evaluate to a true or false value.
  #
  def validates_format_of_url(*attr_names)
    require 'uri/http'

    configuration = {:on => :save, :schemes => %w(http https)}
    configuration.update(attr_names.extract_options!)

    allowed_schemes = [*configuration[:schemes]].map(&:to_s)

    validates_each(attr_names, configuration) do |record, attr_name, value|
      begin
        uri = URI.parse(value)

        if !allowed_schemes.include?(uri.scheme)
          raise(URI::InvalidURIError)
        end

        if [:scheme, :host].any? { |i| uri.send(i).blank? }
          raise(URI::InvalidURIError)
        end

      rescue URI::InvalidURIError => e
        record.errors.add(attr_name, :invalid, :default => configuration[:message], :value => value)
        next
      end
    end
  end

  private

  def assign_segment_order(stage_segment)
    if stage_segment.order.nil?
      max_value = 1
      #max = self.stage_segments.order.max{|a,b| a.order <=> b.order }
      self.stage_segments.each do |stage_segment|
        if !stage_segment.order.nil? and stage_segment.order > max_value
          max_value = stage_segment.order
        end
      end
      stage_segment.order = max_value + 1
    end
  end

end
