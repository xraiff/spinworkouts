class SWS.Views.WorkoutPlayerModal extends SWS.Views.AbsSwsModal
  name: "playerModal"
  player: null
  template: JST['workoutPlayerModal']

  events:
    "click #details_btn": "handleDetails"
    "click #delete_btn": "handleDelete"
    "click #perform_workout_btn" : "handlePerform"
    "click #player_modal_dismiss_btn": "handleCancel"

  initialize: ->
    SWS.log "WorkoutPlayerModal::initialize"
    @render()

  handleDelete: (event) =>
    event.preventDefault()
    SWS.log "playerModal: handleDelete"
    new SWS.Views.MessageModal({
      message: "Are you sure you want to delete this workout?"
      ok: () =>
        SWS.log "handleDeleteLink: confirm"
    })

  handleDetails: (event) =>
    event.preventDefault()
    SWS.log "playerModal: handleDetails"
    @hide()
    $.pjax(
      container: '#inner-container'
      push: true
      url: "/workouts/#{@model.id}"
    )

  handlePerform: (event) =>
    event.preventDefault()
    SWS.log "playerModal: handlePerform"
    @hide()

    performance = SWS.Singletons.Performance.get()
    performance.loadWorkout(@model)
    $.pjax(
      container: '#inner-container'
      push: true
      url: "/workouts/#{@model.id}/perform"
    )

  render: () =>
    SWS.log "WorkoutPlayerModal::render"
    # Empty the modal first - in case anything has been left behind
    $('#modal').empty()

    duration = @model.get('duration_sec').toHHMMSS()
    isOwnWorkout = (@model.get('user_id') == @user.id)
    SWS.log "WorkoutPlayerModal:render, isOwnWorkout = #{isOwnWorkout}"
    $(@el).html(@template(model: @model, duration: duration, isOwnWorkout: isOwnWorkout))

    $('#modal').html(@el)
    @graph = new SWS.Views.IntensityWidget({model: @model})
    $('#player_modal_graph').html(@graph.el)
    $('#modal').modal({keyboard: false})
    @player = SWS.Singletons.ModalPlayerAdapter.get()
    @player.loadCollection(@model.stages, true)
    this

