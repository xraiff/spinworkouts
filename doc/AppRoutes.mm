<map version="0.9.0">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node COLOR="#000000" CREATED="1336495369567" ID="ID_1677993693" MODIFIED="1357488230826" TEXT="App Routes">
<font NAME="SansSerif" SIZE="20"/>
<hook NAME="accessories/plugins/AutomaticLayout.properties"/>
<node COLOR="#0033ff" CREATED="1336495396407" ID="ID_746235665" MODIFIED="1357484950523" POSITION="right" TEXT="sessions">
<edge STYLE="sharp_bezier" WIDTH="8"/>
<font NAME="SansSerif" SIZE="18"/>
<node COLOR="#00b439" CREATED="1336495407086" ID="ID_1149087977" MODIFIED="1336495408036" TEXT="new">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
<node COLOR="#990000" CREATED="1336495420406" ID="ID_669068018" MODIFIED="1336495447483" TEXT="format.html { render :layout =&gt; false  }">
<font NAME="SansSerif" SIZE="14"/>
</node>
</node>
<node COLOR="#00b439" CREATED="1336495408622" ID="ID_517763141" MODIFIED="1336495409571" TEXT="create">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
<node COLOR="#990000" CREATED="1336495428870" ID="ID_345663899" MODIFIED="1336495440507" TEXT="format.js { render :layout =&gt; false }">
<font NAME="SansSerif" SIZE="14"/>
</node>
</node>
<node COLOR="#00b439" CREATED="1336495410662" ID="ID_860709555" MODIFIED="1336495412211" TEXT="destroy">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
<node COLOR="#990000" CREATED="1336495449158" ID="ID_419211312" MODIFIED="1336495456307" TEXT="session[:user_id] = nil">
<font NAME="SansSerif" SIZE="14"/>
</node>
<node COLOR="#990000" CREATED="1336495457230" ID="ID_667271598" MODIFIED="1336495469020" TEXT="redirect_to root_url, notice: &quot;Signed out!&quot;">
<font NAME="SansSerif" SIZE="14"/>
</node>
</node>
<node COLOR="#00b439" CREATED="1336495412950" ID="ID_1648674718" MODIFIED="1336495414355" TEXT="failure">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
<node COLOR="#990000" CREATED="1336495470390" ID="ID_1265593072" MODIFIED="1336495477317" TEXT="format.js { render :layout =&gt; false }">
<font NAME="SansSerif" SIZE="14"/>
</node>
</node>
</node>
</node>
</map>
