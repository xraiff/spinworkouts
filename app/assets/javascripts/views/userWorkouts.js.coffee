class SWS.Views.UserWorkouts extends Backbone.View
  workouts: null

  constructor: (options) ->
    @cid = _.uniqueId('view');
    @_configure(options || {});

    # Set element to pre-existing element in constructor, so that
    # "delegate" can function appropriately
    @el = options.el || throw new Error("options.el not defined")
    @_ensureElement();
    @initialize.apply(this, arguments);
    @delegateEvents();

  initialize: (options = {}) ->
    if options.workouts
      @workouts = new SWS.Collections.Workouts(workouts)

      for workout in @workouts.models
        new SWS.Views.WorkoutCard({model: workout})