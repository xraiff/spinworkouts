class Segment < ActiveRecord::Base
  attr_accessible :title, :description, :duration, :heartrate, :is_duration_constant, :resistance, :user_id
  has_many :stage_segments
  has_many :stages, :through => :stage_segments  
  serialize :heartrate
  serialize :resistance
  
  has_many :segment_instructions, dependent: :delete_all
  has_many :instructions, :through => :segment_instructions

  belongs_to :user
  
  require 'json_friends'
  include JsonFriends
end
