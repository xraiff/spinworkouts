# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20130923003013) do

  create_table "comments", :force => true do |t|
    t.string   "commenter"
    t.text     "body"
    t.integer  "workout_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "identities", :force => true do |t|
    t.string   "name"
    t.string   "email"
    t.string   "password_digest"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "instructions", :force => true do |t|
    t.string   "text"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
    t.integer  "user_id"
  end

  add_index "instructions", ["user_id"], :name => "index_instructions_on_user_id"

  create_table "segment_instructions", :force => true do |t|
    t.integer  "segment_id"
    t.integer  "instruction_id"
    t.integer  "offset"
    t.datetime "created_at",     :null => false
    t.datetime "updated_at",     :null => false
  end

  add_index "segment_instructions", ["instruction_id"], :name => "fk_segment_instructions_instructions"
  add_index "segment_instructions", ["segment_id"], :name => "fk_segment_instructions_segments"

  create_table "segments", :force => true do |t|
    t.string   "title"
    t.string   "description"
    t.integer  "duration"
    t.boolean  "is_duration_constant"
    t.text     "heartrate"
    t.text     "resistance"
    t.integer  "instruction_id"
    t.datetime "created_at",           :null => false
    t.datetime "updated_at",           :null => false
    t.integer  "user_id"
  end

  add_index "segments", ["duration"], :name => "index_segments_on_duration"
  add_index "segments", ["user_id"], :name => "user_id"

  create_table "site_comments", :force => true do |t|
    t.string   "name"
    t.text     "body"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "stage_segments", :force => true do |t|
    t.integer  "stage_id"
    t.integer  "segment_id"
    t.integer  "order"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "stage_segments", ["segment_id"], :name => "fk_stage_segments_segments"
  add_index "stage_segments", ["stage_id"], :name => "fk_stage_segments_stages"

  create_table "stages", :force => true do |t|
    t.string   "title"
    t.string   "description"
    t.string   "image_link"
    t.string   "youtube_id"
    t.integer  "duration_sec"
    t.datetime "created_at",            :null => false
    t.datetime "updated_at",            :null => false
    t.integer  "user_id"
    t.text     "intensity_points_json"
    t.integer  "num_points"
    t.integer  "min"
    t.integer  "max"
    t.integer  "mean"
  end

  add_index "stages", ["num_points"], :name => "index_num_points_on_stages"
  add_index "stages", ["user_id"], :name => "index_stages_on_user_id"
  add_index "stages", ["youtube_id"], :name => "index_youtube_id_on_stages"

  create_table "tags", :force => true do |t|
    t.string   "name"
    t.integer  "workout_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "users", :force => true do |t|
    t.string   "provider"
    t.string   "uid"
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "email"
    t.boolean  "admin"
    t.boolean  "system_user", :default => false
  end

  add_index "users", ["uid"], :name => "index_uid_on_users"

  create_table "workout_stages", :force => true do |t|
    t.integer  "workout_id"
    t.integer  "stage_id"
    t.integer  "order"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "workout_stages", ["stage_id"], :name => "fk_workout_stages_stages"
  add_index "workout_stages", ["workout_id"], :name => "fk_workout_stages_workouts"

  create_table "workouts", :force => true do |t|
    t.string   "title"
    t.string   "description"
    t.integer  "user_id"
    t.integer  "duration_sec"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
  end

  add_index "workouts", ["user_id"], :name => "fk_workouts_users"
  add_index "workouts", ["user_id"], :name => "index_workouts_on_user_id"

  add_foreign_key "segment_instructions", "instructions", :name => "fk_segment_instructions_instructions"
  add_foreign_key "segment_instructions", "segments", :name => "fk_segment_instructions_segments"

  add_foreign_key "stage_segments", "segments", :name => "fk_stage_segments_segments"
  add_foreign_key "stage_segments", "stages", :name => "fk_stage_segments_stages"

  add_foreign_key "workout_stages", "stages", :name => "fk_workout_stages_stages"
  add_foreign_key "workout_stages", "workouts", :name => "fk_workout_stages_workouts"

end
