Given /^the system knows about the following user[s]?:$/ do |users|
  users.hashes.each do |user|
    factory_workout = FactoryGirl.create(:user, user)
  end
end

Given /^I am (signed|logged) out$/ do |dummy|
  visit "/sessions/destroy"
end

Given /^I am (signed|logged) in with provider "([^"]*)"$/ do |dummy,provider|
  visit "/auth/#{provider.downcase}"
end      

Given /^I (sign|log) in with provider "([^"]*)"$/ do |dummy,provider|
  visit "/auth/#{provider.downcase}"
end       

Given /^I (sign|log) in with invalid credentials to provider "([^"]*)"$/ do |dummy,provider|
  OmniAuth.config.mock_auth[provider.to_sym] = :invalid_credentials
  visit "/auth/#{provider.downcase}"
end        

