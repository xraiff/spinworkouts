require 'spec_helper'

describe UsersController do
  render_views
  #fixtures :session

  context "logged out" do
    describe "GET #show" do
      it "should assign the indicated user" do
        @user = FactoryGirl.create(:user)
        get :show, :id => @user.id
        assigns(:user).should eq(@user)
      end

      context "User has a couple of workouts" do
        before(:each) do
          @user = FactoryGirl.create(:user, :with_workouts, :number_of_workouts => 2)
          @w1 = @user.workouts[0]
          @w2 = @user.workouts[1]
        end

        it "should assign the workouts associated with the user" do
          get :show, :id => @user.id
          assigns(:workouts).should eq(@user.workouts)
        end

        it "should show the titles of each of the workouts" do
          get :show, :id => @user.id
          response.should contain(@w1.title)
          response.should contain(@w2.title)
        end
      end
    end
  end

  context "logged in" do
    before(:each) do
      @user = FactoryGirl.create(:user)
      session[:user_id] = @user.id
    end

    describe "GET #show" do
      #it "should return a workout" do
      #  Workout.should_receive(:find).and_return(@workout)
      #  get :show, :id => @workout.id
      #  response.headers['X-PJAX-URL'].should_not be_nil
      #end
    end
  end
end
