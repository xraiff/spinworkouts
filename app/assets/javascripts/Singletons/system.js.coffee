class SWS.Singletons.System

  instance = null

  class SystemPrivate extends Backbone.Model
    minIntensity: 100
    maxIntensity: 1000
    ga_account: 'UA-36010515-1'
    ga_domain: 'spinworkouts.com'
    ga_src: 'www.google-analytics.com/ga.js'
    statesToLog: ''

    initialize: (data) ->
      SWS.log "system initialize with"

    # Perhaps have the user object be a sub-class of this?
  #  set: (attrs, options) =>
  #    Backbone.Model.prototype.set.apply(this, arguments)
  #
  #    if attrs and attrs.workouts and @workouts
  #      @workouts.reset(attrs.workouts)

    setTimedMessage: (msg, options = {}) =>
      seconds = options.seconds || 2
      messageType = options.messageType || 'info'
      @set({'message': msg, 'messageType': messageType})
      @timedMessageHide(seconds)

    timedMessageHide: (seconds = 2) =>
      setTimeout(() =>
        @set('message', null)
      , seconds * 1000)

  @get: (data) ->
    instance ?= new SystemPrivate(data)



