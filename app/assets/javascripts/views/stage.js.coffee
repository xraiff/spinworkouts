class SWS.Views.Stage extends Backbone.View
  template: JST['stage']         
  
  events: 
    "click .deleteStage": "deleteStage"    
                 
  initialize: ->
    # Triggered when a model changes
    @model.on('change', @render, this)  

  render: ->
    $(@el).html(@template(stage: @model))
    this          
    
  deleteStage: (event) =>
    event.preventDefault() 
    @model.destroy(
      url: '/api/stages/' + @model.id
      success: (model, response) =>
        alert "success"
      error: (model, response) ->
        alert "failure"
        #responseObj = $.parseJSON(response.responseText)
    )
