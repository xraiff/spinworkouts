class SWS.Singletons.ModalPlayerAdapter

  instance = null

  class ModalPlayerAdapterPrivate extends SWS.Models.JwPlayerAdapter
    playerElement: "modalplayer"

  @get: (options) ->
    instance ?= new ModalPlayerAdapterPrivate(options)