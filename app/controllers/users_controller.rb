require 'net/http'
require 'uri'

class UsersController < ApplicationController
  #before_filter :except => [:index, :show] do |controller|
  #  controller.authenticate_user!(request.fullpath)
  #end
  before_filter :authenticate_user!, :except => [:index, :show]
  # rescue_from ActiveRecord::RecordNotFound, :with => :record_not_found

  # GET /users
  def index
    respond_to do |format|
      @users = User.paginate(:page => params[:page], :per_page => params[:per_page])
      format.html {
        render :layout => get_layout
      }
      format.json { @users.to_json }
    end
  end

  # GET /users/1
  def show
    @user = User.find(params[:id])
    @workouts = @user.workouts
    respond_to do |format|
      format.html {
        # See : https://github.com/defunkt/jquery-pjax/issues/85
        #       https://github.com/defunkt/jquery-pjax/commit/3311a9aa
        # To force pjax to understand the redirection... This *could* be done
        # for all url's actually
        response.headers['X-PJAX-URL'] = request.url
        render :layout => get_layout()
      }# show.html.erb
      format.json {
        render :json => @user
      }
    end
  end

  private

  def record_not_found
    render :text => "404 Not Found", :content_type => "text/html", :status => 404
  end
end
