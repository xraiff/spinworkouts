class Instruction < ActiveRecord::Base
  attr_accessible :text, :user_id

  has_many :segment_instructions
  has_many :segments, :through => :segment_instructions

  belongs_to :user
end
