class SWS.Views.NewWorkoutModal extends SWS.Views.AbsSwsModal
  stages_json: ''
  formName: "newWorkoutForm"
  formUrl: "/workouts"
  name: "NewWorkoutModal"
  stages_ids: ''
  template: JST['newWorkoutModal']

  events:
    "submit #newWorkoutForm": "handleFormSubmit"
    "click #newWorkoutCancel": "handleCancel"

  initialize: (options) ->
    super(options)
    @wrapperName = options.wrapperName ? '#modal'
    @stage_ids = options.stage_ids ? ''
    @stages_json = options.stages_json ? ''
    @render()

  handleFormSubmit: (event) =>
    event.preventDefault()

    # Empty possible errors from previous invocations
    $("##{@formName} .text-error").each(() ->
      $(this).empty()
    )

    ajaxObj =
      type: "POST"
      url: @formUrl
      data: $("##{@formName}").serialize()
      dataType: 'json'
      success: (data, textStatus, request) =>
        @hide()
        if @model
          @model.set(data)
      error: @ajaxErrorHandler

    jQuery.ajax(ajaxObj)

  render: () =>
    # Empty the modal first - in case anything has been left behind
    $('#modal').empty()

    $(@el).html(@template(
      model: @model
      stages_json: @stages_json
      stage_ids: @stage_ids
      title: @model and @model.get('title') || ''
      description: @model and @model.get('description') || ''
      formName: @formName
      message: @message))
    $('#modal').html(@el)
    $('#modal').modal({keyboard: false})
    SWS.index.modalShow('NewWorkoutModal::render')
    this
