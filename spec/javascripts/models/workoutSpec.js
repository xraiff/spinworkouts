describe("Workout model", function () {
  beforeEach(function() {          
    // SWS is initialized by sws.js.coffee
    stage1 = new SWS.Models.Stage({
      id: 1,
      title: "Workout Stage #1"
    });
    SWS.Collections.stages.add(stage1);
    stage2 = new SWS.Models.Stage({
      id: 2,
      title: "Workout Stage #2"
    });                               
    SWS.Collections.stages.add(stage2);
    stage3 = new SWS.Models.Stage({
      id: 3,
      title: "Workout Stage #3"
    });
    SWS.Collections.stages.add(stage3);
    this.workout = new SWS.Models.Workout({
      title: "My Workout",
      description: "a workout",
      stage_ids: [1,2,3]
    });
  });
 
  describe("when instantiated", function() {
    it("exhibits the attributes", function() {
      expect(this.workout.get("title")).toEqual("My Workout");
      expect(this.workout.get("description")).toEqual("a workout");
    });     
    
    it("exhibits the associated stages", function() {
      expect(this.workout.stages.length).toEqual(3);      
    });
  });
   
});