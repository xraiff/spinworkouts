-- MySQL dump 10.13  Distrib 5.5.28, for osx10.6 (i386)
--
-- Host: localhost    Database: spinworkouts_development
-- ------------------------------------------------------
-- Server version	5.5.28-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `comments`
--

DROP TABLE IF EXISTS `comments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `commenter` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `body` text COLLATE utf8_unicode_ci,
  `workout_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `comments`
--

LOCK TABLES `comments` WRITE;
/*!40000 ALTER TABLE `comments` DISABLE KEYS */;
/*!40000 ALTER TABLE `comments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `identities`
--

DROP TABLE IF EXISTS `identities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `identities` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password_digest` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `identities`
--

LOCK TABLES `identities` WRITE;
/*!40000 ALTER TABLE `identities` DISABLE KEYS */;
INSERT INTO `identities` VALUES (1,'xraiff','steve.raiff@gmail.com','$2a$10$sEnn6pK.fqpDGpUiUSxKCeyUJ29GUBmO.jJ2krHC.KqwleyKkDCFy','2013-01-16 21:24:55','2013-01-16 21:24:55');
/*!40000 ALTER TABLE `identities` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `instructions`
--

DROP TABLE IF EXISTS `instructions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `instructions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `text` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `instructions`
--

LOCK TABLES `instructions` WRITE;
/*!40000 ALTER TABLE `instructions` DISABLE KEYS */;
INSERT INTO `instructions` VALUES (1,'We will be doing a cadence count - 18-20 for :15 seconds','2013-01-16 21:23:57','2013-01-16 21:23:57'),(2,'Count your cadence in 3','2013-01-16 21:23:57','2013-01-16 21:23:57'),(3,'Count your cadence in 2','2013-01-16 21:23:57','2013-01-16 21:23:57'),(4,'Count your cadence in 1','2013-01-16 21:23:57','2013-01-16 21:23:57'),(5,'Count your cadence','2013-01-16 21:23:57','2013-01-16 21:23:57'),(6,'Stop counting','2013-01-16 21:23:57','2013-01-16 21:23:57'),(7,'Hold cadence steady','2013-01-16 21:23:57','2013-01-16 21:23:57'),(8,'Recover, :grab a wipe, grab a drink','2013-01-16 21:23:57','2013-01-16 21:23:57'),(9,'Pedal at 80%','2013-01-16 21:23:57','2013-01-16 21:23:57');
/*!40000 ALTER TABLE `instructions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `messages`
--

DROP TABLE IF EXISTS `messages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `messages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `message` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `messages`
--

LOCK TABLES `messages` WRITE;
/*!40000 ALTER TABLE `messages` DISABLE KEYS */;
/*!40000 ALTER TABLE `messages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `schema_migrations`
--

DROP TABLE IF EXISTS `schema_migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `schema_migrations` (
  `version` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  UNIQUE KEY `unique_schema_migrations` (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `schema_migrations`
--

LOCK TABLES `schema_migrations` WRITE;
/*!40000 ALTER TABLE `schema_migrations` DISABLE KEYS */;
INSERT INTO `schema_migrations` VALUES ('20121124143540');
/*!40000 ALTER TABLE `schema_migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `segment_instructions`
--

DROP TABLE IF EXISTS `segment_instructions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `segment_instructions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `segment_id` int(11) DEFAULT NULL,
  `instruction_id` int(11) DEFAULT NULL,
  `offset` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_segment_instructions_instructions` (`instruction_id`),
  KEY `fk_segment_instructions_segments` (`segment_id`),
  CONSTRAINT `fk_segment_instructions_segments` FOREIGN KEY (`segment_id`) REFERENCES `segments` (`id`),
  CONSTRAINT `fk_segment_instructions_instructions` FOREIGN KEY (`instruction_id`) REFERENCES `instructions` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `segment_instructions`
--

LOCK TABLES `segment_instructions` WRITE;
/*!40000 ALTER TABLE `segment_instructions` DISABLE KEYS */;
INSERT INTO `segment_instructions` VALUES (1,1,1,NULL,'2013-01-16 21:23:57','2013-01-16 21:23:57'),(2,1,2,NULL,'2013-01-16 21:23:57','2013-01-16 21:23:57'),(3,1,3,NULL,'2013-01-16 21:23:57','2013-01-16 21:23:57'),(4,1,4,NULL,'2013-01-16 21:23:57','2013-01-16 21:23:57'),(5,1,5,NULL,'2013-01-16 21:23:57','2013-01-16 21:23:57'),(6,1,6,NULL,'2013-01-16 21:23:57','2013-01-16 21:23:57'),(7,2,7,NULL,'2013-01-16 21:23:57','2013-01-16 21:23:57'),(8,3,1,NULL,'2013-01-16 21:23:57','2013-01-16 21:23:57'),(9,3,2,NULL,'2013-01-16 21:23:57','2013-01-16 21:23:57'),(10,3,3,NULL,'2013-01-16 21:23:57','2013-01-16 21:23:57'),(11,3,4,NULL,'2013-01-16 21:23:57','2013-01-16 21:23:57'),(12,3,5,NULL,'2013-01-16 21:23:57','2013-01-16 21:23:57'),(13,3,6,NULL,'2013-01-16 21:23:57','2013-01-16 21:23:57'),(14,4,8,NULL,'2013-01-16 21:23:57','2013-01-16 21:23:57'),(15,5,9,NULL,'2013-01-16 21:23:57','2013-01-16 21:23:57');
/*!40000 ALTER TABLE `segment_instructions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `segments`
--

DROP TABLE IF EXISTS `segments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `segments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `couch_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `duration` int(11) DEFAULT NULL,
  `is_duration_constant` tinyint(1) DEFAULT NULL,
  `heartrate` text COLLATE utf8_unicode_ci,
  `resistance` text COLLATE utf8_unicode_ci,
  `instruction_id` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_segments_on_duration` (`duration`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `segments`
--

LOCK TABLES `segments` WRITE;
/*!40000 ALTER TABLE `segments` DISABLE KEYS */;
INSERT INTO `segments` VALUES (1,'Hill Climb, Cadence count 80 RPM',NULL,'80028b64505b9e4dde8ef9239a383bae',28,1,'---\n- 140\n- 148\n','---\n- 5\n',NULL,'2013-01-16 21:23:57','2013-01-16 21:23:57'),(2,'Hold cadence',NULL,'80028b64505b9e4dde8ef9239a384aab',30,0,'---\n- 140\n- 140\n','---\n- 5\n',NULL,'2013-01-16 21:23:57','2013-01-16 21:23:57'),(3,'Hill Climb, Cadence count 80 RPM',NULL,'80028b64505b9e4dde8ef9239a385317',28,1,'---\n- 140\n- 148\n','---\n- 5\n',NULL,'2013-01-16 21:23:57','2013-01-16 21:23:57'),(4,'Rest-Recover',NULL,'80028b64505b9e4dde8ef9239a3862b6',20,0,'---\n- 140\n- 120\n','---\n- 1\n',NULL,'2013-01-16 21:23:57','2013-01-16 21:23:57'),(5,'80% cadence',NULL,'80028b64505b9e4dde8ef9239a3864ec',30,0,'---\n- 140\n- 160\n','---\n- 7\n',NULL,'2013-01-16 21:23:57','2013-01-16 21:23:57');
/*!40000 ALTER TABLE `segments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `site_comments`
--

DROP TABLE IF EXISTS `site_comments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `site_comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `body` text COLLATE utf8_unicode_ci,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `site_comments`
--

LOCK TABLES `site_comments` WRITE;
/*!40000 ALTER TABLE `site_comments` DISABLE KEYS */;
/*!40000 ALTER TABLE `site_comments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stage_segments`
--

DROP TABLE IF EXISTS `stage_segments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stage_segments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `stage_id` int(11) DEFAULT NULL,
  `segment_id` int(11) DEFAULT NULL,
  `order` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_stage_segments_segments` (`segment_id`),
  KEY `fk_stage_segments_stages` (`stage_id`),
  CONSTRAINT `fk_stage_segments_stages` FOREIGN KEY (`stage_id`) REFERENCES `stages` (`id`),
  CONSTRAINT `fk_stage_segments_segments` FOREIGN KEY (`segment_id`) REFERENCES `segments` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stage_segments`
--

LOCK TABLES `stage_segments` WRITE;
/*!40000 ALTER TABLE `stage_segments` DISABLE KEYS */;
INSERT INTO `stage_segments` VALUES (1,1,3,NULL,'2013-01-16 21:23:58','2013-01-16 21:23:58'),(2,1,2,NULL,'2013-01-16 21:23:58','2013-01-16 21:23:58'),(3,1,3,NULL,'2013-01-16 21:23:58','2013-01-16 21:23:58'),(4,1,2,NULL,'2013-01-16 21:23:58','2013-01-16 21:23:58'),(5,1,4,NULL,'2013-01-16 21:23:58','2013-01-16 21:23:58'),(6,2,5,NULL,'2013-01-16 21:23:58','2013-01-16 21:23:58'),(7,2,4,NULL,'2013-01-16 21:23:58','2013-01-16 21:23:58'),(8,2,5,NULL,'2013-01-16 21:23:58','2013-01-16 21:23:58'),(9,2,4,NULL,'2013-01-16 21:23:58','2013-01-16 21:23:58'),(10,2,5,NULL,'2013-01-16 21:23:58','2013-01-16 21:23:58'),(11,2,4,NULL,'2013-01-16 21:23:58','2013-01-16 21:23:58'),(12,2,5,NULL,'2013-01-16 21:23:58','2013-01-16 21:23:58'),(13,2,4,NULL,'2013-01-16 21:23:58','2013-01-16 21:23:58');
/*!40000 ALTER TABLE `stage_segments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stages`
--

DROP TABLE IF EXISTS `stages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `image_link` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `youtube_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `duration` time DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_youtube_id_on_stages` (`youtube_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stages`
--

LOCK TABLES `stages` WRITE;
/*!40000 ALTER TABLE `stages` DISABLE KEYS */;
INSERT INTO `stages` VALUES (1,'Cycling Warmup','Cycling Warmup','http://img.youtube.com/vi/Xc-IpsWlS5M/2.jpg','Xc-IpsWlS5M',NULL,'2013-01-16 21:23:57','2013-01-16 21:23:57'),(2,'Hill climb endurance 72-80RPM','Hill climb endurance 72-80RPM','http://img.youtube.com/vi/nQBPMouBaDA/2.jpg','nQBPMouBaDA',NULL,'2013-01-16 21:23:58','2013-01-16 21:23:58');
/*!40000 ALTER TABLE `stages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tags`
--

DROP TABLE IF EXISTS `tags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tags` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `workout_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tags`
--

LOCK TABLES `tags` WRITE;
/*!40000 ALTER TABLE `tags` DISABLE KEYS */;
/*!40000 ALTER TABLE `tags` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `provider` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `uid` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `admin` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_uid_on_users` (`uid`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'identity','1','xraiff','2013-01-16 21:24:55','2013-01-16 21:24:55','steve.raiff@gmail.com',0);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `workout_stages`
--

DROP TABLE IF EXISTS `workout_stages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `workout_stages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `workout_id` int(11) DEFAULT NULL,
  `stage_id` int(11) DEFAULT NULL,
  `order` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_workout_stages_stages` (`stage_id`),
  KEY `fk_workout_stages_workouts` (`workout_id`),
  CONSTRAINT `fk_workout_stages_workouts` FOREIGN KEY (`workout_id`) REFERENCES `workouts` (`id`),
  CONSTRAINT `fk_workout_stages_stages` FOREIGN KEY (`stage_id`) REFERENCES `stages` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `workout_stages`
--

LOCK TABLES `workout_stages` WRITE;
/*!40000 ALTER TABLE `workout_stages` DISABLE KEYS */;
INSERT INTO `workout_stages` VALUES (1,1,1,NULL,'2013-01-16 21:23:58','2013-01-16 21:23:58'),(2,1,2,1,'2013-01-16 21:23:58','2013-01-16 21:23:58');
/*!40000 ALTER TABLE `workout_stages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `workouts`
--

DROP TABLE IF EXISTS `workouts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `workouts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `couch_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `duration_sec` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_workouts_users` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `workouts`
--

LOCK TABLES `workouts` WRITE;
/*!40000 ALTER TABLE `workouts` DISABLE KEYS */;
INSERT INTO `workouts` VALUES (1,'Workout #2','Workout #2','80028b64505b9e4dde8ef9239a382c7f',0,0,'2013-01-16 21:23:57','2013-01-16 21:23:57');
/*!40000 ALTER TABLE `workouts` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2013-01-16 13:28:29
