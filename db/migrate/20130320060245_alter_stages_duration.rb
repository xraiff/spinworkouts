class AlterStagesDuration < ActiveRecord::Migration
  def up
    change_column :stages, :duration, :integer
    rename_column :stages, :duration, :duration_sec
  end

  def down
  end
end
