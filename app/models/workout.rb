class Workout < ActiveRecord::Base
  attr_accessible :description, :title, :user_id, :stages, :stages_attributes
  has_many :workout_stages, :before_add => [:assign_stage_order], dependent: :delete_all
  has_many :stages, :through => :workout_stages

  before_save :set_duration_sec

  belongs_to :user
  validates :title, :length => {:minimum => 5, :maximum => 255}

  scope :all_with_stage, -> { joins(:stages).where("stage_id is not null").uniq(:id) }

  require 'json_friends'
  require 'json'
  include JsonFriends

  def as_json(options = {})
    {:id => self.id,
     :classname => self.class.name,
     :title => self.title,
     :description => self.description,
     :duration_sec => self.duration_sec,
     :user_id => self.user_id,
     :stages => self.stages
    }
  end

  def json_to_stages(data_json)
    stages_data = JSON.parse(data_json, :symbolize_names => true)
    stages_data.each do |stage_data|
      if stage_data[:id]
        # if id is set, ignore passed-in data
        stage = Stage.find(stage_data[:id])
      else
        stage = Stage.create!(stage_data)
        if stage_data[:intensity_points]
          stage.intensity_points = stage_data[:intensity_points]
          stage.save!
        end
      end
      self.stages << stage unless stage.nil?
    end
    self
  end

  def duration_sec
    self.duration()
  end

  def duration
    durations = stages.map(&:duration_sec)
    if durations.include?(nil)
      nil
    else
      durations.inject(0, :+)
    end
  end

  def images
    items = []
    stages.each do |stage|
      if !stage.image_link.nil?
        items.push stage.image_link
      end
    end
    items
  end

  def set_duration_sec
    self.duration_sec = self.duration()
  end

  def to_svg(options = {})
    options.select! { |k, v| ['width', 'height'].include?(k) }
    options.merge!({:xmlns => "http://www.w3.org/2000/svg", 'xmlns:xlink'.to_sym => "http://www.w3.org/1999/xlink"})
    Rails.logger.info "options = #{options}"
    rects = self.to_percentRects
    builder = Nokogiri::XML::Builder.new do |xml|
      xml.svg(options) {
        rects.each do |rect|
          Rails.logger.info "rect = #{rect.to_s}"
          xml.rect(:x => "#{rect[:x]}%", :width => "#{rect[:w]}%", :y => "#{rect[:y]}%", :height => "#{rect[:h]}%", :fill => "#8ff", :stroke => "black", 'stroke-width'.to_sym => "2")
        end
      }
    end
    builder.to_xml
  end

  def to_percentRects()
    rects = []
    if !self.duration_sec.nil?
      total_duration = Float(self.duration_sec)
      position = 0.0

      stages.each_with_index do |stage, index|
        offset = position / total_duration
        options = {:scale => ((stage.duration_sec / total_duration) * 100.0), :offset => offset, :index => index}
        position += stage.duration_sec
        rects += stage.to_percentRects(options)
      end
    end

    rects
  end

  def <=>(other)
    @duration_sec <=> other.duration_sec
  end

  private

  def assign_stage_order(workout_stage)
    if workout_stage.order.nil?
      max_value = 1
      self.workout_stages.each do |workout_stage|
        if !workout_stage.order.nil? and workout_stage.order > max_value
          max_value = workout_stage.order
        end
      end
      workout_stage.order = max_value + 1
    end
  end

end
