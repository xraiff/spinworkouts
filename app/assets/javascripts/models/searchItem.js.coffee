# A SearchItem may or may not have Stage or Workout content...
# It could be considered a metaclass.
class SWS.Models.SearchItem extends Backbone.Model
  description: null
  duration_sec: null
  image_link: null
  intensity_points: null
  pub_date: null
  title: null
  youtube_id: null


# Perhaps have the user object be a sub-class of this?
  set: (attrs, options) =>
    SWS.log "SearchItem, set: #{JSON.stringify(attrs)}"
    Backbone.Model.prototype.set.apply(this, arguments)