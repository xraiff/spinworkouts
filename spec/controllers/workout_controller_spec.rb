require 'spec_helper'

describe WorkoutsController do
  render_views

  #describe "routing" do
  #
  #
  #end

  #fixtures :session

  #Html tests
  #it "should successfully log in with a good login" do
  #  post 'create'
  #  flash[:notice].should_not be_nil
  #  response.should redirect_to
  #end
  context "logged out" do
    before(:each) do
      @workout = FactoryGirl.create(:workout)
    end

    describe "GET #new" do
      it "should return unauthorized with xhr" do
        xhr :get, :new
        response.status.should == 401
      end
    end

    describe "GET #show" do
      it "should return a workout" do
        Workout.should_receive(:find).and_return(@workout)
        get :show, :id => @workout.id
      end

      it "should return json when requested" do
        xhr :get, :show, {:id => @workout.id, :format => :json}
        response.should be_success
        response.body.should eq(@workout.to_json)
      end

      it "should set the X-PJAX-URL header when set" do
        request.env['HTTP_X_PJAX'] = true
        Workout.should_receive(:find).and_return(@workout)
        get :show, :id => @workout.id
        response.headers['X-PJAX-URL'].should_not be_nil
      end
    end
  end

  context "logged in" do
    before(:each) do
      @user = FactoryGirl.create(:user)
      session[:user_id] = @user.id
    end

    context "with predefined workout(s)" do
      before(:each) do
        @workout = FactoryGirl.create(:workout)
      end
      describe "GET #index" do
        it "should paginate through the workouts" do
          #workout_model = mock_model(Workout).as_null_object
          Workout.stub(:paginate).and_return([@workout])
          Workout.should_receive(:paginate)
          get :index
        end
      end

      describe "GET #show" do
        it "should return a workout" do
          Workout.should_receive(:find).and_return(@workout)
          get :show, :id => @workout.id
        end

        it "should set the X-PJAX-URL header when set" do
          request.env['HTTP_X_PJAX'] = true
          Workout.should_receive(:find).and_return(@workout)
          get :show, :id => @workout.id
          response.headers['X-PJAX-URL'].should_not be_nil
        end
      end

      #When passing an id of an existing workout to new, it should copy the old one
      describe "GET #new/id" do
        it "should a copy workout" do
          get :new, :id => @workout.id
          assigns(:workout).should eq(@workout)
        end

        it "should not fail if a bad id is passed" do
          expect {
            get :new, :id => '3487980205679'
          }.to_not raise_error
        end
      end
    end

    describe "GET #new" do
      before(:each) do
        request.env['HTTP_X_PJAX'] = true
      end

      it "should return a modal" do
        request.env['HTTP_X_PJAX'] = true
        get :new
        response.should render_template(:new)
      end

      it "should set stages if good stage_ids passed in" do
        s1 = FactoryGirl.create(:stage)
        s2 = FactoryGirl.create(:stage)
        s3 = FactoryGirl.create(:stage)
        get :new, :stage_ids => [s1.id, s2.id, s3.id]
        assigns(:stages)
        assigns(:stages).count.should == 3
      end

      it "should not set stages if bogus stage_ids passed in" do
        get :new, :stage_ids => [1, 3, 4]
        assigns(:stages).size.should == 0
      end
    end

    describe "GET #add_stage/stage_id" do
      context "user has one or more workouts" do
        before(:each) do
          request.env['HTTP_X_PJAX'] = true
          @stage = FactoryGirl.create(:stage)
          @user.workouts << FactoryGirl.create(:workout)
        end

        it "should assign the corresponding stage variable" do
          get :add_stage, :stage_id => @stage.id
          assigns(:stage).should eq(@stage)
        end
      end
    end

    describe "POST #add_stage/stage_id" do
      before(:each) do
        @stage = FactoryGirl.create(:stage)
      end

      # When user has no workouts, a workout should be created for them, and the stage(s) added to it
      context "user has no workouts" do
        it "should create a workout for the user" do
          expect {
            post :add_stage, {:stage_id => @stage.id}
          }.to change { @user.workouts.count }.by(1)
        end

        it "should fail if too short a workout title was passed in" do
          expect {
            post :add_stage, {:stage_id => @stage.id, :workout_title => 'xyz'}
            response.should render_template(:new)
          }.to change { @user.workouts.count }.by(0)
        end

        it "should create a workout for the user with the title as specified" do
          expect {
            post :add_stage, {:stage_id => @stage.id, :workout_title => 'xyz 123'}
          }.to change { @user.workouts.count }.by(1)
        end
      end

      context "user doesn't own workout being posted to" do
        it "should return unauthorized" do
          other_user = FactoryGirl.create(:user, :with_workouts, :number_of_workouts => 1)
          post :add_stage, {:stage_id => @stage.id, :workout_id => other_user.workouts.first.id}
          response.status.should == 401
        end
      end

      context "user has one or more workouts" do
        before(:each) do
          @user.workouts << FactoryGirl.create(:workout)
          @workout = @user.workouts[0]
        end

        it "should append the stage to the workout" do
          expect {
            post :add_stage, {:stage_id => @stage.id, :workout_id => @workout.id}
          }.to change { @workout.stages.count }.by(1)
        end
      end
    end

    describe "POST #create_xhr" do
      it "should set title, description accordingly" do
        xhr :post, :create, :workout => {:title => 'workout123', :description => 'this is a new workout'}
        w = Workout.find_by_title('workout123')
        w.should_not be_nil
        w.title.should == 'workout123'
        w.description.should == 'this is a new workout'
      end

      it "should fail and return json error message if too short a workout title was passed in" do
        expect {
          xhr :post, :create, :workout => {:title => 'xyz', :description => 'this is a new workout'}
          response.should contain("short")
          response.status.should == 400
        }.to change { @user.workouts.count }.by(0)
      end

      it "should create a new workout for the user" do
        xhr :post, :create, :workout => {:title => 'workout234', :description => 'this is a new workout'}
        w = Workout.find_by_title('workout234')
        w.should_not be_nil
        w.description.should == 'this is a new workout'
      end

      it "should create a new workout for the user, including new stages if no id's specified" do
        stages = [FactoryGirl.build(:stage)]
        #stages = {[{:title => stage.title,
        #         :description => stage.description,
        #         :intensity_points => stage.intensity_points,
        #         :duration_sec => stage.duration_sec,
        #         :image_link => stage.image_link,
        #         :youtube_id => stage.youtube_id}]}
        expect {
          xhr :post, :create, {
              :workout => {:title => 'new workout-xymox',
                           :description => 'this is a new workout' },
              :stages_json => stages.to_json
          }
          response.should be_success
        }.to change { @user.workouts.count }.by(1)
        w = Workout.find_by_title('new workout-xymox')
        w.should_not be_nil
        w.stages.count.should == 1
        w.stages[0].title.should eq(stages[0].title)
        w.stages[0].id.should_not == stage.id
      end


      it "should append stages to new workout if stage id's were passed in" do
        s1 = FactoryGirl.create(:stage)
        s2 = FactoryGirl.create(:stage)
        s3 = FactoryGirl.create(:stage)
        data = {:title => 'workoutXYZ', :description => 'this is a new workout',
                :stages => [{:id => s1.id}, {:id => s2.id}, {:id => s3.id}]}
        expect {
          xhr :post, :create, :data_json => data.to_json
          response.should be_success
        }.to change { @user.workouts.count }.by(1)
        w = Workout.find_by_title('workoutXYZ')
        w.should_not be_nil
        w.stages.count.should == 3
      end

      it "should create new stages as appropriate as well as append stage id's passed in" do
        stage = FactoryGirl.create(:stage)
        s1 = FactoryGirl.create(:stage)
        s2 = FactoryGirl.create(:stage)
        data = { :title => 'new workout-xymox2',
                 :description => 'this is a new workout',
                 :stages => [{:id => s1.id},
                     {:title => stage.title,
                      :description => stage.description,
                      :intensity_points => stage.intensity_points,
                      :duration_sec => stage.duration_sec,
                      :image_link => stage.image_link,
                      :youtube_id => stage.youtube_id},
                     {:id => s2.id}] }
        expect {
          xhr :post, :create, :data_json => data.to_json
        }.to change { @user.workouts.count }.by(1)
        w = Workout.find_by_title('new workout-xymox2')
        w.should_not be_nil
        w.stages.count.should == 3
        w.stages.map { |s| s.id}.should include(s1.id)
        w.stages.map { |s| s.id}.should include(s2.id)
      end
    end

    describe "POST #create" do
      it "should set title, description accordingly" do
        post :create, {:workout => {:title => 'workout123', :description => 'this is a new workout'}}
        w = Workout.find_by_title('workout123')
        w.should_not be_nil
        w.title.should == 'workout123'
        w.description.should == 'this is a new workout'
      end

      it "should render :new template if too short a workout title was passed in" do
        expect {
          post :create, {:workout => {:title => 'xyz', :description => 'this is a new workout'}}
          response.should render_template(:new)
        }.to change { @user.workouts.count }.by(0)
      end

      it "should create a new workout for the user" do
        post :create, {:workout => {:title => 'workout234', :description => 'this is a new workout'}}
        w = Workout.find_by_title('workout234')
        w.should_not be_nil
        w.description.should == 'this is a new workout'
      end

      it "should create a new workout for the user" do
        expect {
          post :create, {:workout => {:title => 'new workout', :description => 'this is a new workout'}}
        }.to change { @user.workouts.count }.by(1)
      end

      it "should create new stages if any of the workout stages have changed" do
        pending
      end
    end

    #Updates should only operate on the workout themselves, not underlying stages ... that's for create

    describe "POST #update" do
      it "should return error status if the user is not the owner" do
        workout = FactoryGirl.create(:workout, :with_stages, {:user_id => @user.id})
        user2 = FactoryGirl.create(:user)
        session[:user_id] = user2.id
        xhr :post, :update, {:id => workout.id, :workout => {:title => 'test 1234', :description => 'a description'}}
        response.status.should == 400
      end

      it "should not update the stages that are owned by this user" do
        workout = FactoryGirl.create(:workout, :with_non_owned_stages, {:user_id => @user.id})

        stage = FactoryGirl.create(:stage, {:user_id => @user.id})
        title_before = stage.title
        wss = FactoryGirl.create(:workout_stage, workout: workout)
        wss.stage = stage
        workout.workout_stages << wss
        workout.save!

        xhr :post, :update, {:id => workout.id, :workout => {:stages_attributes => {:id => stage.id, :title => title_before + 'XYZ'}}}
        response.should redirect_to(workout_path(workout.id))
        s = Stage.find(stage.id)
        s.title.should eq(title_before)
      end

      it "should not update stages that are not owned by this user" do
        workout = FactoryGirl.create(:workout, :with_non_owned_stages, {:user_id => @user.id})
        w = Workout.find(workout.id)
        stage_before = w.stages[0]

        xhr :post, :update, {:id => workout.id, :workout => {:stages_attributes => {:id => stage_before.id, :title => stage_before.title + 'XYZ'}}}
        response.should redirect_to(workout_path(workout.id))
        w2 = Workout.find(workout.id)
        stage_after = w.stages[0]
        stage_after.id.should == stage_before.id
        stage_after.title.should eq(stage_before.title)
      end
    end

    describe "DELETE workout" do
      it "should remove the appropriate workout when indicated" do
        workout = FactoryGirl.create(:workout, :with_non_owned_stages, {:user_id => @user.id})

        xhr :post, :destroy, {:id => workout.id, :format => :json}
        response.should be_success
        expect {
          Workout.find(workout.id)
        }.to raise_error
      end

      it "should not remove associated stages" do
        workout = FactoryGirl.create(:workout, :with_non_owned_stages, {:user_id => @user.id})
        stages = workout.stages

        xhr :post, :destroy, {:id => workout.id, :format => :json}
        response.should be_success
        expect {
          stages.each do |stage|
            Stage.find(stage.id)
          end
        }.to_not raise_error
      end
    end
  end
end
