require 'spec_helper'

describe MediaIntensitySet do
  before :each do
    @mid = FactoryGirl.build(:media_intensity_set_1)
  end

  #it "Adding data should change the num_points" do
  #  @mid.data << '{"t":1.0, "i": 800}'
  #end


  describe "#to_svg" do
    before(:each) do
      @mid = FactoryGirl.build(:media_intensity_set_0, {:datapoints => [{:t => 10, :i => 20}, {:t => 100, :i => 200}]})
    end

    it "should return an xml-parsable svg document of the data" do
      svg = @mid.to_svg
      svg.should_not be_nil
      expect {
        Nokogiri::XML(svg) { |config| config.strict }
      }.to_not raise_error
    end

    it "should set the height, width to the passed in parameters" do
      svg = @mid.to_svg({:width => 200, :height => 100})
      svg.should_not be_nil
      expect {
        doc = Nokogiri::XML(svg) { |config| config.strict }
        doc.root['width'].should eq("200")
        doc.root['height'].should eq("100")
      }.to_not raise_error
    end
  end

  describe "#max" do
    before(:each) do
      @mid = FactoryGirl.build(:media_intensity_set_0, {:datapoints => [{:t => 10, :i => 20}, {:t => 100, :i => 200}]})
    end

    it "should return the maximum point in the data set" do
      @mid.max.should == 200
    end

    it "should update when the data is changed" do
      @mid.datapoints = [{:t => 5, :i => 300}, {:t => 10, :i => 50}]
      @mid.max.should == 300
    end
  end

  describe "#min" do
    before(:each) do
      @mid = FactoryGirl.build(:media_intensity_set_0, {:datapoints => [{:t => 10, :i => 20}, {:t => 100, :i => 200}]})
    end

    it "should return the minimum point in the data set" do
      @mid.min.should == 20
    end

    it "should update the number of points when the data is changed" do
      @mid.datapoints = [{:t => 5, :i => 300}, {:t => 10, :i => 50}]
      @mid.min.should == 50
    end
  end

  describe "#num_points" do
    before(:each) do
      @mid = FactoryGirl.build(:media_intensity_set_0, {:datapoints => [{:t => 10, :i => 20}, {:t => 100, :i => 200}]})
    end

    it "should return the number of points in the data set" do
      @mid.num_points.should == 2
    end

    it "should update the number of points when the data is changed" do
      @mid.datapoints = [{:t => 5, :i => 300}, {:t => 10, :i => 50}, {:t => 20, :i => 40}]
      @mid.num_points.should == 3
    end
  end

  describe "#stage_id" do
    it "should respond to the stage_id method" do
      mis = MediaIntensitySet.new
      mis.should respond_to(:stage_id)
    end

    it { should be_accessible :stage_id }
  end

  describe "#compute" do
    before(:each) do
      @mis = FactoryGirl.create(:media_intensity_set_1)
      @datapoints = @mis.datapoints
    end

    it "should alter the datapoints when the duration_sec changes accordingly" do
      @mis.duration_sec = @mis.duration_sec * 2
      @mis.compute()
      datapoints = @mis.datapoints
      datapoints.should_not be_nil
      datapoints.length.should == @datapoints.length
      datapoints[datapoints.length - 1][:t].should == @datapoints[@datapoints.length - 1][:t] * 2
      datapoints[datapoints.length - 1][:i].should == @datapoints[@datapoints.length - 1][:i]
    end
  end
end
