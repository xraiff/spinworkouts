class SWS.Notifier
  instance = null

  class PrivateClass
    constructor: (@target, @message) ->

  @set_notify: (target, message) =>
    instance = new PrivateClass(target, message)

  @notify: (target) =>
    if !instance or instance.target != target
      return false

    oldMessage = $(target).html()
    oldFontWeight = $(target).css('font-weight')
    $(target).fadeOut('slow', () ->
      $(target).html(oldMessage)
      $(target).css('font-weight', oldFontWeight)
      $(this).show()
    )
    $(target).html(instance.message)
    $(target).css('font-weight', 'bold')
    # Clear the singleton
    instance = null

  @log: (msg) =>
    jQuery.ajax(
      url: "/log?q=#{encodeURIComponent(msg)}"
    )

