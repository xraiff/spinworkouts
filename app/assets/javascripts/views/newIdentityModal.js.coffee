class SWS.Views.NewIdentityModal extends SWS.Views.AbsSwsModal
  formName: 'newIdentityForm'
  formUrl: "/auth/identity/register"
  name: "NewIdentityModal"
  template: JST['newIdentityModal']

  events:
    "submit #newIdentityForm" : "handleFormSubmit"
    "click #newIdentityCancel" : "handleCancel"

  initialize: (options) ->
    super(options)
    @model = SWS.Singletons.SystemUser.get()
    @standardRender()
