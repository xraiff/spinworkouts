Given /^the system knows about the following workout[s]?:$/ do |seed_workouts|
  @workouts = Array.new
  seed_workouts.hashes.each do |workout|
    stages_str = workout['stages']
    workout.delete('stages')
    factory_workout = FactoryGirl.create(:workout, workout)
    if !stages_str.nil? && !stages_str.empty?
      stages = stages_str.split(/,/)
      stages.each do |stage_str|
        stage_array = stage_str.split(/ : /)                
        stage = FactoryGirl.create(:stage, {:title => stage_array[0].strip, :image_link => stage_array[1].strip})
        factory_workout.stages.push stage
      end
    end
    @workouts.push factory_workout
  end
end

#And I should see a link to workout detail for "Workout 1"
Then /^(?:|I )should see a link to workout detail for "([^"]*)"$/ do |title|
  #workout = @workouts.find_by_title(title).should
  response.should have_link(title)
end