Feature: Workout Detail
  In order to interact with a workout.
  When I visit a workout detail page, I need to see all of the relevant information for the workout

  Scenario: Display all of the workout stages pertaining to a workout
    Given the system knows about a workout with 15 workout stages.
    When I am on the workout detail page.
    Then I should see all of the workout stages listed.


