require 'spec_helper'

describe Stage do
  @longtext='now is the time for all good men to come to the aid of our country. now is the time for all good men to come to the aid of our country. now is the time for all good men to come to the aid of our country. now is the time for all good men to come to the aid of our country. now is the time for all good men to come to the aid of our country.  now is the time for all good men to come to the aid of our country. now is the time for all good men to come to the aid of our country. now is the time for all good men to come to the aid of our country. now is the time for all good men to come to the aid of our country.'

  describe "#create!" do
    it "should create successfully" do
      expect {
        s = Stage.create!({"duration_sec" => [],
                           "image_link" => "https://i.ytimg.com/vi/dkr7uK12UoA/default.jpg",
                           "youtube_id" => "dkr7uK12UoA",
                           "title" => "Stayin' Fit: Spin Class (with Briant Mitchell)",
                           "description" => "Sorry for the long pre-roll, to view a trimmed version of this video see http://youtu.be/KLlWYscNJi0 Otherwise, you can skip forward to 0:20."})
      }.to_not raise_error
    end
  end

  describe "#to_percentRects" do
    it "should return an empty array when no intensity points exist and the duration is not set" do
      s = FactoryGirl.build(:stage, {:intensity_points => [], :duration_sec => nil})
      rects = s.to_percentRects
      rects.should be_an_instance_of(Array)
      rects.count.should == 0
    end

    it "should return one rectangle of the default intensity when no intensity points exist, but the duration does" do
      expect {
        s = Stage.create!({"duration_sec" => [],
                           "image_link" => "https://i.ytimg.com/vi/dkr7uK12UoA/default.jpg",
                           "youtube_id" => "dkr7uK12UoA",
                           "title" => "Stayin' Fit: Spin Class (with Briant Mitchell)",
                           "description" => "Sorry for the long pre-roll, to view a trimmed version of this video see http://youtu.be/KLlWYscNJi0 Otherwise, you can skip forward to 0:20."})
        result = s.to_percentRects()
        result.should be_an_instance_of(Array)
      }.to_not raise_error
    end

    it "should return one rectangle if only one datapoint exists" do
      s = FactoryGirl.build(:stage, {:intensity_points => [{:t => 0, :i => 500}]})
      rects = s.to_percentRects
      rects.should be_an_instance_of(Array)
      rects.count.should == 1
    end

    it "should return one rectangle if two datapoints exists" do
      s = FactoryGirl.build(:stage)
      s.intensity_points = [{:t => 0, :i => 500}, {:t => s.duration_sec, :i => 400}]
      rects = s.to_percentRects
      rects.should be_an_instance_of(Array)
      rects.count.should == 1
    end

    it "should return two rectangles if two datapoints exist, last one before the end of the stage" do
      s = FactoryGirl.build(:stage, {:intensity_points => [{:t => 0, :i => 500}, {:t => 100, :i => 400}]})
      rects = s.to_percentRects
      rects.should be_an_instance_of(Array)
      rects.count.should == 2
    end

    it "should return two rectangles if three datapoints exists" do
      s = FactoryGirl.build(:stage)
      s.intensity_points = [{:t => 0, :i => 500}, {:t => 100, :i => 300}, {:t => s.duration_sec, :i => 500}]
      rects = s.to_percentRects
      rects.should be_an_instance_of(Array)
      rects.count.should == 2
    end
  end

  describe "#intensity_points" do
    before(:each) do
      FactoryGirl.reload
      a = [{:t => 0, :i => 200}]
      @stage = FactoryGirl.create(:stage, {:intensity_points => a})
    end

    it "should respond to intensity_points" do
      s = Stage.new
      s.should respond_to(:intensity_points)
    end

    it "should persist intensity_points" do
      st2 = Stage.find(@stage.id)
      st2.intensity_points.should_not be_nil
    end

    it "should return a hash of the data" do
      a = [{:t => 0, :i => 200}]
      stage = FactoryGirl.build(:stage, {:intensity_points => a})
      stage.intensity_points.should == a
    end
  end

  describe "#intensity_points=" do
    before(:each) do
      a = [{:t => 0, :i => 200}]
      @stage = FactoryGirl.create(:stage, {:intensity_points => a})
    end

    it "should update intensity_points_json, mean, max, min when data is assigned" do
      @stage.intensity_points = [{:t => 1, :i => 2}, {:t => 10, :i => 100}]
    end

    it "should throw an error if t is missing from any hash array elements" do
      expect {
        @stage.intensity_points = [{:i => 2}, {:t => 10, :i => 100}]
      }.to raise_error(KeyError)
    end

    it "should throw an error if i is missing from any hash array elements" do
      expect {
        @stage.intensity_points = [{:t => 2}, {:t => 10, :i => 100}]
      }.to raise_error(KeyError)
    end

    it "should throw an error if t is not an integer" do
      expect {
        @stage.intensity_points = [{:t => 2.3, :i => 5}, {:t => 10, :i => 100}]
      }.to raise_error(RangeError)
    end

    it "should throw an error if i is not an integer" do
      expect {
        @stage.intensity_points = [{:t => 2, :i => 5.6}, {:t => 10, :i => 100}]
      }.to raise_error(RangeError)
    end

    it "should throw an error if t is less than 0" do
      expect {
        @stage.intensity_points = [{:t => -2, :i => 5}, {:t => 10, :i => 100}]
      }.to raise_error(RangeError)
    end

    it "should throw an error if i is less than 0" do
      expect {
        @stage.intensity_points = [{:t => 2, :i => -5}, {:t => 10, :i => 100}]
      }.to raise_error(RangeError)
    end
  end

  describe "#new!" do
    it "should fail to create a stage without a youtube_id" do
      expect {
        Stage.new!()
      }.to raise_error
    end
  end

  describe "#segments" do
    it "should respond to segments" do
      s = Stage.new
      s.should respond_to(:segments)
    end
  end


  describe "#title" do
    it { should_not accept_values_for(:title, nil, "a", @longtext) }
    it { should accept_values_for(:title, "This is a title", "This is also a title") }
  end

  describe "#user_id" do
    it "should respond to user_id" do
      s = Stage.new
      s.should respond_to(:user_id)
    end
    it { should be_accessible :user_id }
  end

  describe "#workouts" do
    it "should respond to workouts" do
      s = Stage.new
      s.should respond_to(:workouts)
    end
  end

  describe "#workout_stage" do
    it "should respond to workout_stages" do
      s = Stage.new
      s.should respond_to(:workout_stages)
    end
  end
end
