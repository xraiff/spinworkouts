class SWS.Singletons.SearchPlayerAdapter

  instance = null

  class SearchPlayerAdapterPrivate extends SWS.Models.JwPlayerAdapter
    playerElement: "searchplayer"

  @get: (options) ->
    instance ?= new SearchPlayerAdapterPrivate(options)