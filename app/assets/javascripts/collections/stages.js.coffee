class SWS.Collections.Stages extends Backbone.Collection
  model: SWS.Models.Stage
  url: '/api/stages'
