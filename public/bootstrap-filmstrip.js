var extensionMethods = {
    number: 0,

    slide: function (type, next) {

        var $active = this.$element.find('.item.active')
            , $next = next || $active[type]()
            , isCycling = this.interval
            , direction = type == 'next' ? 'left' : 'right'
            , fallback  = type == 'next' ? 'first' : 'last'
            , that = this
            , e

        // SUR: hack to prevent looping
        // ToDo: only show relevant arrows - implement MyCarousel inheritance
        if ((direction == 'right' && !$next.length) ||
            (direction == 'left' && !$next.length))
            return;

        this.sliding = true

        isCycling && this.pause()

        $next = $next.length ? $next : this.$element.find('.item')[fallback]()

        e = $.Event('slide', {
            relatedTarget: $next[0]
            , direction: direction
        })

        if ($next.hasClass('active')) return

        if (this.$indicators && this.$indicators.length) {
            this.$indicators.find('.active').removeClass('active')
            this.$element.one('slid', function () {
                var $nextIndicator = $(that.$indicators.children()[that.getActiveIndex()])
                $nextIndicator && $nextIndicator.addClass('active')
            })
        }

        if ($.support.transition && this.$element.hasClass('slide')) {
            this.$element.trigger(e)
            if (e.isDefaultPrevented()) return
            $next.addClass(type)
            $next[0].offsetWidth // force reflow
            $active.addClass(direction)
            $next.addClass(direction)
            this.$element.one($.support.transition.end, function () {
                $next.removeClass([type, direction].join(' ')).addClass('active')
                $active.removeClass(['active', direction].join(' '))
                that.sliding = false
                setTimeout(function () { that.$element.trigger('slid') }, 0)
            })
        } else {
            this.$element.trigger(e)
            if (e.isDefaultPrevented()) return
            $active.removeClass('active')
            $next.addClass('active')
            this.sliding = false
            this.$element.trigger('slid')
        }

        isCycling && this.cycle()

        return this
    }
};

$.extend(true, $[ "fn" ][ "carousel" ][ "Constructor" ].prototype, extensionMethods);