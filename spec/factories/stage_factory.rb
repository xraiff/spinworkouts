FactoryGirl.define do
  factory :stage, class: Stage do
    sequence :title do |n|
      "Stage #{n}"
    end
    ignore do
      yt_ids ['Xc-IpsWlS5M', 'GU7s351-Dfs', 'nF_owBP6MtA', 'mC7BRhUjIvc', '5mD3yJ60mrk', 'YOVxp5c_lwM', 'E4XKo1_tD9c']
      yt_durations {{'Xc-IpsWlS5M' => 298,
                     'GU7s351-Dfs' => 2159,
                     'nF_owBP6MtA' => 4781,
                     'mC7BRhUjIvc' => 3813,
                     '5mD3yJ60mrk' => 755,
                     'YOVxp5c_lwM' => 3348,
                     'E4XKo1_tD9c' => 3412}}
      points {{'Xc-IpsWlS5M' => [{t:0, i:100}, {t:50, i:500}, {t:100, i:700}, {t:298, i:500}],
               'GU7s351-Dfs' => [{t:0, i:200}],
               'nF_owBP6MtA' => [{t:0, i:500}, {t:400, i:1000}, {t:2500, i:300}, {t:4000, i:500}],
               'mC7BRhUjIvc' => [{t:0, i:300}, {t:500, i:800}, {t:3000, i:100}],
               '5mD3yJ60mrk' => [{t:0, i:800}, {t:500, i:600}, {t:300, i: 400}],
               'YOVxp5c_lwM' => [{t:0, i:100}, {t:300, i:300}, {t:500, i: 500}, {t:1500, i:700}],
               'E4XKo1_tD9c' => [{t:0, i:100}, {t:300, i:300}, {t:2000, i:400}, {t:2700, i:300}]}}
    end
    youtube_id { yt_ids[rand(yt_ids.length)] }
    image_link { "http://img.youtube.com/vi/#{youtube_id}/2.jpg" }
    duration_sec { yt_durations[youtube_id] }
    intensity_points { points[youtube_id] }

    description {
      if rand(2) == 1
        'ipsem lorem dolsem, robetussin, parthenon, excaliber, mortician, Edward de Bono'
      else
        ''
      end
    }

    after(:create) { |stage| stage.compute() }

    trait :with_segments do
      ignore do
        number_of_segments 6
      end

      after(:create) do |stage, evaluator|
        segments = FactoryGirl.create_list(:segment, evaluator.number_of_segments, :with_instructions, user: stage.user)
        segments.each do |segment|
          ss = FactoryGirl.create(:stage_segment, stage: stage)
          ss.segment = segment
          stage.stage_segments << ss
        end
      end
    end
  end

  factory :stage_segment, class: StageSegment do
    sequence :order do |n|
      n
    end
  end
end