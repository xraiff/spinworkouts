class AddUserToInstruction < ActiveRecord::Migration
  def change
    add_column :instructions, :user_id, :integer
    add_index :instructions, :user_id
  end
end
