# This class is responsible for communication to and from the player
class SWS.Models.JwPlayerAdapter extends Backbone.Model
  lastItemIndex: null
  loaded: false
  playerElement: "mediaplayer"
  playState: null
  playlistItemIndex: null
  position: 0
  seconds: 0

  initialize: (options) =>
    jwplayer(@playerElement).setVolume(0)
#    jwplayer(@playerElement).resize(400, 300)
    @loaded = true

  getDuration: () =>
    jwplayer(@playerElement).getDuration()

  #----------------------------------------------------------
  getItem: () =>
    jwplayer(@playerElement).getPlaylistItem()

  isPlaying: () =>
    @playerState == 'playing' || @playerState == 'buffering'

  isPaused: () =>
    @playerState == 'paused'

  loadCollection: (collection) =>
    jwplayer(@playerElement).setup(@collectionToPlaylist(collection))
    @loadHandlers()

  loadItem: (item, playWhenReady) =>
    jwplayer(@playerElement).setup(@itemToObj(item, playWhenReady))
    @loadHandlers()

  loadHandlers: () =>
    jwplayer(@playerElement).onPlay(@onPlay)
    jwplayer(@playerElement).onPause(@onPause)
    jwplayer(@playerElement).onBuffer(@onBuffer)
    jwplayer(@playerElement).onTime(@onTime)
    jwplayer(@playerElement).onPlaylistItem(@onPlaylistItem)

  minimize: () =>
    if @loaded
      jwplayer(@playerElement).pause(true)
      jwplayer(@playerElement).resize(400, 0)

  normalizeObject: (obj) =>
    dummy = for param, value of obj
      if not value then obj[param] = ""
    obj

  resize: (width, height) =>
    jwplayer(@playerElement).resize(width, height)

  setItem: (item_num, play) =>
    @playlistItemIndex = item_num
    jwplayer(@playerElement).playlistItem(item_num)
    if play
      @play()

  play: () =>
    SWS.Notifier.log("#{@playerElement}:play()")
    try
      jwplayer(@playerElement).play(true)
    catch err
      SWS.Notifier.log(err.message)

  pause: () =>
    jwplayer(@playerElement).pause(true)

  togglePlay: () =>
    jwplayer(@playerElement).play()

  #ToDo: test for a changed playlist item index, update internal state accordingly
  onPlay: (oldState, test) =>
    @playerState = 'playing'
    @trigger("#{@playerElement}:statechange", {state: 'playing'})
    @testItem()

  onPause: (oldState) =>
    @playerState = 'paused'
    @trigger("#{@playerElement}:statechange", {state: 'paused'})

  onBuffer: (oldState) =>
    @playerState = 'buffering'
    @trigger("#{@playerElement}:statechange", {state: 'buffering'})
    @testItem()

  onPlaylistItem: (event) =>
    @trigger("#{@playerElement}:item", {index: event.index})

  onTime: (event) =>
    oldSeconds = @seconds
    @position = event.position
    @seconds = Math.round(@position)
    if Math.round(@position) != oldSeconds
      @trigger("#{@playerElement}:seconds", {position: @position})
    @trigger("#{@playerElement}:time", {position: @position})

  testItem: () =>
    currentItem = jwplayer(@playerElement).getPlaylistItem()
    if currentItem.mediaid != @lastItemIndex
      @lastItemIndex = currentItem.mediaid
      @trigger("#{@playerElement}:itemchange", {index: parseInt(currentItem.mediaid)})

  # Set the state from the player
  setState: (newState) =>
    if @playerState isnt newState
      @playerState = newState
      @trigger("#{@playerElement}:statechange")

  collectionToPlaylist: (collection) =>
    my_array = []
    index = 0
    collection.each((item) =>
      item = this.itemToObj(item)
      item.mediaid = index
      my_array.push item
      index++
    )
    { playlist: my_array }

  itemToObj: (item, playWhenReady = false) ->
    {file: 'http://www.youtube.com/watch?v=' + item.get('youtube_id'), image: item.get('image_link'), title: item.get('title'), autostart: playWhenReady}
