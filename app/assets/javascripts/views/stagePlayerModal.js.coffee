class SWS.Views.StagePlayerModal extends SWS.Views.AbsSwsModal
  isWorkoutView: false
  name: "playerModal"
  player: null
  stageIndex: 0
  template: JST['stagePlayerModal']

  events:
    "click #add_workout_btn": "handleAddToWorkout"       # from absSwsModal
    "click #add_perform_btn": "handleAddToPerform"
    "click #new_workout_btn": "handleNewWorkout"
    "click #perform_btn" : "handlePerform"
    "click #dismiss_btn": "handleCancel"
    "click #save_btn": "handleSave"
    "click #skip_to_btn": "handleSkipTo"

  initialize: (options) ->
    @isWorkoutView = options.isWorkoutView ? false
    @stageIndex = options.stageIndex ? 0
    @render()

  handleAddToPerform: (event) =>
    SWS.log "handleAddToPerform"
    event.preventDefault()
    @hide()
    if !@performance.isPerforming()
      return false
    @performance.addStage(@model)

  handleNewWorkout: (event) =>
    event.preventDefault()
    new SWS.Views.NewWorkoutModal({stage_ids: [@model.id]})

  handlePerform: (event) =>
    event.preventDefault()
    @hide()
    Backbone.history.navigate("/stages/#{@model.id}/perform", {trigger: true})

  handleSave: (event, callback = null) =>
    event.preventDefault()
    data_obj =
      workout_id: $('#workoutSelect').val()

    ajaxObj =
      type: "POST"
      url: "/workouts/add_stage/#{@model.id}"
      data: data_obj
      dataType: 'json'
      success: (data, textStatus, request) =>
        @hide()
        @system.setTimedMessage "Saved"
      error: @ajaxErrorHandler

    jQuery.ajax(ajaxObj)

  handleSkipTo: (event) =>
    event.preventDefault()
    @hide()
    @performance.skipTo(@stageIndex)

  render: () =>
    # Empty the modal first - in case anything has been left behind
    $('#modal').empty()

    duration = @model.get('duration_sec').toHHMMSS()
    $(@el).html(@template(model: @model, duration: duration, performance: @performance, user: @user, isWorkoutView: @isWorkoutView))

    $('#modal').html(@el)
    @graph = new SWS.Views.IntensityWidget({model: @model})
    $('#player_modal_graph').html(@graph.el)
    $('#modal').modal({keyboard: false})
    SWS.index.modalShow("StagePlayerModal::render")
    @player = SWS.Singletons.ModalPlayerAdapter.get()
    @player.loadItem(@model, true)
    this