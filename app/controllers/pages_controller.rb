class PagesController < ApplicationController

  def home
    @workouts = Workout.all_with_stage.paginate({:page => 1, :per_page => Rails.application.config.homepage_workouts_to_show})
    @title = "Most Recent Workouts"

    #@workouts = @workouts.select { |w| !w.duration_sec.nil? }

    # TODO: pull all relevant stages that associate with the workouts that were pulled
    stage_ids = []
    @workouts.each do |workout|
      if workout['stage_ids']
        stage_ids |= workout['stage_ids']
      end
    end
    stage_ids.sort!
    @stages = Stage.where(:id => stage_ids)

    render :layout => get_layout()
  end

  def contact
    @title = "Contact"
  end

  def about
    @title = "About"
  end

  def help
	@title = "Help"
  end
end
