class SWS.Views.NewSessionModal extends SWS.Views.AbsSwsModal
  formName: "newSessionForm"
  formUrl: "/auth/identity/callback"
  message: null
  name: "NewSessionModal"
  template: JST['newSessionModal']

  events:
    "submit #newSessionForm" : "handleFormSubmit"
    "click #createAccountButton" : "handleCreateAccount"
    "click #newSessionCancel" : "handleCancel"

  initialize: (options) ->
    super(options)
    @message = options.message ? ''
    @model = SWS.Singletons.SystemUser.get()
    @render()

  handleCreateAccount: (event) =>
    event.preventDefault()
    new SWS.Views.NewIdentityModal(modalType: 'slave')
    false

  render: () =>
    # Empty the modal first - in case anything has been left behind
    $('#modal').empty()

    $(@el).html(@template(model: @model, formName: @formName, message: @message))
    $('#modal').html(@el)
    $('#modal').modal({keyboard: false})
    SWS.index.modalShow('NewSessionModal::render')
    this