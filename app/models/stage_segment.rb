class StageSegment < ActiveRecord::Base
  attr_accessible :order, :segment_id, :stage_id
  
  belongs_to :stage
  belongs_to :segment
end
