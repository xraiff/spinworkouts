class SWS.Models.Workout extends Backbone.Model
  urlRoot: '/workouts'
  title: ''
  description: ''
  stages: null
  
  initialize: (data) ->
    @stages = new SWS.Collections.Stages()
    if data
      @title = data.title or ''
      @description = data.description or ''
      if data.stages
        @stages.reset(data.stages)

  duration: () =>
    duration = 0.0
    @stages.each((stage) =>
      duration += stage.get('duration_sec')
    )
    duration

  #ToDo: enhance graphRects to include the color of each section.
  # lay down a "base curve" for stages without intensity
  # create alternating background masks to distinquish between the stages
  #    => BUT (at some point), we want to trigger off the pressing of each
  #    => background mask... hmmm

  graphRects: () =>
    rects = []
    total_duration = @duration()
    position = 0.0
    index = 0

    @stages.each((stage) =>
      offset = position / total_duration
      options =
        scale: stage.get('duration_sec') / total_duration,
        offset: offset
        index: index
      position += stage.get('duration_sec')
      rects = rects.concat(stage.graphRects(options))
      index++
    )
    rects