class SWS.Singletons.Performance

  instance = null

  class PerformancePrivate extends Backbone.Model
    currentStage: null
    currentStageIndex: 0
    end_time: null
    isShowGraph: true
    mivStep: 50      # roughly, 5% stepsize
    start_date: null
    url: '/performance'
    user: null
    performancePlayer: null
    performancePlayerState: 'init'
    performanceWorkout: null
    teaser: null

    # <If a stage with MIS>
    #   If a stage has an MIS, then it is displayed
    #   The current segment flashes

    #   There shall be an option to copy/create a new stage
    #
    #   If you are the author, you should have the option to modify the MIS
    #
    # <If a stage does not have an MIS>
    #   If a user adjusts the intensity up/down, datapoints are created/adjusted in the corresponding stage's MIS
    #
    #   If the adjustment is < 5 seconds since the last adjustment, then the last segment is modified accordingly.
    #   [Perhaps we have a >| button which forcibly creates a new segment from NOW.]

    # How about a mini-dialog inside the IntensityWidget which says:
    #   Use the up/down buttons to set the intensity of the current
    #   stage, along with a "dismiss" button which hides the widget

    # If the intensity widget is hidden, should the intensity be displayed
    # at all?

    initialize: () ->
      SWS.log "PerformancePrivate initialize"
      @user = SWS.Singletons.SystemUser.get()
      @performanceWorkout = new SWS.Models.Workout()
      @set('mode', 'init', false)    # init, tease, loaded, perform, edit (perform implied)
      @set('seconds', 0, false)
      @set('position', 0, false)
      @set('miv', 200, false)

      @performancePlayer = SWS.Singletons.PerformancePlayerAdapter.get()
      @performancePlayer.on("#{@performancePlayer.playerElement}:seconds", @handlePlayerSeconds, this)
      @performancePlayer.on("#{@performancePlayer.playerElement}:time", @handlePlayerTime, this)
      @performancePlayer.on("#{@performancePlayer.playerElement}:statechange", @handlePlayerStateChange, this)
      @performancePlayer.on("#{@performancePlayer.playerElement}:item", @handlePlayerItem, this)

    addStage: (model, load = false) =>
      rawJsonStage = model.toJSON()
      newStage = new SWS.Models.Stage(rawJsonStage)
      @performanceWorkout.stages.push newStage
      @performanceWorkout.set('stages', @performanceWorkout.stages)
      @teaser = null

      if (@get('mode') is 'init' or @get('mode') is 'tease') and load
        @currentStage = newStage
        @set('miv', @currentStage.getIntensity(@get('position')))
        @set('mode', 'loaded')
        @performancePlayer.loadItem(newStage, true)

      if load
        Backbone.history.navigate("/performance/show", {trigger: true})

      @trigger('change')

    copyCurrentStage: () =>
      SWS.log "copyCurrentStage"
      if @currentStage
        rawJsonStage = @currentStage.toJSON()
        rawJsonStage.datapoints = null
        rawJsonStage.media_intensity_set = null
        @newStage = new SWS.Models.Stage(rawJsonStage)
        @currentStage = @newStage
        @trigger('change')

    currentPercentPosition: () =>
      index = 0
      position = 0
      while index < @currentStageIndex
        position += @performanceWorkout.stages.at(index).get('duration_sec')
        index++

      position += @get('position')
      val = @roundHundredths(position * 100.0 / @performanceWorkout.duration())
      val

    decIntensity: () =>
      newMiv = @get('miv')
      minIntensity = SWS.Singletons.System.get().minIntensity

      if @currentStage
        newMiv = @currentStage.adjIntensity(@mivStep * -1, @get('position'))
        @currentStage.set('id', null)
      else
        newMiv -= 50
        if newMiv < minIntensity
          newMiv = minIntensity
      if @get('miv') != newMiv
        @set('miv', newMiv)
      @get('miv')

    graphRects: () =>
      if @performanceWorkout
        @performanceWorkout.graphRects()

    handlePlayerItem: (event) =>
      SWS.log "handlePlayerItem: #{event.index}"
      @currentStage = @performanceWorkout.stages.at(event.index)
      @currentStageIndex = event.index
      @set('position', 0)

    handlePlayerStateChange: (event) =>
      SWS.log "Performance: onPlayerStateChange"
      @performancePlayerState = event.state
      if @isPlaying()
        if @get('mode') isnt 'perform' and @get('mode') isnt 'edit'
          @set('mode', 'perform')

    handlePlayerSeconds: (event) =>
      @set('seconds', @get('seconds') + 1, false)
      @set('miv', @currentStage.getIntensity(@get('position')))
      # trigger these together...
      @trigger('change:seconds')

    handlePlayerTime: (event) =>
      @set('position', event.position)

    isLoaded: () =>
      mode = @get('mode')
      mode is 'loaded' or mode is 'perform' or mode is 'edit'

    isPlaying: () =>
      @performancePlayerState == 'buffering' || @performancePlayerState == 'playing'

    isPaused: () =>
      @performancePlayerState == 'paused'

    isPerforming: () =>
      mode = @get('mode')
      mode is 'perform' or mode is 'edit'

    isRightNavStages: () =>
      true

    incIntensity: () =>
      newMiv = @get('miv')
      maxIntensity = SWS.Singletons.System.get().maxIntensity

      if @currentStage
        newMiv = @currentStage.adjIntensity(@mivStep, @get('position'))
        @currentStage.set('id', null)
      else
        newMiv += 50
        if newMiv > maxIntensity
          newMiv = maxIntensity
      if @get('miv') != newMiv
        @set('miv', newMiv)
      @get('miv')

    intensity: () =>
      @get('miv')

    loadTeaser: (model) =>
      SWS.log "performance: tease"
      @teaser = model
      @set('mode', 'tease')

    # Copy the workout so that it's modifiable.  The user may even add the same workout multiple times,
    # so we have to be ready for that...
    #
    # What do you do if there is NO intensity curve for a stage?  There should be an "update function"
    # ToDo: test to see if the workout has an intensity curve (at all), set a status accordingly
    loadWorkout: (workout) =>
      SWS.log "loadWorkout"
      rawJsonWorkout = workout.toJSON()
      # Since we don't have deepCopy....
      @performanceWorkout = new SWS.Models.Workout(rawJsonWorkout)
      @currentStage = @performanceWorkout.stages.at(0)

      if @performanceWorkout.stages.length
        @performancePlayer.loadCollection(@performanceWorkout.stages)
      @teaser = null
      @set('mode', 'loaded')

    print_perform_button: () =>
      if @isPerforming()
        @get('seconds').toHHMMSS()
      else
        'Start'

    reset: () =>
      @start_date = new Date()
      @time = 0

    roundHundredths: (value) ->
      Math.round(value * 100) / 100

    setIntensityStep: (step) =>
      @mivStep = step

    skipTo: (index) =>
      if @isPerforming() and index < @performanceWorkout.stages.length
        @performancePlayer.setItem(index)

  @get: ()->
    instance ?= new PerformancePrivate({isShowGraph: true})