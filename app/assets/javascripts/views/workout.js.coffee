class SWS.Views.Workout extends Backbone.View
  performance: null
  stageTemplate: JST['performCard']

  events:
    "click .card_link": "handleSelectStage"
    "click .add_stage": "handleAddStage"

  constructor: (options) ->
    @cid = _.uniqueId('view')
    @_configure(options || {})

    # Set element to pre-existing element in constructor, so that
    # "delegate" can function appropriately
    @el = options.el
    @_ensureElement()
    @initialize.apply(this, arguments)
    @delegateEvents()

  initialize: (options = {}) ->
    @performance = SWS.Singletons.Performance.get()
    @render()

  handlePerformWorkout: (event) =>
    SWS.log "handlePerformWorkout"
    event.preventDefault()
    @performance.loadWorkout(@model)

  # If performing, then launch a modal with this stage preview,
  # otherwise, load in performance player
  handleSelectStage: (event) =>
    SWS.log "handleSelectStage"
    event.preventDefault()
    stage_index = $(event.currentTarget).data('index')
    stage = @model.stages.at(stage_index)
    @playerModal = new SWS.Views.StagePlayerModal({model: stage, isWorkoutView: true, stageIndex: stage_index})
    SWS.Singletons.ModalPlayerAdapter.get().loadItem stage

  id: () =>
    @model.get('id')

  loadStage: (stage_index, play = false) =>
    @highlightStage {index: stage_index}
    if @player
      @player.setItem stage_index, play

  playChange: (event) =>
    SWS.log "workout:playChange event received"

  playStage: (event) =>
    SWS.log "workout: playStage"
    SWS.Notifier.log 'workout: playStage'
    stage_index = $(event.currentTarget).data('index')
    url = "workouts/#{@model.get('id')}/play/#{stage_index}"

    # If we're already at this URL, we simply toggle the play/pause state
    try
      if Backbone.history.fragment is url and @player
        @player.togglePlay()
      else
        Backbone.history.navigate(url, {trigger: true})
    catch err
      SWS.Notifier.log err.message

  render: =>
    $('#bottom_nav').show()
    this

  renderStages: =>
    stages_html = ''

    for stage in @model.stages.models
      stages_html += @stageTemplate(model: stage, cardType: 'stage', performing: true)

    $('#inner-container').html(stages_html)
    @delegateEvents()
