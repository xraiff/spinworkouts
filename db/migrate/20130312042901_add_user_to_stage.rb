class AddUserToStage < ActiveRecord::Migration
  def change
    add_column :stages, :user_id, :integer
    add_index :stages, :user_id
  end
end
