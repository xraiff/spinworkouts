require 'view_spec_helper'

describe "workouts/show.html.erb" do
  before(:each) do
    @workout = FactoryGirl.create(:workout)
    assign(:workout, @workout)
    render
  end

  it "should render title in html" do
    response.should contain(@workout.title)
  end

  it "should render description in html" do
    response.should contain(@workout.description)
  end

  it "should render workout json in data element" do
    response.should have_xpath(".//div[@data-workout]")
    #rendered.should contain("workout-data")
  end
end