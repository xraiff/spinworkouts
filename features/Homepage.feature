Feature: Homepage
  In order to understand what Spinworkouts is about,
  When I visit the home page, I need to see a set of things that pertain to Spinworkouts.

Scenario: Display a set of featured workouts
  Given the system knows about the following workouts:
    | title      |  description | duration_sec | stages |
    | Workout 1  |  An aerobic workout  | 10 |  Stage 1 :  http://cdn.sheknows.com/articles/2011/11/spinning.jpg, Stage 2 :  http://yourenglishlessons.files.wordpress.com/2010/03/spinning-i-276x300.jpg, Stage 3 :  http://fairwoodmartialarts.com/wp-content/uploads/2012/02/spinning-class-3.jpg |
    | Workout 2  |  A red-zone workout  | 20 |  Stage 1 :  http://cdn.sheknows.com/articles/2011/11/spinning.jpg, Stage 2 :  http://yourenglishlessons.files.wordpress.com/2010/03/spinning-i-276x300.jpg, Stage 3 :  http://fairwoodmartialarts.com/wp-content/uploads/2012/02/spinning-class-3.jpg |
    | Workout 3  |  A recovery workout  | 30 |  Stage 1 :  http://cdn.sheknows.com/articles/2011/11/spinning.jpg, Stage 2 :  http://yourenglishlessons.files.wordpress.com/2010/03/spinning-i-276x300.jpg, Stage 3 :  http://fairwoodmartialarts.com/wp-content/uploads/2012/02/spinning-class-3.jpg |
  When I am on the homepage
  Then I should see "Workout 1"
  And I should see a link to workout detail for "Workout 1"
#  And I should see a duration for "Workout 1" of 10
  And I should see "Workout 2"
  And I should see a link to workout detail for "Workout 2"
#  And I should see a duration for "Workout 1" of 20
  And I should see "Workout 3"
  And I should see a link to workout detail for "Workout 3"
#  And I should see a duration for "Workout 1" of 30


