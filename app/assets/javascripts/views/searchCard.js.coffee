class SWS.Views.SearchCard extends Backbone.View
  index: 0
  template: JST['searchCard']

  events:
    "click .card_link" : "handleCardLink"

  initialize: (options = {}) ->
    @index = options.index || 0

  handleCardLink: (event) =>
    SWS.log "SearchCard::handleCardLink"
    event.preventDefault()
    switch @model.constructor.name
      when "Stage"
        new SWS.Views.StagePlayerModal({model: @model})
      when "Workout"
        new SWS.Views.WorkoutPlayerModal({model: @model})
      else
        SWS.log "SearchItem Found"
        new SWS.Views.YouTubePlayerModal({model: @model})

  render: () =>
    performing = SWS.Singletons.Performance.get().isPerforming()
    duration = @model.get('duration_sec').toHHMMSS() if @model.get('duration_sec')?
    graph = null
    cardHtml = ''

    if performing
      cardHtml += "<div class='col-lg-12 col-md-12 col-sm-12 col-xs-12 perform_card_border'>"
    else
      cardHtml += "<div class='col-lg-3 col-md-4 col-sm-6<% end %>col-xs-12 perform_card_border'>"

    cardHtml += @template({model: @model, duration: duration})
    graph_id = "card_#{@model.get('id')}_graph"
    cardHtml += "<div id='#{graph_id}' class='card_description'>"

    if typeof @model.graphRects == 'function'
      graph = new SWS.Views.IntensityWidget({model: @model})
      cardHtml += graph.el.innerHTML

    cardHtml += '</div></a></div>'
    $(@el).html(cardHtml)

#    $(@el).html(@template({model: @model, performing: performing, graph: graph, duration: duration}))
    this
