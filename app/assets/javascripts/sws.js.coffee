window.SWS =
  Models: {}
  Collections: {}
  Views: {}
  Routers: {}
  Singletons: {}
  Helpers: {}
  index: null
  Config:
    defaultTarget: '#inner-container'

  log: (msg) ->
    if SWS.Config.log
      console and console.log(msg)

  init: ->
    SWS.log "SWS.init() start"
    # Initiatialize views thereafter
    user_data = $('#current_user').data('user')
    user = SWS.Singletons.SystemUser.get(user_data)

    # The index page is THE page of the app, it always exists, so we initialize
    # it here.  The player always exists, but we initialize that from the
    # index page
    SWS.Routers.main = new SWS.Routers.Main()
    Backbone.history.start(pushState: true)

    SWS.index = new SWS.Views.SpinworkoutsIndex()
#    performance = SWS.Singletons.Performance.get()
#    performance.set('mode', 'init')


#    if !user.isLoggedIn()
#      new SWS.Views.OnboardingSelectionModal()

#      SWS.index.setTimedNavMessage('videos to exercise with...', 5)
    SWS.log "SWS.init() end"

$(document).ready ->

  $(document).on('click', 'a[class~="pjax"]', (event) ->
    event.preventDefault()
    params = $(event.target).data('pjaxp') || {}
    params.container = params.container || SWS.Config.defaultTarget
    params.url = event.target.href
    if typeof params.url == 'undefined'
      params.url = event.target.parentElement.href
    if typeof params.url == 'undefined'
      params.url = event.target.parentElement.parentElement.href
    # That should be deep enough
    if typeof params.url == 'undefined'
      params.url = event.target.parentElement.parentElement.parentElement.href
    $.pjax(params)
  )                   
  
  $(document).on('submit', 'form[class~="pjax"]', (event) ->
    event.preventDefault()
    params = $(event.target).data('pjaxp') || {}
    #container = $(this).closest('[data-pjax-container]')
    params.container = params.container || SWS.Config.defaultTarget
    $.pjax.submit(event, params.container, params)
  )
    
  $(document).on('pjax:start', (event) ->
    # if $(event.relatedTarget).hasClass('backbone')
    SWS.log 'pjax start'
  )
  
  $(document).on('pjax:end', (event) ->
    SWS.log 'pjax end'
  )

  $(document).on('pjax:success', (event, textStatus, request) ->
    # Todo: get this to work
#    if request.getResponseHeader('REDIRECT')
#      uri = request.getResponseHeader('REDIRECT')
    uri = event.delegateTarget.URL.replace(/^https?:\/\/[^\/]*|\?.*$/g, '')
    performance = SWS.Singletons.Performance.get()
    if performance.isPerforming()
      $('#inner-container .perform_card_border').each(() ->
        $(this).removeClass('col-lg-3').addClass('col-lg-12')
        $(this).removeClass('col-md-4').addClass('col-md-12')
        $(this).removeClass('col-sm-6').addClass('col-sm-12')
      )

    SWS.log "pjax:success, url: #{uri}"
    Backbone.history.navigate(uri, {trigger: true})
  )

  $(document).on('pjax:error', (event) ->
    SWS.log 'pjax error'
  )     
  
  $('.backbone').on('click',(event) ->
    event.preventDefault()
    uri = event.currentTarget.href.replace(/^https?:\/\/[^\/]*|\?.*$/g, '')    
    Backbone.history.navigate(uri, {trigger: true})  
  )

  # Highlight effects
  $('.hover-highlight').mouseover(() ->
    $(this).addClass('hover')
  )

  $('.hover-highlight').mouseout(() ->
    $(this).removeClass('hover')
  )

  SWS.init()
