Feature: Login to Spinworkouts
  In order to login to the site

Background: System knows about a set of users
  Given the system knows about the following users:
  | uid | name      |  email |
  | 12345 | xraiff  |  steve@example.com |

Scenario: See the login modal
  Given I am on the homepage
  When I follow "Sign in"
  Then I should see "Sign in through one of these services" in the modal                                                                         

Scenario: Twitter sign in shows sign out
  Given I am logged out
  And I am on the homepage          
  And I sign in with provider "twitter"
  Then I should see "Sign out"             

Scenario: Facebook sign in shows sign out
  Given I am logged out
  And I am on the homepage          
  And I sign in with provider "facebook"
  Then I should see "Sign out"     

Scenario: Identity sign in shows sign out
  Given I am logged out
  And I am on the homepage          
  And I sign in with provider "identity"
  Then I should see "Sign out"               

Scenario: Bad Twitter sign in doesn't show sign out
  Given I am logged out
  And I am on the homepage          
  And I sign in with invalid credentials to provider "twitter"
  Then I should not see "Sign out"             

Scenario: Bad Facebook sign in doesn't show sign out
  Given I am logged out
  And I am on the homepage          
  And I sign in with invalid credentials to provider "facebook"
  Then I should not see "Sign out"

Scenario: Bad identity sign in shows errors in modal
  Given I am logged out
  And I am on the homepage          
  And I sign in with invalid credentials to provider "identity"
  Then I should not see "Sign out"    
  And I should see "Invalid Credentials"
                                                                         