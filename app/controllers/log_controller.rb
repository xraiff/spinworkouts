class LogController < ApplicationController

  def index
    Rails.logger.info "q: #{params[:q]}"
    send_data 1, disposition: 'inline'
  end
end
