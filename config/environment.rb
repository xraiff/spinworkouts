# Load the rails application
require File.expand_path('../application', __FILE__)

# Initialize the rails application
SWS::Application.initialize!
                               
OmniAuth.config.on_failure = Proc.new { |env|
  OmniAuth::FailureEndpoint.new(env).redirect_to_failure
}

Mime::Type.register "image/svg+xml", :svg