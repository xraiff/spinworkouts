class SWS.Singletons.StagePlayerAdapter

  instance = null

  class StagePlayerAdapterPrivate extends SWS.Models.JwPlayerAdapter
    playerElement: "stageplayer"

  @get: (options) ->
    instance ?= new StagePlayerAdapterPrivate(options)