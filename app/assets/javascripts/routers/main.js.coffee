class SWS.Routers.Main extends Backbone.Router
  @currentWorkout: null
  @ga: null
  @performanceModel: null
  @user: null
  @workoutList: null
  @workoutView: null

  routes:
    '' : 'refreshPage'
    'performance/load_stages/:id': 'workoutLoadStages'
    'performance/show': 'showPerform'
    'stages/created/:id': 'createdStage'
    'stages/:id/perform': 'stagePerform'
    'stages/:id': 'stageShow'
    'stages': 'stagesShow'
#    'stages/:id' : 'loadStage'
    'users/:id/workouts' : 'handleUserWorkouts'
    'workouts/:workout_id/perform' : 'workoutPerform'
    'workouts/:id': 'workoutShow'
    'workouts' : 'workoutsShow'

  @loadWorkoutList: (options = {}) ->
#    if @workoutListView
#      @workoutListView.destroy()
#      @workoutListView = null

    if $('#workoutList')
      workout_data = $('#workoutList').data('workouts')
      workout_collection = new SWS.Collections.Workouts(workout_data)
      options.workouts = workout_collection
      @workoutListView = new SWS.Views.WorkoutList(options)

  initialize: () ->
    SWS.log "router init"
    @ga = SWS.Singletons.GaAdapter.get()
    @performance = SWS.Singletons.Performance.get()

  createdStage: (id) ->
    SWS.log "createdStage: #{id}"
    system = SWS.Singletons.System.get()
    system.trigger('sws:stage:created', {stage: {id: id}});

  messageHide: ->
    $('#message').hide()

  handleUserWorkouts: (id) ->
    @refreshPage()
    @ga.log()
    @constructor.loadWorkoutList({showDeleteLinks: true})

  morphToPerform: ->
    $('#performance_container').removeClass('visuallyhidden')
    $('#inner-container').removeClass('col-lg-12').addClass('col-lg-offset-9').addClass('col-lg-3')
    $('#inner-container').removeClass('col-md-12').addClass('col-md-offset-9').addClass('col-md-3')
    $('#inner-container .perform_card_border').each(() ->
      $(this).removeClass('col-lg-3').addClass('col-lg-12')
      $(this).removeClass('col-md-4').addClass('col-md-12')
      $(this).removeClass('col-sm-6').addClass('col-sm-12')
    )
    $('#bottom_nav').show()

  refreshPage: ->
    SWS.log "refreshPage"
    $('#bottom_nav').hide()
    if !@performance.isPerforming()
      @performance.set('mode', 'init')
    @constructor.loadWorkoutList()
    @ga.log()

  setContentTitle: (title) ->
    $('#content_title').html(title)

  showPerform: ->
    $('#inner-container').html('')
    @morphToPerform()

  stageShow: (id) ->
    stage = new SWS.Models.Stage($("#stage_card_#{id}").data('stage'))
    @performance.loadTeaser(stage)
    $('#inner-container').html('')

  stagePerform: (id) ->
    stage = new SWS.Models.Stage($("#stage_card_#{id}").data('stage'))
    @performance.addStage(stage, true)
    $('#inner-container').html('')
    @ga.log({title: stage.title})
    @morphToPerform()

  # Tell the current workout view to load it's stages into inner-container
  workoutLoadStages: (id) ->
    SWS.log "main::workoutLoadStages"
    @workoutView.renderStages()
    wid = @performance.performanceWorkout.get('id')
    Backbone.history.navigate("/workouts/#{wid}/perform##{id}", {trigger: true})

  workoutPerform: (id) ->
    @workoutShow(id, true)
    @morphToPerform()
    performancePlayer = SWS.Singletons.PerformancePlayerAdapter.get()
    performancePlayer.togglePlay()

  # If we are performing, then this should have been an ajax call, not a workout call...
  # thus, we assume that we are not performing.  If we are, ignore this call.
  workoutShow: (id, perform = false) ->
    # Pull workout data from page, load into player
    SWS.log 'Player Router, loadWorkout'
    if @performance.isPerforming()
      SWS.log "Warning::Cannot load workout while performing another"
      return

    dom_el = "#workout_#{id}"
    if @performance.isLoaded()
      SWS.log "performanceModel IS loaded"
      workout = @performance.performanceWorkout
    else
      workout = new SWS.Models.Workout($(dom_el).data('workout'))
      if perform
        @performance.loadWorkout(workout)
      else
        @performance.loadTeaser(workout)
    @ga.log({title: workout.title})
    @workoutView = new SWS.Views.Workout({el: dom_el, model: workout})

  workoutsShow: () ->
    @ga.log({title: "Top Workouts"})
    SWS.log "show workouts"

