require 'spec_helper'

describe MisController do
  describe "GET #index" do
    context "When called directly" do
      before(:each) do
        @mis = []
        @mis << FactoryGirl.create(:media_intensity_set_0)
        @mis << FactoryGirl.create(:media_intensity_set_1)
        @mis << FactoryGirl.create(:media_intensity_set_2)
      end

      describe "GET #index.html" do
        it "should return a page with the svg images for the matching media_intensity_data" do
          get :index
          assigns(:mis).should eq(@mis)
          response.should render_template('layouts/application')
          response.should render_template(:index)
        end
      end

      describe "GET #index.html<PJAX>" do
        it "should return a page with the svg images for the matching media_intensity_data, with no chrome" do
          request.env['HTTP_X_PJAX'] = true
          xhr :get, :index
          assigns(:mis).should eq(@mis)
          response.should_not render_template('layouts/application')
          response.should render_template(:index)
        end
      end

      describe "GET #index.json" do
        it "should return json data for all media_intensity_data" do
          xhr :get, :index, {:format => :json}
          response.body.should eq(@mis.to_json)
        end
      end
    end

    context "When nested under stage" do
      before(:each) do
        @stage = FactoryGirl.create(:stage, :with_media_intensity_set)
        @mis = @stage.media_intensity_set
      end

      describe "GET #index.html" do
        it "should return a page with the svg images for the media_intensity_set" do
          get :index, :stage_id => @stage.id
          assigns(:mis).should eq(@mis)
          assigns(:stage).should eq(@stage)
          response.should render_template('layouts/application')
          response.should render_template(:index)
        end
      end

      describe "GET #index.html<PJAX>" do
        it "should return a page with the svg images for the matching media_intensity_set, with no chrome" do
          request.env['HTTP_X_PJAX'] = true
          xhr :get, :index, :stage_id => @stage.id
          assigns(:mis).should eq(@mis)
          assigns(:stage).should eq(@stage)
          response.should_not render_template('layouts/application')
          response.should render_template(:index)
        end
      end

      describe "GET #index.json" do
        it "should return json data for all media_intensity_data" do
          xhr :get, :index, {:stage_id => @stage.id, :format => :json}
          assigns(:stage).should eq(@stage)
          response.body.should eq(@mis.to_json)
        end
      end
    end
  end
end