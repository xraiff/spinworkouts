class SWS.Singletons.GaAdapter
  instance = null

  # Reference: https://developers.google.com/analytics/devguides/collection/analyticsjs/pages
  class GaAdapterPrivate extends Backbone.Model
    gq: null
    sys: null

    initialize: () ->
      window.ga = window.ga || [];
      @ga = window.ga
      @history = []
      @sys = SWS.Singletons.System.get()

    log: (event) =>
      @ga('send', 'pageview')
      SWS.log "GA sent"

    # Only logs state if the state is included in the list of statesToLog in the system configuration
#    logState: (state, extra)  =>
#      if @sys.statesToLog is 'all' or $.inArray(state, @sys.statesToLog) > -1
#        event = state
#        if extra
#          event += '/' + extra
#        @log(event)

  @get: () ->
    instance ?= new GaAdapterPrivate()

