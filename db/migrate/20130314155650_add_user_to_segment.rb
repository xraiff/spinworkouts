class AddUserToSegment < ActiveRecord::Migration
  def change
    add_column :segments, :user_id, :integer
    add_index :segments, :user_id
  end
end
