class SWS.Views.SearchResults extends Backbone.View
  searchResults: null

  initialize: (searchResults) ->
    @performance = SWS.Singletons.Performance.get()
    @searchResults = searchResults
    @render()

  render: =>
    i = 0
    @searchResults.each((searchItem) =>
      if i > 1 and i % 2 == 0
        $(@el).append "<div class='clearfix visible-sm'></div>"
      if i > 2 and i % 3 == 0
        $(@el).append += "<div class='clearfix visible-md'></div>"

      if searchItem.get('classname') is "Stage"
        rawJson = searchItem.toJSON()
        model = new SWS.Models.Stage(rawJson)
      else
        model = searchItem

      searchItemView = new SWS.Views.SearchCard({
        model: model
        index: i
      })
      $(@el).append searchItemView.render().el
      i++
    )
    $(@el).append "<br><div class='clearfix'></div>"
    $(SWS.Config.defaultTarget).html(@el)
    this