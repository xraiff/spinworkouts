class SWS.Views.NavDynamic extends Backbone.View
  # This view is really the master visual container for the clientside application
  # It's responsibilities include:
  #     -top level navigation
  #     -central login facility
  #     -search

  el: '#nav_dynamic'
  performance: null
  performancePlayer: null
  template: JST['nav_dynamic']
  system: null
  user: null

  events:
    "click #searchButton": "handleSearch"
    "click #searchInput": "handleSearchClick"
    "click #signin": "handleSignIn"
    "click #stop": "handleStop"
    "click #signout": "handleSignOut"
    "submit #searchBox": "handleSearch"
    "click #graph_btn" : "handleGraphButton"
    "click #editGraph": "handleEditGraph"
    "click #intensityDown": "handleIntensityDown"
    "click #intensityUp": "handleIntensityUp"
    "click #my_workouts": "handleNavAway"
    "click #perf_time": "handleClickTime"
    "click #perform_workout": "handlePerformWorkout"
    "click #savePerform": "handleSavePerform"

  # ToDo: add option for initialize message in navbar
  initialize: () ->
    # We Need *some* statement in this function in order to be compiled...
    window.SWS.log "navDynamic::initialize"
    @user = SWS.Singletons.SystemUser.get()
    @user.on('change', @render, this)

    @performancePlayer = SWS.Singletons.PerformancePlayerAdapter.get()
    @performance = SWS.Singletons.Performance.get()

    # Handle the event like this since the logo is not within navDynamic's jurisdiction
    $('#brand-logo').on('click', (event) =>
#      @handleNavAway(event)

      screenfull.toggle()
    )

    @system = SWS.Singletons.System.get()
    @system.on('change:message', @render, this)

    @performance.on('change:seconds', @update_time, this)
    @performance.on('change:mode', @render, this)
    @performance.on('change:miv', @render, this)
    @render()

  graphButtonMarkup: =>
    if @performance.get('isShowGraph')
      'btn-primary'
    else
      'btn-default'

  handleClickTime: () =>
    SWS.log "handleClickTime"
    event.preventDefault()
    @performancePlayer = SWS.Singletons.PerformancePlayerAdapter.get()
    @performancePlayer.togglePlay()

  handleEditGraph: (event) =>
    SWS.log "handleEditGraph"
    event.preventDefault()
    @system.setTimedMessage('Use the up/down keys to program the intensity for this stage', 5)
    @performance.set('mode', 'edit')

  handleGraphButton: (event) =>
    event.preventDefault()
    @performance.set('isShowGraph', !@performance.get('isShowGraph'))
    @render()

  handleGraphSelect: (event) =>
    stage = @performance.performanceWorkout.stages.at(event.index)
    stage_id = stage.get('id')
    target = "#stage_card_#{stage_id}"
    if $(target).html() is undefined
      Backbone.history.navigate("/performance/load_stages/#{stage_id}", {trigger: true})
    else
      url = location.href
      location.href = target
      history.replaceState(null,null,url)

  handleIntensityDown: (event) =>
    SWS.log "intensityWidget: intensityDown"
    event.preventDefault()
    @performance.decIntensity()

  handleIntensityUp: (event) =>
    SWS.log "intensityWidget: intensityUp"
    event.preventDefault()
    @performance.incIntensity()

  handleNavAway: (event) =>
    if @performance.isPerforming()
      new SWS.Views.MessageModal({
        message: "Are you sure you want to leave this workout?"
        ok: () =>
          window.location.href = event.currentTarget.href
      })
      event.preventDefault()
      return false
    # otherwise, fall through to the navigation

  handlePerformWorkout: (event) =>
    SWS.log "handlePerformWorkout"
    event.preventDefault()
    route = Backbone.history.fragment
    $.pjax(
      container: '#inner-container'
      push: true
      url: "/#{route}/perform"
    )

  handleSavePerform: (event) =>
    event.preventDefault()
    if !@user.isLoggedIn()
      @user.once('change:id', () =>
        # Hack: make sure that the DOM is updated from the last modal before doing this one.
        setTimeout(() =>
          new SWS.Views.NewWorkoutModal({model: @performance.performanceWorkout, \
            stages_json: JSON.stringify(@performance.performanceWorkout.stages)})
        , 500)
      )
      new SWS.Views.NewSessionModal({message: 'To create a new workout, please:'})
    else
      new SWS.Views.NewWorkoutModal({model: @performance.performanceWorkout,  \
        stages_json: JSON.stringify(@performance.performanceWorkout.stages)})

    #    @user.loginRequired(
#      () =>
#        new SWS.Views.NewWorkoutModal({model: @performance.performanceWorkout, \
#          stages_json: JSON.stringify(@performance.performanceWorkout.stages)})
#      'To create a new workout, please:'

  handleSearch: (event) =>
    SWS.log "index::search"
    event.preventDefault()
    if $('#searchInput').val() is ''
      alert("Please enter a search term")
      return

    if @performance.isPerforming()
      $('#search-layout').val('perform')

    jQuery.ajax(
      type: "POST"
      dataType: 'json'
      data: $('#searchBox').serialize()
      url: '/searches'
      success: (data) =>
        SWS.log "search returned : #{data}"
        if @searchResults
          @searchResults.reset()
        if @searchResultsView
          @searchResultsView.remove()
        @searchResults = new SWS.Collections.SearchResults(data)
        @searchResultsView = new SWS.Views.SearchResults(@searchResults)

      error: (jqXHR, status, err) =>
        SWS.log "search error : #{status}"
        if jqXHR.responseJSON.errors.message
          @system.setTimedMessage(jqXHR.responseJSON.errors.message, {messageType: 'danger', seconds: 3})
    )
    false

  handleSearchClick: (event) =>
    event.preventDefault()
    $(event.target).val('')

  handleSignIn: (event) =>
    event.preventDefault()
    @user.login()

  handleSignOut: (event) =>
    event.preventDefault()
    @user.logout()

  handleStop: (event) =>
    event.preventDefault()
    @performancePlayer.pause()

  loadGraph: () =>
    SWS.log "navDynamic::loadGraph"
    if @performance.isLoaded() and @performance.get('isShowGraph')
      @graphWidget = new SWS.Views.IntensityWidget({position: true, model: @performance})
      $('#bottom_graph').html(@graphWidget.el)
      @graphWidget.on('bottom_graph:select', (event) =>
        @handleGraphSelect(event)
      )
      $(@graphWidget.el).parent().show()
    else if @performance.teaser
      @graphWidget = new SWS.Views.IntensityWidget({position: false, model: @performance.teaser})
      $('#bottom_graph').html(@graphWidget.el)
      if @performance.teaser.constructor.name == 'Workout'
        @graphWidget.on('bottom_graph:select', (event) =>
          @handleGraphSelect(event)
        )
      $(@graphWidget.el).parent().show()
    else if @graphWidget
      $(@graphWidget.el).parent().hide()

  # Renders nav
  render: () =>
    mode = @performance.get('mode')
    SWS.log "navDynamic :: render, mode: #{mode}"

    route = Backbone.history.fragment
    showPerformButton = route.match(/^workouts/) and !@performance.isPerforming()

    graphButtonClasses = "btn btn-graph-ctrl #{@graphButtonMarkup()}"
    $(@el).html(@template(user: @user,      \
      graphButtonClasses: graphButtonClasses,   \
      mode: mode,  \
      performance: @performance,   \
      showPerformButton: showPerformButton, \
      system: @system))

    $('#perf_time').removeClass('btn-danger')
    $('#perf_time').removeClass('btn-warning')
    $('#perf_time').removeClass('btn-success')
    $('#perf_time').addClass(@timeButtonMarkup())
    @loadGraph()
    this

  startPerformance: () ->
    SWS.log "startWorkout"
    @performance.start()

  timeButtonMarkup: =>
    if @performance.isPlaying()
      'btn-success'
    else if @performance.isPaused()
      'btn-warning'
    else
      'btn-danger'

  # Only update the actual time data, otherwise, we have to re-delegate events
  update_time: =>
    $('#perf_time').html(@performance.print_perform_button())
    this