require 'net/http'
require 'uri'

class StagesController < ApplicationController
  # rescue_from ActiveRecord::RecordNotFound, :with => :record_not_found
  #before_filter :except => [:index, :show] do |controller|
  #  controller.authenticate_user!(request.fullpath)
  #end
  before_filter :authenticate_user!, :except => [:index, :show]

  # GET /stages
  def index
    if params[:workout_id]
      @workout = Workout.find(params[:workout_id])
      @stages = @workout.stages
    else
      @stages = Stage.all
    end

    respond_to do |format|
      format.html 
      format.json { send_data @stages.to_json, disposition: 'inline' }
    end
  end     

  # GET /stages/new
  # GET /stages/new.xml
  def new
    begin
      if !params[:id].nil?
        @stage = Stage.find(params[:id]).clone
      elsif !params[:stage].nil?
        @stage = Stage.new(params[:stage])
      else
        raise
      end
    rescue
      @stage = Stage.new
    end
    respond_to do |format|
      format.html { render :new, :layout => get_layout() } # new.html.erb
      format.xml { render :xml => @stage }
    end
  end                       

  # POST /stages
  def create
    @stage = Stage.new(params[:stage])
    begin
      @stage.user_id = current_user.id
      if @stage.save
        if params[:segment_ids]
          params[:segment_ids].each do |segment_id|
            segment = Segment.find(segment_id)
            @stage.segments << segment unless segment.nil?
          end
        end
      end

      if request.xhr?
        if !@stage.errors.empty?
          render :status => :bad_request, :json => { :errors => @stage.errors }
        else
          render :json => @stage
        end
      else
        # Supporting normal html requests for posterity...
        respond_to do |format|
          format.html {
            flash[:page_notice] = 'Congratulations, you now have a new stage to play with'
            redirect_to stage_path(@stage.id)
          }
        end
      end

    rescue ActiveRecord::RecordInvalid
      flash[:notice] = @stage.errors.full_messages.join('\n') if !@stage.errors.empty?
      response.headers['X-PJAX-URL'] = new_workout_path if pjax_request?
      render 'new'
    rescue Exception => e
      logger.info "StagesController::#create: #{e.message}"
    end
  end

  # Note, this is a PJAX-only route
  def created
    response.headers['X-PJAX-URL'] = url_for(:action => :created, :id => params[:id])
    @stage = Stage.find(params[:id])
    @backbone_events << { :name => 'sws:stage:created', :data => { :stage => @stage }}
    render '/shared/js_wrapper', :layout => get_layout()
  end

  # DELETE /stages/:id
  def destroy  
    @stage = Workout.stages.find(params[:id]).first 
    @stage.destroy
    render :json => true
  end

  # GET /stages/:id 
  def show
    @stage = Stage.find(params[:id])
    respond_to do |format|
      format.html { render :layout => get_layout() } # show.html.erb
      format.json { send_data @stage.to_json, disposition: 'inline' }
      format.svg {
        options = params.select {|k,v| ['height', 'width'].include?(k)}
        send_data @stage.to_svg(options),  type: 'image/svg+xml', disposition: 'inline'
      }
    end    
  end

  # PUT /stages/:id
  def update
    @stage = Stage.find(params[:id])
    respond_to do |format|
      format.html { render :layout => get_layout() } # update.html.erb
    end
  end

  private

  def record_not_found
    render :text => "404 Not Found", :content_type => "text/html", :status => 404
  end
end
