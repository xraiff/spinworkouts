class SWS.Models.Stage extends Backbone.Model
  url: '/api/stages'
  user: null
  intensityRectCache: null

  initialize: (data) ->
    intensity_points = @get('intensity_points')
    if not intensity_points or intensity_points.length == 0
      @set('intensity_points', [{t: 0, i: 100}])

    @user = SWS.Singletons.SystemUser.get()

#  adjPoints: (amount, points, fromTime) =>
#    for point in points by -1
#      point.i += amount
#      if point.i < 0
#        point.i = 0
#      else if point > @systemMax
#        point.i = @systemMax
#      if point.t <= fromTime
#        break
#    @asPercentRects = null   # Reset calculated values
#    points

  adjIntensity: (amount, fromTime) =>
    fromTime = parseInt(fromTime)
    intensity_points = @get('intensity_points')
    minIntensity = SWS.Singletons.System.get().minIntensity
    maxIntensity = SWS.Singletons.System.get().maxIntensity

    index = intensity_points.length - 1
    while index > 0
      if intensity_points[index].t <= fromTime
        break
      index -= 1

    newIntensity = intensity_points[index].i + amount
    if newIntensity > maxIntensity
      newIntensity = maxIntensity
    if newIntensity < minIntensity
      newIntensity = minIntensity

    percentOfStage = (fromTime - intensity_points[index].t) / @get('duration_sec') * 100
    if percentOfStage <= @user.options.get('minRectPercent')
      intensity_points[index].i = newIntensity
    else if index == 0
      intensity_points.push {t: fromTime, i: newIntensity}
    else
      intensity_points.splice(index+1,0, {t: fromTime, i: newIntensity})

    @set('id', null)
    @set('intensity_points', intensity_points)
    SWS.log "intensity_points = #{JSON.stringify(intensity_points)}"
    @asPercentRects = null
    newIntensity

  getIntensity: (atTime = 0) =>
    lastMiv = 0
    if @get('intensity_points')
      for point in @get('intensity_points')
        if point.t > atTime
          break
        else
          lastMiv = point.i
    lastMiv

  # ToDo: override parent.set() in order to clear the asPercentRects cache

  graphRects: (options) =>
    #    if @datapointsRectCache
    #      return @intensityRectCache

    # Build up Rects
    @intensityRectCache = []
    lastPoint = null
    intensity_points = @get('intensity_points')

    if intensity_points.length == 1
      @intensityRectCache.push @pointsToRect(intensity_points[0], {t: @get('duration_sec'), i: intensity_points[0].i}, options)

    else if intensity_points.length > 1
      for point in intensity_points
        if lastPoint
          @intensityRectCache.push @pointsToRect(lastPoint, point, options)
        lastPoint = point

      if lastPoint.t < @get('duration_sec')
        @intensityRectCache.push @pointsToRect(lastPoint, {t: @get('duration_sec'), i: lastPoint.i}, options)

    @intensityRectCache

  roundHundredths: (value) ->
    Math.round(value * 100) / 100

  pointsToRect: (p1, p2, options = {}) ->
    scale = options.scale ? 1.0
    offset = options.offset ? 0.0
    index = options.index ? 0
#    x1 = (offset * 100) + Math.round(p1.t / @get('duration_sec') * 100.0 * scale)
    x1 = @roundHundredths((offset * 100) + (p1.t / @get('duration_sec') * 100.0 * scale))
    x2 = @roundHundredths((offset * 100) + (p2.t / @get('duration_sec') * 100.0 * scale))
    y = p1.i / 10
    {x: x1, w: @roundHundredths(x2 - x1), y: 100-y, h: y, index: index}