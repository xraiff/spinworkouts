class ApplicationController < ActionController::Base
  protect_from_forgery
  before_filter :set_debug_environment
  before_filter :set_controller_globals

  include ApplicationHelper

  protected

  # We always login via modal!
  def authenticate_user!(return_to = nil)
    if !current_user
      #if request.xhr?
      #  if pjax?
      #    flash[:notice] = 'You need to sign in to perform this action'
      #    if return_to
      #      session[:return_to] = return_to
      #      redirect_to :controller => :sessions, :action => :new
      #    else
      #      redirect_to :controller => :sessions, :action => :new
      #    end
      #  else
          Rails.logger.info "Here in Authenticate user"
          render :layout => false, :file => "public/401", :status => :unauthorized
        #end
      #else
      #  redirect_to root_url, :alert => 'You need to sign in for access to this page.'
      #end
      return false
    end
  end


  def ajax_failure(options = {})
    # Always respond with json for now, possibly revisit later
    render :status => (options.has_key?(:status) ? options[:status] : :bad_request), :json => options.has_key?(:message) ? options[:message] : ''
  end


  # This is a list of page elements, replaced dynamically by javascript on a pjax
  # request... elements IN ADDITION to the main pjax element that's replaced.
  def set_controller_globals
    @replacements = []

    # Backbone events are events sent to the Backbone system model
    @backbone_events = []
  end

  def pjax?
    # SUR: added request.env to make it testable by rspec...
    request.env['HTTP_X_PJAX'].present? || env['HTTP_X_PJAX'].present?
    #pjax_request?
  end

  def require_pjax
    if !pjax?
      render :file => "public/401", :status => :unauthorized
      return
    end
  end

  def get_layout(options={})
    if options
      [:title, :class].each do |option|
        if !options[option].nil?
          instance_variable_set("@#{option.to_s}", options[option])
        end
      end
    end
    if request.xhr?
      if pjax?
        response.headers['X-PJAX-URL'] = request.url
        'pjax'
      else
        'ajax'
      end
    else
      'application'
    end
  end

  # Sets flags used to do "development-type" things
  def set_debug_environment
    logger.info "set_debug_environment..."
    %w{debug_yt debug_log}.each do |flag|
      if params[flag.to_sym]
        if params[flag.to_sym] == '0'
          session.delete(flag.to_sym)
        else
          logger.info "Setting #{flag} to true"
          session[flag.to_sym] = true
        end
      end
    end
  end

  #def redirect_to(options = {}, response_status = {})
  #  logger.info "In OUR redirect_to"
  #  #%w(X-PJAX X-PJAX-URL X-PJAX-Container X-Requested-With).each do |header|
  #  %w(X-PJAX X-PJAX-Container X-Requested-With).each do |header|
  #    logger.info "header : #{header}"
  #    if !env[header].nil?
  #      logger.info "setting header #{header} in response"
  #      response.headers[header] = env[header]
  #    end
  #  end
  #  super options, response_status
  #end
  
  helper_method :current_user, :pjax?  
end
