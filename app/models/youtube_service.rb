require 'google/api_client'

# This api is registered at:
# https://cloud.google.com/console?redirected=true#/project/682111809513
class YouTubeService

  # Set DEVELOPER_KEY to the "API key" value from the "Access" tab of the
  # Google APIs Console <http://code.google.com/apis/console#access>
  # Please ensure that you have enabled the YouTube Data API for your project.
  #API_KEY = "sws-spinworkouts"
  API_KEY = YOU_TUBE[:api_key]
  APP_NAME = "sws-spinworkouts"

  def self.query(search)

    client = self.setup()
    youtube = client.discovered_api('youtube', 'v3')

    search_term = search.nil? ? 'Spinning' : search

    # Complete options at: https://developers.google.com/youtube/v3/docs/search/list
    opts = Trollop::options do
      opt :q, 'Search term', :type => String, :default => search_term
      opt :maxResults, 'Max results', :type => :int, :default => 25
      opt :type, 'Type', :type => String, :default => 'video'
      opt :duration, 'Duration', :type => String, :default => ''
    end

    opts[:part] = 'id,snippet'
    search_response = client.execute!(
        :api_method => youtube.search.list,
        :parameters => opts
    )

    search_items = self.transform(search_response.data)
    #Rails.logger.info "search_response: \n #{search_items.to_json} \n"
    search_items
  end


  # TRANSFORM this:
  #
  #         "kind": "youtube#searchResult",
  #"etag": "\"a0wP00HsE4pk2wtEZAypPNNMDK8/msEyx9LR9sGpIFEPutq5aHFGLoM\"",
  #    "id": {
  #    "kind": "youtube#video",
  #    "videoId": "P5OZ4XlSWoA"
  #},
  #    "snippet": {
  #    "publishedAt": "2009-12-10T19:32:05.000Z",
  #    "channelId": "UCGiSCVGNukLqv8hwpKCsQKQ",
  #    "title": "Fitness master class: Spinning",
  #    "description": "The best indoor workout you're not doing? Spinning. But what if we told you it can sizzle 400 calories a session? We got answers to your burning questions fr...",
  #    "thumbnails": {
  #    "default": {
  #    "url": "https://i.ytimg.com/vi/P5OZ4XlSWoA/default.jpg"
  #},
  #    "medium": {
  #    "url": "https://i.ytimg.com/vi/P5OZ4XlSWoA/mqdefault.jpg"
  #},
  #    "high": {
  #    "url": "https://i.ytimg.com/vi/P5OZ4XlSWoA/hqdefault.jpg"
  #}
  #},
  #    "channelTitle": "SelfMagazine"
  #}
  # INTO THIS:
  #
  # id: null
  # title: null
  # pub_date: null
  # description: null
  # image_link: null

  def self.transform(response_data)
    items = []
    begin
      response_data.items.each do |item|
        items.push({:youtube_id => item.id.videoId,
                    :classname => 'YouTube',
                    :title => item.snippet.title,
                    :description => item.snippet.description,
                    :image_link => item.snippet.thumbnails.default.url,
                    :pub_date => item.snippet.publishedAt})
      end

    rescue Exception => e
      Rails.logger.info "YouTubeService::transform : #{e.message}"
    end
    items
  end

  def self.setup()
    client = Google::APIClient.new({:key => self::API_KEY,
                                   :application_name => self::APP_NAME,
                                   :authorization => nil,
                                   :user_ip => '50.0.151.231'})
    client
  end


  def self.setup_oauth()
    # Initialize the client & Google+ API
    client = Google::APIClient.new
    plus = client.discovered_api('plus')

    # Initialize OAuth 2.0 client
    #client.authorization.client_id = '<CLIENT_ID_FROM_API_CONSOLE>'
    #client.authorization.client_secret = '<CLIENT_SECRET>'
    #client.authorization.redirect_uri = '<YOUR_REDIRECT_URI>'
    #
    #client.authorization.scope = 'https://www.googleapis.com/auth/plus.me'

    # Request authorization
    #redirect_uri = client.authorization.authorization_uri

    # Wait for authorization code then exchange for token
    #client.authorization.code = '....'
    #client.authorization.fetch_access_token!

    client
  end
end
