Number.prototype.toHHMMSS = function () {
    var hours   = Math.floor(this / 3600);
    var minutes = Math.floor((this - (hours * 3600)) / 60);
    var seconds = this - (hours * 3600) - (minutes * 60);

    if (hours   < 10) {hours   = "0"+hours;}
    if (minutes < 10) {minutes = "0"+minutes;}
    if (seconds < 10) {seconds = "0"+seconds;}
    var time = (hours != "00") ? hours + ':' : '';
    time += minutes+':'+seconds;
    return time;
}

//Number.prototype.roundHundredths = function() {
//    Math.round(this * 100.0) / 100.0
//}
//
//Number.prototype.percentHundredths = function() {
//    Math.round(this * 10000.0) / 100.0
//}
