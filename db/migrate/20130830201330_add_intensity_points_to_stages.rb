class AddIntensityPointsToStages < ActiveRecord::Migration
  def up
    drop_table :media_intensity_sets
    drop_column :stages, :media_intensity_set_id
  end

  def change
    add_column :stages, :intensity_points_json, :text, :default => ''
  end
end
