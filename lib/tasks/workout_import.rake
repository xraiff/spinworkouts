namespace :workout do

  require 'fileutils'
  #require 'ruby-debug'
  #require 'json_pure'

  task :mock => :environment do
    require 'factory_girl'

  # Requires supporting ruby files with custom matchers and macros, etc,
  # in spec/support/ and its subdirectories.
    Dir[Rails.root.join("spec/factories/*.rb")].each {|f| require f}

    u = FactoryGirl.create(:user, :with_workouts, system_user: true)
    u.save!
    # Do this a second time.  The first one sets the duration_sec, this one persists it.

    #workout = Workout.new({:title => 'test 1', :description => 'test 1'})
  end

  task :seed => :environment do

  end

  task :mis => :environment do
    require 'factory_girl'
    Dir[Rails.root.join("spec/factories/*.rb")].each {|f| require f}

    i = 0
    begin
      while true
        m = FactoryGirl.create("media_intensity_set_#{i}".to_sym)
        i += 1
      end
    rescue => e
      p e.message
    end
  end

  task :import => :environment do
    if ENV['file'].nil?
      p "Usage: rake workout:import file=file_to_parse"
      exit 
    end                  
    processCouchFile('Workout', Workout) 
    processCouchFile('WorkoutSegment', Segment)
    processCouchFile('WorkoutStage', Stage)
  end
  
  def processCouchFile(typename, classname)
    begin  
      json = ''
      filename = ENV['file']
      File.open(filename, 'r') do |file|
        while (line = file.gets) do
          json = json + line
        end
        struct = ActiveSupport::JSON.decode(json)
        struct['rows'].each do |outer_item|
          item = outer_item['doc']
          if item['type'].nil?
            next 
          end                         
          if item['type'] == typename
            classname.importObject(item)
          end
        end
      end
    rescue Exception => e
      p "File #{filename} doesn't exist"
    end    
  end

  task :print => :environment do
    p "Workouts: "
    workouts = Workout.all
    p workouts.to_s
  end

  task :delete_mock => :environment do
    users = User.find_all_by_system_user(true)
    users.each do |user|
      user.workouts.destroy_all
      user.stages.destroy_all
      user.segments.destroy_all
      user.instructions.destroy_all
      user.delete
    end
  end

  task :delete_mis => :environment do
    MediaIntensitySet.delete_all
  end

  task :delete_all => :environment do
    p "Deleting Workouts ..."
    WorkoutStage.delete_all
    StageSegment.delete_all
    SegmentInstruction.delete_all
    Workout.delete_all
    Segment.delete_all
    Stage.delete_all
    Instruction.delete_all
    User.delete_all
  end
end
