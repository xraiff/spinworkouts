FactoryGirl.define do
  factory :instruction, class: Instruction do
    sequence :text do |n|
      "Instruction #{n}"
    end
  end

  factory :segment_instruction, class: SegmentInstruction do
    sequence :offset do |n|
      n * 10
    end
  end

  factory :segment, class: Segment do
    sequence :title do |n|
      "Segment #{n}"
    end

    heartrate [150,160,170,180,140,120]
    resistance [5, 6, 7, 8, 7, 6]

    trait :with_instructions do
      ignore do
        number_of_instructions 5
      end

      after(:create) do |segment, evaluator|
        instructions = FactoryGirl.create_list(:instruction, evaluator.number_of_instructions, user: segment.user)
        instructions.each do |instruction|
          si = FactoryGirl.create(:segment_instruction, segment: segment)
          si.instruction = instruction
          segment.segment_instructions << si
        end
      end
    end
  end

  factory :workout_stage, class: WorkoutStage do
    sequence :order do |n|
      n
    end
  end

  factory :workout, class: Workout do
    sequence :title do |n|
      "Workout #{n}"
    end
    sequence :description do |n|
      "Workout Description #{n}"
    end
    trait :with_stages do
      ignore do
        number_of_stages 6
      end
      after(:create) do |workout, evaluator|
        stages = FactoryGirl.create_list(:stage, evaluator.number_of_stages, :with_segments, user: workout.user)
        stages.each do |stage|
          wss = FactoryGirl.create(:workout_stage, workout: workout)
          wss.stage = stage
          workout.workout_stages << wss
        end
        workout.duration_sec = stages.map(&:duration_sec).inject(0, :+)
      end
    end

    trait :with_non_owned_stages do
      ignore do
        number_of_stages 6
      end
      after(:create) do |workout, evaluator|
        user = FactoryGirl.create(:user)
        stages = FactoryGirl.create_list(:stage, evaluator.number_of_stages, :with_segments, user: user)
        stages.each do |stage|
          wss = FactoryGirl.create(:workout_stage, workout: workout)
          wss.stage = stage
          workout.workout_stages << wss
        end
        workout.duration_sec = stages.map(&:duration_sec).inject(0, :+)
      end
    end
  end

  factory :user, class: User do
    provider 'identity'
    sequence :name do |n|
      "foo_#{n}"
    end
    uid "12345"
    email { "#{name}@example.com" }
    trait :with_workouts do
      ignore do
        number_of_workouts 6
      end
      after(:create) do |user, evaluator|
        ws = FactoryGirl.create_list(:workout, evaluator.number_of_workouts, :with_stages, user: user)
        user.workouts << ws
      end
    end
    trait :with_stages do
      ignore do
        number_of_stages 5
      end
      after(:create) do |user, evaluator|
        FactoryGirl.create_list(:stage, evaluator.number_of_stages, user_id: user.id)
      end
    end
  end

end

