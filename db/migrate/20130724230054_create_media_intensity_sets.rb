class CreateMediaIntensitySets < ActiveRecord::Migration
  def up
    create_table :media_intensity_sets do |t|
      t.integer :num_points, :default => 0
      t.integer :duration_sec, :default => 0
      t.integer :min, :default => 0
      t.integer :max, :default => 0
      t.integer :mean, :default => 0
      t.integer :stage_id
      t.text :data_json, :default => ''
      t.timestamps
    end

    add_index "media_intensity_sets", ["stage_id"], :name => "fk_media_instensity_sets_stages"
    add_column :stages, :media_intensity_set_id, :integer
  end

  def down
    drop_table :media_intensity_sets
  end
end
