require 'spec_helper'

describe User do
  describe "#create" do
    it "should raise error for a bad email" do
      expect {
        User.create!(:name => 'fred', :email => 'lkjlaef089uoaisjdf3')
      }.to raise_error
    end

    it "should not require an email" do
      expect {
        User.create!(:name => 'fred')
      }.to_not raise_error
    end
  end

  describe "#system_user" do
    before(:each) do
      @suser1 = FactoryGirl.create(:user, system_user: true)
      @suser2 = FactoryGirl.create(:user, system_user: true)
      @user = FactoryGirl.create(:user)
    end

    it "should find the system user" do
      users = User.find_all_by_system_user(true)
      users.count.should == 2
    end
  end

  describe "#workouts" do
    before(:each) do
      @number_of_workouts = 6
      @user = FactoryGirl.create(:user, :with_workouts, :number_of_workouts => @number_of_workouts)
    end

    it "should find workouts belonging to this user when created" do
      workouts = Workout.find_all_by_user_id(@user.id)
      workouts.count.should == @number_of_workouts
    end

    it "should find workout stages belonging to user" do
      stages = Stage.find_all_by_user_id(@user.id)
      stages.count.should == @number_of_workouts * 6
    end

    it "should find workout segments belonging to user" do
      segments = Segment.find_all_by_user_id(@user.id)
      segments.count.should == @number_of_workouts * 6 * 6
    end

    it "should find workout instructions belonging to user" do
      instructions = Instruction.find_all_by_user_id(@user.id)
      instructions.count.should > 0
    end


    #it "find workouts belonging to user when assigned as such" do
    #  @number_of_workouts = 5
    #  @user2 = FactoryGirl.create(:user)
    #  #@user2.workouts = FactoryGirl.create(:workout, :with_stages, :number_of_stages => 4, user_id: @user2.id )
    #  workout = FactoryGirl.create(:workout, :with_stages, :number_of_stages => 4, user_id: @user2.id)
    #  stages = Stage.find_by_user_id(@user2.id)
    #  stages.length.should == 4
    #end

    it "should not delete workouts when it is deleted" do
      w1 = @user.workouts[0]
      w2 = @user.workouts[1]
      @user.destroy
      expect { Workout.find(w1.id) }.to_not raise_error(ActiveRecord::RecordNotFound)
      expect { Workout.find(w2.id) }.to_not raise_error(ActiveRecord::RecordNotFound)
    end
  end

  describe "#stages" do
    before(:each) do
      FactoryGirl.reload
      @number_of_workouts = 2
      @user = FactoryGirl.create(:user, :with_workouts, :number_of_workouts => @number_of_workouts)
    end

    it "should find underlying stages created when parent workout was created" do
      @user.stages.length.should
    end
  end
end
