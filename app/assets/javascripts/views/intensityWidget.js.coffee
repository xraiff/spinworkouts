class SWS.Views.IntensityWidget extends Backbone.View
  baseModel: null
  height: 0
  positionRect: ''
  showPosition: false
  template: JST['intensityWidget']
  width: 0
  user: null

  # The widget should watch two data models...
  # The first one, as the template for the base
  # The second one, as the dynamic state of the MID that the user is set to

  # Model must have the following methods() and <events> defined:
  #   graphRects()
  #   <change>
  #
  #   If options.position defined:
  #       currentPercentPosition()
  #       <change:position>
  #

  events:
    "click .basegraph": "handleRectSelect"

  initialize: (options = {}) ->
    @showPosition = options.position ? false
#    @el = options.el ? '#bottom_graph'
    @positionEl = options.positionEl ? '#positionRect'
    @height = options.height ? 50
    @width = options.width ? '100%'
    @user = SWS.Singletons.SystemUser.get()
    @model.on('change', @render, this)
    if @showPosition
      @model.on('change:position', @renderPosition, this)
    @render()

  getPosition: () =>
    y = @model.miv / 10

    position =
      x: @model.currentPercentPosition()
      y: 0
      h: 100
      w: 4
    position

  handleRectSelect: (event) =>
    event.preventDefault()
    SWS.log "IntesityWidget::handleRectSelect"
    i = $(event.target).data('index')
    SWS.log "index = #{i}"
    @trigger("#{@el.replace(/#/,'')}:select", {index: i})

#    $(event.target).css('fill', '#f00')
#    SWS.log "highlight"

  renderPosition: =>
    position = @getPosition()
    $(@positionRect).attr('x', "#{position.x}%")
    $(@positionRect).attr('height', "#{position.h}%")
    $(@positionRect).attr('y', "#{position.y}%")

  render: =>
    if @showPosition
      position = @getPosition()
    rects = @model.graphRects()

    $(@el).html(@template(rects: rects, position: position, width: @width, height: @height))
    this