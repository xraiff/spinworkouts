module JsonFriends
  
  # Attribute intersection operation, for use by clients that want to
  # pull a hash of only *some* attributes of the workout  
  def &(attr_array)
    if attr_array.class != Array
      raise ArgumentError("rhs of & must be an array selecting object attributes")
    end
    return_hash = {}    
    attr_array.each do |attr|
      if self.respond_to?(attr)
        return_hash[attr] = self[attr]
      end
    end              
    return_hash
  end

  # Attribute difference operation, for use by clients that want to
  # pull a hash of only *some* attributes of the workout  
  
  def -(attr_array)  
    if attr_array.class != Array
      raise ArgumentError("rhs of & must be an array selecting object attributes")
    end    
    return_hash = {}    
    attr_array.each do |attr|
      if self.respond_to?(attr) 
      else
        return_hash[attr] = self[attr]
      end
    end              
    return_hash
  end
  
end
