class IdentitiesController < ApplicationController  
  def new                                    
    logger.debug "Identities::new controller method"
    @identity = env['omniauth.identity']    

    respond_to do |format|
      #format.js  # responds with JUST new.js.erb
      format.html { render :layout => get_layout() } # responds with JUST new.html.erb
    end
  end

  def failure
    @identity = env['omniauth.identity']

    # Always respond with json for now, possibly revisit later
    render :status => :bad_request, :json => { :errors => @identity.errors.to_hash }
  end
end
